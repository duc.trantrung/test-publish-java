package unit;

import com.merchantsdk.payment.config.ApiUrl;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.config.PathUtility;
import com.merchantsdk.payment.service.AuthorizationService;
import com.merchantsdk.payment.service.Transaction;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.json.JSONObject;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TestTransaction {
    private static Config config;
    private static Transaction transaction;
    private static Map<PathUtility, String> countryUrls;
    private static AuthorizationService authorizationService;
    @BeforeAll
    public static void setUp(){
        config = new Config(
                "STAGING",
                "VN",
                "fd092e5b-900c-4969-8c2f-48ab29ef9d67",
                "nRrOISCpbpgFx3D_",
                "0a46279c-c38c-480b-9fda-1466a5700445",
                "6a6f98dd2dd903f9da03f2139",
                "e9b5560b0be844a2ad55c6afa8b23fbb",
                "BDGSPQYYUqLXNkmy",
                "http://localhost:8888/result"
        );
        transaction = new Transaction();
        countryUrls = new ApiUrl().getURL(config.getCountry());
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testGenerateHeaders(){
        String path = "/mocapay/partner/v2/charge/init";
        String msgID = "";
        String hmac = "Fvw4HfXV39ZkZeLMXRISFy576VYvU1iUuFJ2U9TRUT4=";
        Map<String, String> headers = transaction.generateHeaders(authorizationService, config, "POST","application/json", path, "ONLINE", null, hmac, Instant.now(), msgID);
        assert headers.get("Authorization").split(":").length == 2;
        assert headers.size() == 9;

        path = "/grabid/v1/oauth2/token";
        headers = transaction.generateHeaders(authorizationService, config, "GET","application/json", path, "ONLINE", null, hmac, Instant.now(), msgID);
        assertEquals(7,headers.size() );
    }

    @Test
    public void testGet(){
        String path = countryUrls.get(PathUtility.PATH_POS_CREATE_QR_CODE);
        String httpMethod = "GET";
        String contentType = "application/json";
        String type = "OFFLINE";
        String accessToken = null;
        JSONObject reqBody = new JSONObject();
        CloseableHttpResponse response = transaction.sendRequest(config, httpMethod, path, contentType, reqBody, accessToken, type, null);
        assert response.getCode() != 200;
    }

    @Test
    public void testPOST(){
        String path = countryUrls.get(PathUtility.PATH_ONA_CHARGE_INIT);
        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);
        long amount = 2000;
        String currency = "VND";
        JSONObject reqBody = new JSONObject();
        reqBody.put("partnerTxID", partnerTxID);
        reqBody.put("partnerGroupTxID",partnerGroupTxID);
        reqBody.put("amount",amount);
        reqBody.put("currency",currency);
        reqBody.put("merchantID",config.getMerchant_id());
        reqBody.put("description","testing otc");
        reqBody.put("isSync",false);

        String httpMethod = "POST";
        String contentType = "application/json";
        String type = "ONLINE";
        String accessToken = null;

        CloseableHttpResponse response = transaction.sendRequest(config, httpMethod, path, contentType, reqBody, accessToken, type, null);
        assert response.getCode() == 200;
    }

    @Test
    public void testPUT(){
        String msgID = authorizationService.generateMD5(authorizationService.getRandomString(32));
        String grabID = "84dfaba5-7a1b-4e91-aa1c-f7ef93895266";
        String terminalID = "terminal25-meow-meow-meow";
        String partnerTxID = authorizationService.generateMD5(authorizationService.getRandomString(32));
        String origPartnerTxID = "partner-xi-1";
        String type = "OFFLINE";
        JSONObject bodyCancel = new JSONObject();
        bodyCancel.put("msgID", msgID);
        bodyCancel.put("grabID", grabID);
        bodyCancel.put("terminalID", terminalID);
        bodyCancel.put("currency", "VND");
        bodyCancel.put("origTxID", origPartnerTxID);
        bodyCancel.put("partnerTxID",partnerTxID);

        String path = countryUrls.get(PathUtility.PATH_POS_CANCEL_TRANSACTION);
        path = path.replace("{origPartnerTxID}", origPartnerTxID);
        CloseableHttpResponse response = transaction.sendRequest(config, "PUT", path, "application/json", bodyCancel, null, type, null);

        assert response.getCode() != 200;
    }

    @Test
    public void testGetHeaderFromRequest(){
        HashMap<String, Object> requestParams = new HashMap<>();

        ConcurrentHashMap<String, String> tmpHeader = new ConcurrentHashMap<>();
        tmpHeader.put("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhdWQiOiJlOWI1NTYwYjBiZTg0NGEyYWQ1NWM2YWZhOGIyM2ZiYiIsImF1dGhfdGltZSI6MTYzNzA3MjMxMCwiY2xvdWQiOiJtb2NhIiwiZXhwIjoxNjY4NjA4MzMwLCJpYXQiOjE2MzcwNzIzMzAsImludF9zdmNfYXV0aHpfY3R4IjoiMTFhYzdjMGE3NDVkNDU4Zjk0OGViYTVhZTJkYzJkZGEiLCJpc3MiOiJodHRwczovL2lkcC5ncmFiLmNvbSIsImp0aSI6ImZ0ZnY2d3hOUjRPNmhVb0JINnlSQ0EiLCJuYmYiOjE2MzcwNzIxNTAsInBpZCI6ImZkMDkyZTViLTkwMGMtNDk2OS04YzJmLTQ4YWIyOWVmOWQ2NyIsInNjcCI6IltcIjYwNmViOGIwOTg3ZjQyNDdhOGJiMmYxNDMwOTI5MmM2XCJdIiwic3ViIjoiNjg4ODUwZjQtMzI1YS00YjI2LTg5NTItNmNhZDM1NDMwMDMyIiwic3ZjIjoiUEFTU0VOR0VSIiwidGtfdHlwZSI6ImFjY2VzcyJ9.UhDXFvDBUp6WPKBzAVppEzxwKheHgtlh3kpVUMInZNW8cIdiXP4CA9RQIE1kxR3QkPBdlxWNsvZcJRrHrK8Imqzfwoaw7An_ZxM-t7lInh-SbYSE8aSygB9gcwktYIXsyiGeAG8YVcz_d7ahnThnfA3DshJge4T1KlPyLqqm0qSCmCndiwVvobuT96ehfA_QjlAQ9IC63A76idL51esiwO0NS4x5iH7ueObWmrldtUkRU40KaDZpENEiQak3L42Hm7LWkX_JKEb3ezWXy6_RiSpvJjJijkYKhBeeyT3vt16tjs10iWCjFEJpi2zU5k4kxRVluvTrUuZ8ZDwmyVmm4w");
        tmpHeader.put("Accept", "application/json");

        requestParams.put("body", null);
        requestParams.put("apiUrl", null);
        requestParams.put("header", tmpHeader);

        Map<String, String> header = transaction.getHeaderFromRequest(requestParams);
        assert header.getClass() == ConcurrentHashMap.class;

        assertEquals("Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhdWQiOiJlOWI1NTYwYjBiZTg0NGEyYWQ1NWM2YWZhOGIyM2ZiYiIsImF1dGhfdGltZSI6MTYzNzA3MjMxMCwiY2xvdWQiOiJtb2NhIiwiZXhwIjoxNjY4NjA4MzMwLCJpYXQiOjE2MzcwNzIzMzAsImludF9zdmNfYXV0aHpfY3R4IjoiMTFhYzdjMGE3NDVkNDU4Zjk0OGViYTVhZTJkYzJkZGEiLCJpc3MiOiJodHRwczovL2lkcC5ncmFiLmNvbSIsImp0aSI6ImZ0ZnY2d3hOUjRPNmhVb0JINnlSQ0EiLCJuYmYiOjE2MzcwNzIxNTAsInBpZCI6ImZkMDkyZTViLTkwMGMtNDk2OS04YzJmLTQ4YWIyOWVmOWQ2NyIsInNjcCI6IltcIjYwNmViOGIwOTg3ZjQyNDdhOGJiMmYxNDMwOTI5MmM2XCJdIiwic3ViIjoiNjg4ODUwZjQtMzI1YS00YjI2LTg5NTItNmNhZDM1NDMwMDMyIiwic3ZjIjoiUEFTU0VOR0VSIiwidGtfdHlwZSI6ImFjY2VzcyJ9.UhDXFvDBUp6WPKBzAVppEzxwKheHgtlh3kpVUMInZNW8cIdiXP4CA9RQIE1kxR3QkPBdlxWNsvZcJRrHrK8Imqzfwoaw7An_ZxM-t7lInh-SbYSE8aSygB9gcwktYIXsyiGeAG8YVcz_d7ahnThnfA3DshJge4T1KlPyLqqm0qSCmCndiwVvobuT96ehfA_QjlAQ9IC63A76idL51esiwO0NS4x5iH7ueObWmrldtUkRU40KaDZpENEiQak3L42Hm7LWkX_JKEb3ezWXy6_RiSpvJjJijkYKhBeeyT3vt16tjs10iWCjFEJpi2zU5k4kxRVluvTrUuZ8ZDwmyVmm4w",
                header.get("Authorization"));

        assertEquals("application/json", header.get("Accept"));

        String wrongHeader = "This is wrong header";
        requestParams.put("header", wrongHeader);
        header = transaction.getHeaderFromRequest(requestParams);
        assertNull(header);

        requestParams.clear();
        requestParams.put("headers", tmpHeader);
        header = transaction.getHeaderFromRequest(requestParams);
        assertNull(header);
    }

    @Test
    public void testGetBodyFromRequest(){
        HashMap<String, Object> requestParams = new HashMap<>();
        requestParams.put("body", "mewo mew mow");
        requestParams.put("apiUrl", null);
        requestParams.put("header", null);

        Object body = transaction.getBodyFromRequest(requestParams);
        assertEquals(String.class, body.getClass());
        assertEquals("mewo mew mow", body.toString());

        JSONObject reqBody = new JSONObject();
        reqBody.put("name","meo");
        requestParams.put("body", reqBody);

        assertNull(transaction.getBodyFromRequest(requestParams));
    }

    @Test
    public void testGetUrlFromRequest(){
        HashMap<String, Object> requestParams = new HashMap<>();
        requestParams.put("body", "mewo mew mow");
        requestParams.put("apiUrl", "grab/payment");
        requestParams.put("header", null);

        Object url = transaction.getUrlFromRequest(requestParams);
        assertEquals(String.class, url.getClass());
        assertEquals("grab/payment", url.toString());

        requestParams.put("apiUrl", null);
        assertNull(transaction.getUrlFromRequest(requestParams));
    }

    @Test
    public void testBuildHttpGetUrl(){
        String path = "https://grab.com";
        JSONObject params = new JSONObject();
        params.put("id", 123456);
        params.put("name", "john.wick");


        String fullPath = transaction.buildHttpGetUrl(params, path);
        String[] partURL = fullPath.split("\\?");
        assertEquals("https://grab.com", partURL[0]);

        String[] param = partURL[1].split("&");
        assertEquals(2, param.length);
        assert param[0].equals("id=123456") || param[0].equals("name=john.wick");
        assert param[1].equals("id=123456") || param[1].equals("name=john.wick");

        path = "https://grab.com?sub=moca";
        fullPath = transaction.buildHttpGetUrl(params, path);
        partURL = fullPath.split("\\?");
        assertEquals("https://grab.com", partURL[0]);

        param = partURL[1].split("&");
        assertEquals(3, param.length);
        assert param[0].equals("id=123456") || param[0].equals("name=john.wick") || param[0].equals("sub=moca");
        assert param[1].equals("id=123456") || param[1].equals("name=john.wick") || param[0].equals("sub=moca");

        params.clear();
        path = "https://grab.com?sub=moca";
        fullPath = transaction.buildHttpGetUrl(params, path);
        assertEquals(path, fullPath);

        path = "https://grab.com";
        fullPath = transaction.buildHttpGetUrl(null, path);
        assertEquals(path, fullPath);
    }

    @Test
    public void testPrepareRequest(){
        String httpMethod = "GET";
        String path = countryUrls.get(PathUtility.PATH_POS_TRANSACTION_DETAILS);
        String type = "offline";
        String contentType = "application/json";
        JSONObject requestBody = new JSONObject();
        String accessToken = null;
        String msgID = null;
        String partnerTxId = "qwertyuiopasdfghjklzxcvbnmqwerty";
        ConcurrentHashMap<String, String> header;
        String body;
        String url;
        int n = 6;

        // test get pos transaction
        HashMap<String, Object> prepare = transaction.prepareRequest(authorizationService,config, httpMethod, path, contentType, requestBody, accessToken, type, msgID);

        header = transaction.getHeaderFromRequest(prepare);
        assertNotNull(header, "Header should not be null");
        assertTrue(header.containsKey("Authorization"),"Header should contains 'Authorization' param");

        body = transaction.getBodyFromRequest(prepare);
        assertNotNull(body,"Body should not be null");
        assertTrue(body.isEmpty(), "Body should be empty");

        url = transaction.getUrlFromRequest(prepare);
        assertNotNull(url,"url should not be null");

        String[] param = url.split("\\?")[1].split("&");
        assertEquals(param.length, n, "Url should have " + n +  " params");

        // test POS POST
        requestBody.put("amount", 2000);
        requestBody.put("currency", "VND");
        requestBody.put("partnerTxID", partnerTxId);

        httpMethod = "POST";
        path = countryUrls.get(PathUtility.PATH_POS_CREATE_QR_CODE);

        prepare = transaction.prepareRequest(authorizationService,config, httpMethod, path, contentType, requestBody, accessToken, type, msgID);

        header = transaction.getHeaderFromRequest(prepare);
        assertNotNull(header,"Header should not be null");
        assertEquals(9, header.size(),"Header should contains 9 params");

        body = transaction.getBodyFromRequest(prepare);
        assertNotNull(body,"Body should not be null");
        assertTrue(body.contains("msgID"),"Body should contains msgID");

        url = transaction.getUrlFromRequest(prepare);
        assertNotNull(url,"url should not be null");

        n = 0;
        assertEquals(n, url.split("\\?").length - 1, "Url should have " + n +  " params");

        // test POST OnA OAuth
        requestBody.clear();
        requestBody.put("grant_type","authorization_code");
        requestBody.put("client_id",config.getClient_id());
        requestBody.put("client_secret",config.getClient_secret());
        requestBody.put("code_verifier",authorizationService.base64URLEncode(partnerTxId+partnerTxId));
        requestBody.put("redirect_uri",config.getRedirect_uri());
        requestBody.put("code","f53a5b4a814548cdb8c07484ebda2f25");

        path = countryUrls.get(PathUtility.PATH_OAUTH_TOKEN);
        type = "online";
        prepare = transaction.prepareRequest(authorizationService, config, httpMethod, path, contentType, requestBody, accessToken, type, msgID);

        header = transaction.getHeaderFromRequest(prepare);
        assertNotNull(header, "Header should not be null");
        assertEquals(7, header.size(),"Header should contains 7 params");

        body = transaction.getBodyFromRequest(prepare);
        assertNotNull( body,"Body should not be null");

        url = transaction.getUrlFromRequest(prepare);
        assertNotNull(url,"Url should not be null");
        assertTrue( url.contains("/grabid/v1/oauth2/token"), "Url should contains path to OAuth API");
    }
}
