package integration.regional;

import com.merchantsdk.payment.MerchantIntegrationOnline;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMerchantIntegrationOnlineRegional {
    private static AuthorizationService authorizationService;
    private static MerchantIntegrationOnline merchantIntegrationOnline;
    private static Config config;

    @BeforeAll
    public static void setUp(){
        final String staging = "STG";
        final String country = "MY";
        final String partner_id = "b597c141-58e6-41c0-a362-027eed96fc81";
        final String partner_secret = "w_iQ9ls106MTxP1V";
        final String merchant_id = "ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379";
        final String client_id = "8888ef0060c24f41929ca41bfc702032";
        final String client_secret = "K6J1aznYAfT8ckyn";
        final String redirect_uri = "http://localhost:8888/welcome";

        merchantIntegrationOnline = new MerchantIntegrationOnline(
                staging,
                country,
                partner_id,
                partner_secret,
                merchant_id,
                client_id,
                client_secret,
                redirect_uri
        );
        config = merchantIntegrationOnline.getConfig();
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testOnaChargeInit(){
        config.setPartner_id("b597c141-58e6-41c0-a362-027eed96fc81");
        config.setPartner_secret("w_iQ9ls106MTxP1V");
        config.setMerchant_id("ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379");

        long amount = 20;
        String currency = "MYR";
        boolean isSync = false;
        String description = "this is testing";

        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);
        JSONObject response = merchantIntegrationOnline.onaChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null,null);

        assert response.length() == 2;
        assertEquals(partnerTxID, response.get("partnerTxID"), "partnerTxID in response should equals in request");
        assertNotNull(response.get("request"));

        String[] hidePaymentMethods = {"POSTPAID"};
        partnerTxID = authorizationService.getRandomString(32);
        partnerGroupTxID = authorizationService.getRandomString(32);
        response = merchantIntegrationOnline.onaChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null,hidePaymentMethods);
        assert response.length() == 2;
        assertEquals(partnerTxID,response.get("partnerTxID"), "partnerTxID in response should equals in request");
        assertNotNull(response.get("request"));
    }
}
