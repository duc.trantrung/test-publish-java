package integration.vn;

import com.merchantsdk.payment.config.ApiUrl;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import com.merchantsdk.payment.service.OfflineTransaction;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Set;

public class TestOfflineTransaction {
    private static AuthorizationService authorizationService;
    private static OfflineTransaction offlineTransaction;
    private static Config config;

    @BeforeAll
    public static void setUp(){
        config = new Config(
                "STAGING",
                "VN",
                "a965c5e5-5efa-4950-b217-1afadc81f811",
                "JBlllk4rZv3Yf1j7",
                "1c334ed5-2acf-471d-9eaa-ca84e6815948",
                "d3949374fa4d4ca9b9449668b",
                "e6393d0d51d14107b0342c9d0d962298",
                "e6393d0d51d14107b0342c9d0d962298",
                "http://localhost:8888/result"
        );
        config.setGrab_id("83b2e210-d2c7-494b-8e3e-551d77920075");
        offlineTransaction = new OfflineTransaction(config, new ApiUrl().getURL(config.getCountry()));
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testOfflineTransaction() throws Exception{
        String partnerTxID = "partner-" + authorizationService.getRandomString(24);
        String msg = authorizationService.getRandomString(32);
        String msgID = authorizationService.generateMD5(msg);

        long amount = 10000;
        CloseableHttpResponse qrCode = offlineTransaction.apiCreateQrCode(msgID, partnerTxID, amount, "VND");

        assert qrCode.getCode() == 200;

        HttpEntity responseEntity = qrCode.getEntity();
        String responseString = EntityUtils.toString(responseEntity, StandardCharsets.UTF_8);
        JSONObject responseBody = new JSONObject(responseString);
        Set<String> keys = responseBody.keySet();
        assert keys.size() == 5;
        assert keys.contains("msgID");
        assert keys.contains("qrcode");
        assert keys.contains("txID");
        assert keys.contains("qrid");
        assert keys.contains("expiryTime");

        CloseableHttpResponse getTx = offlineTransaction.apiPosGetTxnStatus(msgID, partnerTxID, "VND");
        assert getTx.getCode() >= 400;

        CloseableHttpResponse refundGetTx = offlineTransaction.apiPosGetTxnStatus(msgID, partnerTxID, "VND");
        assert refundGetTx.getCode() >= 400;
    }
}
