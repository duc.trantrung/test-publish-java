package integration.vn;

import com.merchantsdk.payment.MerchantIntegrationOnline;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestMerchantIntegrationOnlineVN {
    private static AuthorizationService authorizationService;
    private static MerchantIntegrationOnline merchantIntegrationOnline;
    private static Config config;
    private static String staging = "STAGING";
    private static String country = "VN";
    private static String partner_id = "fd092e5b-900c-4969-8c2f-48ab29ef9d67";
    private static String partner_secret = "nRrOISCpbpgFx3D_";
    private static String merchant_id = "0a46279c-c38c-480b-9fda-1466a5700445";
    private static String client_id = "e9b5560b0be844a2ad55c6afa8b23fbb";
    private static String client_secret = "BDGSPQYYUqLXNkmy";
    private static String redirect_uri = "http://localhost:8888/result";

    @BeforeAll
    public static void setUp(){
        merchantIntegrationOnline = new MerchantIntegrationOnline(
                staging,
                country,
                partner_id,
                partner_secret,
                merchant_id,
                client_id,
                client_secret,
                redirect_uri
        );
        config = merchantIntegrationOnline.getConfig();
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testApiChargeInit(){
        long amount = 2000;
        String currency = "VND";
        boolean isSync = false;
        String description = "this is testing";
        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);
        JSONObject response = merchantIntegrationOnline.onaChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null,null);
        assert response.length() == 2;
        assertEquals(partnerTxID,response.get("partnerTxID"), "partnerTxID in response should equals in request");
        assertNotNull(response.get("request"));

        String[] hidePaymentMethods = {"POSTPAID"};
        partnerTxID = authorizationService.getRandomString(32);
        partnerGroupTxID = authorizationService.getRandomString(32);
        response = merchantIntegrationOnline.onaChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null,hidePaymentMethods);
        assert response.length() == 2;
        assertEquals(partnerTxID,response.get("partnerTxID"), "partnerTxID in response should equals in request");
        assertNotNull(response.get("request"));

    }
}
