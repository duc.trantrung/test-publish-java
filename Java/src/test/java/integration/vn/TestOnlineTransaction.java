package integration.vn;

import com.merchantsdk.payment.config.ApiUrl;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import com.merchantsdk.payment.service.OnlineTransaction;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestOnlineTransaction {
    private static AuthorizationService authorizationService;
    private static OnlineTransaction onlineTransaction;
    private static Config config;

    @BeforeAll
    public static void setUp(){
        config = new Config(
                "STAGING",
                "VN",
                "fd092e5b-900c-4969-8c2f-48ab29ef9d67",
                "nRrOISCpbpgFx3D_",
                "0a46279c-c38c-480b-9fda-1466a5700445",
                "6a6f98dd2dd903f9da03f2139",
                "e9b5560b0be844a2ad55c6afa8b23fbb",
                "BDGSPQYYUqLXNkmy",
                "http://localhost:8888/result"
        );
        onlineTransaction = new OnlineTransaction(config, new ApiUrl().getURL(config.getCountry()));
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testApiChargeInit(){
        long amount = 2000;
        String currency = "VND";
        boolean isSync = false;
        String description = "this is testing";

        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);

        CloseableHttpResponse response = onlineTransaction.apiChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null, null);

        assert response.getCode() == 200;

        // test hidPaymentMethods
        String[] hidePaymentMethods = {"INSTALMENT"};
        partnerTxID = authorizationService.getRandomString(32);
        partnerGroupTxID = authorizationService.getRandomString(32);
        response = onlineTransaction.apiChargeInit(partnerTxID,partnerGroupTxID,amount, currency, isSync, description,null,null,null, hidePaymentMethods);
        assertEquals(200,response.getCode() );
    }

    @Test
    public void testOnlineTrancsaction(){
        long amount = 2000;
        String currency = "VND";
        boolean isSync = false;
        String description = "this is testing";
        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);

        String respWebURL = onlineTransaction.apiCreateWerURL(partnerTxID, partnerGroupTxID, amount, currency, isSync, description, null, null, null, null, null);
        assertNotNull(respWebURL);
        assertFalse(respWebURL.isEmpty());
    }
}