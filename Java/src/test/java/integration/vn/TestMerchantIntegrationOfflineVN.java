package integration.vn;

import com.merchantsdk.payment.MerchantIntegrationOffline;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TestMerchantIntegrationOfflineVN {
    private static AuthorizationService authorizationService;
    private static MerchantIntegrationOffline merchantIntegrationOffline;
    private static Config config;

    @BeforeAll
    public static void setUp(){
        final String staging = "STG";
        final String country = "VN";
        final String partner_id = "939e5338-c610-4d09-b3f1-392a3c88a27f";
        final String partner_secret = "JX9CcWUUIhJzQS-h";
        final String merchant_id = "f68c4f68-f55f-4214-a2cc-ef9d756941e6";
        final String terminal_id = "6c6693bb4b370f3626060ba96";

        merchantIntegrationOffline = new MerchantIntegrationOffline(
                staging,
                country,
                partner_id,
                partner_secret,
                merchant_id,
                terminal_id
        );
        config = merchantIntegrationOffline.getConfig();
        authorizationService = new AuthorizationService();
    }

    @Test
    public void testCreateQrCode(){

        String partnerTxID = "partner-" + authorizationService.getRandomString(24);
        String msg = authorizationService.getRandomString(32);
        String msgID = authorizationService.generateMD5(msg);

        long amount = 10000;
        String currency = "VND";
        JSONObject qrCode = merchantIntegrationOffline.posCreateQRCode(msgID, partnerTxID, amount, currency);

        assertEquals(5,qrCode.length());
        assertNotNull(qrCode.get("msgID"));
        assertEquals( msgID, qrCode.get("msgID"),"msgId should be same in request and response");
        assertNotNull(qrCode.get("qrcode"));
        assertNotNull(qrCode.get("txID"));
        assertNotNull(qrCode.get("qrid"));
        assertNotNull(qrCode.get("expiryTime"));

        String key = "Error";
        JSONObject getTx = merchantIntegrationOffline.posGetTxnDetails(msgID, partnerTxID, currency);
        assertTrue((Integer)getTx.get(key) >= 400);

        JSONObject refundGetTx = merchantIntegrationOffline.posGetRefundDetails(msgID, partnerTxID, currency);
        assertTrue((Integer)refundGetTx.get(key) >= 400);
    }
}

