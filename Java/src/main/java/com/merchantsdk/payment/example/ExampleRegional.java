package com.merchantsdk.payment.example;

import com.merchantsdk.payment.MerchantIntegration;
import com.merchantsdk.payment.MerchantIntegrationOnline;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import org.json.JSONObject;

import java.util.Scanner;

public class ExampleRegional {
    private final AuthorizationService authorizationService;

    private Config config;
    private final String staging = "STAGING";
    private final String country = "MY";
    private final String partner_id = "b597c141-58e6-41c0-a362-027eed96fc81";
    private final String partner_secret = "w_iQ9ls106MTxP1V";
    private final String merchant_id = "ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379";
    private final String terminal_id = "6a6f98dd2dd903f9da03f2139";
    private final String client_id = "8888ef0060c24f41929ca41bfc702032";
    private final String client_secret = "K6J1aznYAfT8ckyn";
    private final String redirect_uri = "http://172.104.183.204/index.php";

    public ExampleRegional(){
        this.config = new Config(
                staging,
                country,
                partner_id,
                partner_secret,
                merchant_id,
                terminal_id,
                client_id,
                client_secret,
                redirect_uri
        );
        this.authorizationService = new AuthorizationService();
    }

    private void exampleMerchantIntegration(){
        Scanner sc = new Scanner(System.in);
        String staging = this.config.getStaging();
        String country = this.config.getCountry();
        String partner_id = this.config.getPartner_id();
        String partner_secret = this.config.getPartner_secret();
        String merchant_id = this.config.getMerchant_id();
        String terminal_id = this.config.getTerminal_id();

        String client_id = this.config.getClient_id();
        String client_secret = this.config.getClient_secret();
        String redirect_url = this.config.getRedirect_uri();

        // API OnA charge init
        MerchantIntegrationOnline merchantIntegration = new MerchantIntegrationOnline(staging, country,partner_id, partner_secret, merchant_id,client_id, client_secret, redirect_url);

        System.out.println("Please input currency code : ");
        String currency = sc.next();

        System.out.println("Please input amount : ");
        long amount = sc.nextLong();

        String description = "regional testing ";
        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);
        String[] hidePaymentMethods = {"POSTPAID"};
        // API OnA web
        String respWebURL = merchantIntegration.onaCreateWebUrl(partnerTxID, partnerGroupTxID, amount, currency, false, description, null, null, null, hidePaymentMethods,null);
        System.out.println("Please copy this link and open it in browser and pay: \n" + respWebURL);
        System.out.println("Enter response code after payment " );
        String code = sc.next();

        // API OnA OAuth
        JSONObject respAuthCode = merchantIntegration.onaOAuth2Token(partnerTxID,code);
        System.out.println("Auth code = " + respAuthCode);

        String access_token = respAuthCode.get("access_token").toString();

        // API OnA charge complete
        JSONObject respChargeComplete = merchantIntegration.onaChargeComplete(partnerTxID,access_token);
        System.out.println("Response Charge Complete : " + respChargeComplete);

        // API OnA charge complete status
        JSONObject getChargeStatus = merchantIntegration.onaGetChargeStatus(partnerTxID, currency, access_token);
        System.out.println("GetChargeStatus :" +getChargeStatus);

        // API OnA refund
        String originTxID = getChargeStatus.get("txID").toString();
        System.out.println("Please input refund amount : ");
        long refundAmount = sc.nextLong();
        JSONObject refund = merchantIntegration.onaRefund( partnerTxID, partnerGroupTxID, refundAmount, currency, originTxID, "testing refund",access_token);
        System.out.println("Refund : " + refund);

        // API OnA refund status
        JSONObject refundStatus = merchantIntegration.onaGetRefundStatus(partnerTxID, currency,access_token);
        System.out.println("Refund status : "+ refundStatus);

        // API OnA OTC status
        JSONObject OTCStatus = merchantIntegration.onaGetOTCStatus(partnerTxID,currency);
        System.out.println("OTC status : "+ OTCStatus);
        sc.close();
    }

    public static void main(String[] args){
        ExampleRegional test = new ExampleRegional();
        test.exampleMerchantIntegration();
    }
}

