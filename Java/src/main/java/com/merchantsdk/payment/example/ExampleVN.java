package com.merchantsdk.payment.example;

import com.merchantsdk.payment.MerchantIntegrationOnline;
import com.merchantsdk.payment.config.Config;
import com.merchantsdk.payment.service.AuthorizationService;
import org.json.JSONObject;

import java.util.Scanner;

public class ExampleVN {
    private AuthorizationService authorizationService;
    private Config config;
    private final String staging = "STAGING";
    private final String country = "VN";
    private final String partner_id = "fd092e5b-900c-4969-8c2f-48ab29ef9d67";
    private final String partner_secret = "nRrOISCpbpgFx3D_";
    private final String merchant_id = "0a46279c-c38c-480b-9fda-1466a5700445";
    private final String terminal_id = "6a6f98dd2dd903f9da03f2139";
    private final String client_id = "e9b5560b0be844a2ad55c6afa8b23fbb";
    private final String client_secret = "BDGSPQYYUqLXNkmy";
    private final String redirect_uri = "http://localhost:8888/result";

    public ExampleVN(){
        this.config = new Config(
                staging,
                country,
                partner_id,
                partner_secret,
                merchant_id,
                terminal_id,
                client_id,
                client_secret,
                redirect_uri
        );
        this.authorizationService = new AuthorizationService();
    }

    private void exampleMerchantIntegration(){
        String staging = this.config.getStaging();
        String country = this.config.getCountry();
        String partner_id = this.config.getPartner_id();
        String partner_secret = this.config.getPartner_secret();
        String merchant_id = this.config.getMerchant_id();

        String client_id = this.config.getClient_id();
        String client_secret = this.config.getClient_secret();
        String redirect_url = this.config.getRedirect_uri();

        // API OnA charge init
        MerchantIntegrationOnline merchantIntegration = new MerchantIntegrationOnline(staging, country,partner_id, partner_secret, merchant_id,client_id, client_secret, redirect_url);
        long amount = 2000;
        String currency = "VND";
        String description = "this is testing";
        String partnerTxID = authorizationService.getRandomString(32);
        String partnerGroupTxID = authorizationService.getRandomString(32);

        // API OnA web
        String respWebURL = merchantIntegration.onaCreateWebUrl(partnerTxID, partnerGroupTxID, amount, currency, false, description, null, null, null, null, null);
        if (respWebURL.startsWith("Error!")) {
            System.out.println(respWebURL);
            return;
        }
        System.out.println("Please copy this link and open it in browser and pay: \n" + respWebURL);
        System.out.println("Enter response code after payment " );
        Scanner sc = new Scanner(System.in);
        String code = sc.nextLine();
        System.out.println(code);

        // API OnA OAuth
        JSONObject respAuthCode = merchantIntegration.onaOAuth2Token(partnerTxID,code);
        System.out.println("Auth code = " + respAuthCode);

        String access_token = respAuthCode.get("access_token").toString();

        // API OnA charge complete
        JSONObject respChargeComplete = merchantIntegration.onaChargeComplete(partnerTxID,access_token);
        System.out.println("Response Charge Complete : " + respChargeComplete);

        // API OnA charge complete status
        JSONObject getChargeStatus = merchantIntegration.onaGetChargeStatus(partnerTxID, currency, access_token);
        System.out.println("GetChargeStatus :" + getChargeStatus);

        // API OnA refund
        String originTxID = getChargeStatus.get("txID").toString();
        System.out.println("Please input refund amount : ");
        long refundAmount = sc.nextLong();
        JSONObject refund = merchantIntegration.onaRefund( partnerTxID, partnerGroupTxID, refundAmount, currency, originTxID, "testing refund", access_token);
        System.out.println("Refund : " + refund);

        // API OnA refund status
        JSONObject refundStatus = merchantIntegration.onaGetRefundStatus(partnerTxID, currency,access_token);
        System.out.println("Refund status : "+ refundStatus);

        // API OnA OTC status
        JSONObject OTCStatus = merchantIntegration.onaGetOTCStatus(partnerTxID,currency);
        System.out.println("OTC status : "+ OTCStatus);
        sc.close();
    }

    public static void main(String[] args){
        ExampleVN test = new ExampleVN();
        test.exampleMerchantIntegration();
    }
}
