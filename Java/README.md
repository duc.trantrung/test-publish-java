# JAVA SDK for Merchant Integration 

## Merchant is provided by class MerchantIntegration (package com.merchantsdk.payment)

## Required 

- **JDK 8 or above**

- **json package(org.json)**

## How to use SDK

For using this SDK, You might copy all files in [src](./src) folder and paste it to your project.
You also might install all package in ```build.gradle``` file.

## How to use MerchantIntegration class


To use this class, you may create instance of class with parameters

### Config:

- stage : String : "PRD"/"STG"
- country : String : Country code of the merchant location. [Refer](https://countrycode.org/)
- partner_id : String - Unique ID for a partner. Retrieve from Developer Hom
- partner_secret : String - Secret key for a partner. Retrieve from Developer Home
- merchant_id : String - Retrieve from Developer Home.
- terminal_id : String - Retrieve from Developer Home. **Only available for POS (P2M)**
- client_id : String - Retrieve from Developer Home. **Only available for OnA**
- client_secret : String - Retrieve from Developer Home. **Only available for OnA**
- redirect_url : String - The url configured in Developer Home. **Only available for OnA**

#### Online Merchant 
```
MerchantIntegrationOnline merchantIntegrationOnline = new MerchantIntegrationOnline(stage, country, partner_id, partner_secret, merchant_id, client_id, client_secret, redirect_url)
```
#### Offline Merchant
```
MerchantIntegrationOffline merchantIntegrationOffline = new MerchantIntegrationOffline(stage, country, partner_id, partner_secret, merchant_id, terminal_id)
```

### Example

***Online Transaction (OnA)***
```
MerchantIntegrationOnline merchantIntegrationOnline = new MerchantIntegrationOnline("stage", "country", "partner_id", "partner_secret", "merchant_id", "client_id", "client_secret", "redirect_urlt")
```

***Offline Transaction (POS)***
```
MerchantIntegrationOffline merchantIntegrationOffline = new MerchantIntegrationOffline("stage", "country", "partner_id", "partner_secret", "merchant_id", "terminal_id")
```

### How to request APIs

#### Example set up parameters

```
String partnerTxID = "partnerTxID";
String partnerGroupTxID = "partnerGroupTxID";
String refundPartnerTxID = "refundPartnerTxID"; 
long amount = 2000;
String currency = "CUR";
boolean isSync = false;
String description = "this is testing";

String origPartnerTxID = "origPartnerTxID";
String msgID = "msgID";
```

#### Online Transactions (OnA)

While I am writting this document, I am leaving *metaInfo*, *items*, *shippingDetails*, *hidePaymentMethods*, *state* as null value because it is optional.

##### 1. onaChargeInit

```
merchantIntegrationOnline.onaChargeInit(String partnerTxID, String  partnerGroupTxID, String amount, String currency, String  isSync, String  description,JSONObject metaInfo,JSONObject items,JSONObject shippingDetails, String[] hidePaymentMethods)
```
- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaChargeInit(partnerTxID, partnerGroupTxID, amount, currency, isSync, description, null, null, null, null);
```

##### 2. onaCreateWebUrl

```
merchantIntegrationOnline.onaCreateWebUrl(String partnerTxID, String  partnerGroupTxID, String amount, String currency, String  isSync, String  description,JSONObject metaInfo,JSONObject items,JSONObject shippingDetails,String[] hidePaymentMethods, String state)
```
- **Example request:**
```
String response = merchantIntegrationOnline.onaCreateWebUrl(partnerTxID, partnerGroupTxID, amount, currency, isSync, description, null, null, null, null, null);
```

##### 3. onaOAuth2Token

```
merchantIntegrationOnline.onaOAuth2Token(String partnerTxID, String code)
```
Merchant get **code** in redirected url after successfully pay

- **Example request:**
```
String code = "a09a6c1da7c843519d03978d659c60d8" ;
JSONObject response = merchantIntegrationOnline.onaOAuth2Token(partnerTxID, code);
```

##### 4. onaChargeComplete

```
merchantIntegrationOnline.onaChargeComplete(String partnerTxID,String accessToken)
```
Merchant get **accessToken** from ***onaOAuth2Token*** API response

- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaChargeComplete(partnerTxID, accessToken);
```

##### 5. onaGetChargeStatus

```
merchantIntegrationOnline.onaGetChargeStatus(String partnerTxID,String currency,String accessToken)
```
Merchant get **accessToken** from ***onaOAuth2Token*** API response

- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaGetChargeStatus(partnerTxID,currency, accessToken);
```

##### 6. onaRefund

```
merchantIntegrationOnline.onaRefund(String refundPartnerTxID, String partnerGroupTxID,long amount,String currency,String txID,String description,String accessToken)
```
Merchant get **accessToken** from ***onaOAuth2Token*** API response

Merchant get **txID** from ***onaChargeComplete*** or ***onaGetChargeStatus*** API response

- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaRefund(refundPartnerTxID, partnerGroupTxID, amount, currency, txID, description, accessToken);
```

##### 7. onaGetRefundStatus

```
merchantIntegrationOnline.onaGetRefundStatus(String refundPartnerTxID,String currency,String accessToken)
```
Merchant get **accessToken** from ***onaOAuth2Token*** API response

- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaGetRefundStatus(refundPartnerTxID, accessToken, currency);
```

##### 8. onaGetOTCStatus

```
merchantIntegrationOnline.onaGetOTCStatus(String partnerTxID,String accessToken,String currency)
```
Merchant get **accessToken** from ***onaOAuth2Token*** API response

- **Example request:**
```
JSONObject response = merchantIntegrationOnline.onaGetOTCStatus(partnerTxID, accessToken, currency);
```

#### Offline Transactions (POS)

##### 1. posCreateQRCode

```
merchantIntegrationOffline.posCreateQRCode(String msgID, String partnerTxID, long amount, String currency)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posCreateQRCode(msgID, partnerTxID, amount, currency);
```

##### 2. posPerformQRCode

```
merchantIntegrationOffline.posPerformQRCode(String msgID, String partnerTxID, long amount, String currency, String code)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posPerformQRCode(msgID, partnerTxID, amount, currency, code);
```

##### 3. posCancel

```
merchantIntegrationOffline.posCancel(String msgID, String partnerTxID,String origPartnerTxID,String origTxID,String currency)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency);
```

##### 4. posRefund

```
merchantIntegrationOffline.posRefund(String msgID, String refundPartnerTxID,long amount,String currency, String origPartnerTxID, String description)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posRefund(msgID, refundPartnerTxID, amount, currency, origPartnerTxID, description);
```

##### 5. posGetTxnDetails

```
merchantIntegrationOffline.posGetTxnDetails(String msgID, String partnerTxID, String currency)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posGetTxnDetails(msgID, partnerTxID, currency);
```

##### 6. posGetRefundDetails

```
merchantIntegrationOffline.posGetRefundDetails(String msgID, String refundPartnerTxID, String currency)
```

- **Example request:**
```
JSONObject response = merchantIntegrationOffline.posGetRefundDetails(msgID, refundPartnerTxID, currency);
```
