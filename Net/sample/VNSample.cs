﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using Net.Public;
using Newtonsoft.Json;

namespace NetConsole
{
    public class VNSample
    {

        private static Random random = new Random();


        public static void SamplePos()
        {
            Console.WriteLine("SAMPLE OFFLINE:");
            Console.ReadKey();

            var partnerId = "939e5338-c610-4d09-b3f1-392a3c88a27f";
            var partnerSecret = "JX9CcWUUIhJzQS-h";
            var merchantId = "f68c4f68-f55f-4214-a2cc-ef9d756941e6";
            var terminalId = "6c6693bb4b370f3626060ba96";
            var env = "STAGING";
            var country = "VN";
            string currency = "VND";

            MerchantIntegrationOffline merchantClient = new MerchantIntegrationOffline(env, country, partnerId, partnerSecret, merchantId, terminalId);
            string partnerTxId = RandomString(32);
            string msgId = RandomString(32);

            Console.WriteLine("partnerTxId: " + partnerTxId);
            Console.WriteLine("msgId: " + msgId);
            Console.Write("Please enter number amount: ");
            long amount = long.Parse(Console.ReadLine());
            var response = merchantClient.PosCreateQRCode(msgId, partnerTxId, amount, currency);

            Console.WriteLine("PosCreateQRCode response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();

            response = merchantClient.PosGetTxnStatus(msgId, partnerTxId, currency);
            Console.WriteLine("PosGetTxnDetails response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
        }

        public static void SampleOnA()
        {
            Console.WriteLine("SAMPLE ONLINE:");
            Console.ReadKey();


            var partnerId = "fd092e5b-900c-4969-8c2f-48ab29ef9d67";
            var partnerSecret = "nRrOISCpbpgFx3D_";
            var merchantId = "0a46279c-c38c-480b-9fda-1466a5700445";
            var clientId = "e9b5560b0be844a2ad55c6afa8b23fbb";
            var clientSecret = "BDGSPQYYUqLXNkmy";
            var env = "STAGING";
            var country = "VN";
            var redirectUrl = "http://localhost:8888/result";
            string currency = "VND";

            MerchantIntegrationOnline merchantClient = new MerchantIntegrationOnline(env, country, partnerId, partnerSecret, merchantId, clientId, clientSecret, redirectUrl);

            string partnerTxId = RandomString(32);
            string partnerGroupTxId = RandomString(32);
            Console.WriteLine("partnerTxId: " + partnerTxId);
            Console.WriteLine("partnerGroupTxId: " + partnerGroupTxId);
            Console.Write("Please enter number amount:");
            long amount = long.Parse(Console.ReadLine());
            Console.WriteLine("amount: " + amount + " " + currency);

            var state = RandomString(7);
            Console.WriteLine("state: " + state);

            var response = merchantClient.OnaCreateWebUrl(partnerTxId, partnerGroupTxId, amount, currency, state);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Please copy url, open it on browser and make a payment by Grab app: "
                   + response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.Write("Please enter code from redirect url: ");
            var code = Console.ReadLine();

            response = merchantClient.OnaOAuth2Token(partnerTxId, code);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            var bodyResp = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);
            var accessToken = bodyResp.access_token.ToString();

            response = merchantClient.OnaChargeComplete(partnerTxId, accessToken);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Charge complete response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            var txId = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result).txID.ToString();
            Console.ReadKey();
            response = merchantClient.OnaGetChargeStatus(partnerTxId, currency, accessToken);
            Console.WriteLine("Get charge status response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.WriteLine("Refund flow");
            Console.ReadKey();
            Console.Write("Please enter number amount: ");
            amount = long.Parse(Console.ReadLine());

            var refundTxId = RandomString(32);
            response = merchantClient.OnaRefund(refundTxId, RandomString(32), amount, currency, txId, "description", accessToken);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Refund response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.WriteLine("Get refund status response:");
            response = merchantClient.OnaGetRefundStatus(refundTxId, currency, accessToken);
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);

            Console.ReadKey();

        }

        public static void DisplayError(HttpResponseMessage response)
        {
            Console.WriteLine("Something went wrong!!!!");
            Console.WriteLine(response);
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghyklmnopqrstuwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateMsgID()
        {
            var random = new Random();
            var md5 = MD5.Create();
            var id = string.Format("{0}_{1:N}", random.Next().ToString(), Guid.NewGuid());
            var msgID = Encoding.UTF8.GetString(md5.ComputeHash(Encoding.UTF8.GetBytes(id)));
            return msgID;
        }
    }
}
