﻿using System;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using Net.Public;
using Newtonsoft.Json;

namespace NetConsole
{
    public class RegionalSample
    {
        private static Random random = new Random();


        public static void SamplePos()
        {

            Console.WriteLine("SAMPLE OFFLINE:");
            Console.ReadKey();

            var partnerId = "679db50f-3943-4fc1-af87-93e075a32869";
            var partnerSecret = "ZRh3iPhLpaAbBANN";
            var merchantId = "6ea846c7-afb8-4a03-8c7c-5ddedda57a88";
            var terminalId = "6ac42887732823c9d9298c868";
            var env = "STAGING";
            var country = "PH";
            string currency = "PHP";

            MerchantIntegrationOffline merchantClient = new MerchantIntegrationOffline(env, country, partnerId, partnerSecret, merchantId, terminalId);
            string partnerTxId = RandomString(32);
            string msgId = RandomString(32);

            Console.WriteLine("partnerTxId: " + partnerTxId);
            Console.WriteLine("msgId: " + msgId);
            Console.Write("Please enter number amount: ");
            long amount = long.Parse(Console.ReadLine());
            var response = merchantClient.PosCreateQRCode(msgId, partnerTxId, amount, currency);

            Console.WriteLine("PosCreateQRCode response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();

            response = merchantClient.PosGetTxnStatus(msgId, partnerTxId, currency);
            Console.WriteLine("PosGetTxnDetails response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
        }

        public static void SampleOnA()
        {

            Console.WriteLine("SAMPLE ONLINE:");
            Console.ReadKey();


            var partnerId = "b597c141-58e6-41c0-a362-027eed96fc81";
            var partnerSecret = "w_iQ9ls106MTxP1V";
            var merchantId = "ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379";
            var clientId = "8888ef0060c24f41929ca41bfc702032";
            var clientSecret = "K6J1aznYAfT8ckyn";
            var env = "STAGING";
            var country = "MY";
            var redirectUrl = "http://172.104.183.204/index.php";
            string currency = "MYR";

            MerchantIntegrationOnline merchantClient = new MerchantIntegrationOnline(env, country, partnerId, partnerSecret, merchantId, clientId, clientSecret, redirectUrl);

            string partnerTxId = RandomString(32);
            string partnerGroupTxId = RandomString(32);
            Console.WriteLine("partnerTxId: " + partnerTxId);
            Console.WriteLine("partnerGroupTxId: " + partnerGroupTxId);
            Console.Write("Please enter number amount:");
            long amount = long.Parse(Console.ReadLine());
            Console.WriteLine("amount: " + amount + " " + currency);

            var state = RandomString(7);
            Console.WriteLine("state: " + state);
            string[] arr = { "INSTALMENT" };
            var response = merchantClient.OnaCreateWebUrl(partnerTxId, partnerGroupTxId, amount, currency, state,hidePaymentMethods:arr);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Please copy url, open it on browser and make a payment by Grab app: "
                   + response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.Write("Please enter code from redirect url: ");
            var code = Console.ReadLine();

            response = merchantClient.OnaOAuth2Token(partnerTxId, code);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            var bodyResp = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);
            var accessToken = bodyResp.access_token.ToString();

            response = merchantClient.OnaChargeComplete(partnerTxId, accessToken);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Charge complete response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            var txId = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result).txID.ToString();
            Console.ReadKey();
            response = merchantClient.OnaGetChargeStatus(partnerTxId, currency, accessToken);
            Console.WriteLine("Get charge status response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.WriteLine("Refund flow");
            Console.ReadKey();
            Console.Write("Please enter number amount: ");
            amount = long.Parse(Console.ReadLine());

            var refundTxId = RandomString(32);
            response = merchantClient.OnaRefund(refundTxId, RandomString(32), amount, currency, txId, "description", accessToken);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DisplayError(response);
            };

            Console.WriteLine("Refund response:");
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
            Console.ReadKey();
            Console.WriteLine("Get refund status response:");
            response = merchantClient.OnaGetRefundStatus(refundTxId, currency, accessToken);
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);

            Console.ReadKey();

        }

        public static void DisplayError(HttpResponseMessage response)
        {
            Console.WriteLine("Something went wrong!!!!");
            Console.WriteLine(response);
            Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghyklmnopqrstuwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateMsgID()
        {
            var random = new Random();
            var md5 = MD5.Create();
            var id = string.Format("{0}_{1:N}", random.Next().ToString(), Guid.NewGuid());
            var msgID = Encoding.UTF8.GetString(md5.ComputeHash(Encoding.UTF8.GetBytes(id)));
            return msgID;
        }
    }
}
