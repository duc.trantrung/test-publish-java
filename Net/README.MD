##### Net solution structure
- MerchantSDK project: .NET library, supporting .NET Standard 2.0+.

#### How to use SDK

1. Add Net library to your solution.
Right click on your project solution. Select `Add` -> `Existing project` -> Select Net.csproj file.
1. Import Net project to your project.
Right click on your project Dependencies. Select `Add Reference` -> Select Net Project.

##### Config
```
    # environment : accept value 'STG','PRD'
    # country: which country you live(VN,SG, ID, MY,...)
    # partnerId: the partnerId from integration team share to merchant
    # partnerSecret: the partnerSecret from integration team share to merchant
    # merchantId: the merchantId from integration team share to merchant
    # terminalId: the terminalId from integration team share to merchant. It need config when use offline transaction
    # clientId: the clientId from integration team share to merchant. It need config when use online transaction
    # clientSecret: the clientSecret from integration team share to merchant. It need config when use online transaction
    # redirectUri: the redirectUri from integration team share to merchant. It need config when use online transaction
```

##### How to configure for online acception
```
using Net.Public;
...
MerchantIntegrationOnline clientOnA = new MerchantIntegrationOnline(env, country, partnerId, partnerSecret,merchantId, clientId, clientSecret, redirectUrl);
```

##### How to configure for POS
```
using Net.Public;
...
MerchantIntegrationOffline clientPos = new MerchantIntegrationOffline(env, country, partnerId, partnerSecret, merchantId, terminalId );
```

##### Detail for API online acception

1. Charge Init: 

```
respChargeInit = clientOnA.OnaChargeInit(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods)
```
2. Create Web Url: 
```
    respCreateWebUrl = clientOnA.OnaCreateWebUrl(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods, state)
```

3. get OAuth Token

```
    respAuthCode = clientOnA.OnaOAuth2Token(partnerTxID,code)
```

4. Charge Complete
```
    respChargeComplete = clientOnA.OnaChargeComplete(partnerTxID,respAuthCode.access_token)
```

5 Get Charge status
```
    respChargeStatus = clientOnA.OnaGetChargeStatus(partnerTxID, currency, respAuthCode.accessToken)
```

6. refund online
```
    response = clientOnA.OnaRefund(refundPartnerTxID, amount, origPartnerTxID, description, respAuthCode.accessToken, currency)
```

7. Get Refund Status
```
    response = clientOnA.OnaGetRefundStatus(partnerTxID, respAuthCode.accessToken, currency)
```

8. Get One time charge status
```
    response = clientOnA.OnaGetOTCStatus(partnerTxID, respAuthCode.accessToken, currency)
```

# Detail for API offline

1. Create QR Code
```
    respCreateQRCode= clientPos.PosCreateQRCode(msgID, partnerTxID, amount, currency)
```

2. Get transaction detail:
```
    resp= clientPos.PosGetTxnStatus(msgID, partnerTxID, currency)
```

3. Cancel a Transaction
```
    resp= clientPos.PosCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency)
```

4. Refund a POS Payment
```
    resp= clientPos.PosRefund(msgID, refundPartnerTxID, amount, currency, origPartnerTxID, description)
```

5. Get refund transaction detail:
```
    resp= clientPos.PosGetRefundDetails(msgID, refundPartnerTxID, currency)
```

6. Perform a Transaction
```
    resp= clientPos.PosPerformQRCode(msgID, partnerTxID, amount, currency, code)
```
