# Thanh toán bằng mã QR hiển thị trên website

### Luồng xử lí

<img src="/merchants/docs/_assets/OnA-qr-code.png" alt="" style="width: 70%"/>


- Bước 1: Khách hàng chọn thanh toán bằng Grab/Moca, và nhấn nút Thanh toán.
- Bước 2: Merchant server [khởi tạo thanh toán](/online-payment/api/init-payment.md), gửi thông tin sang Moca server, nhận về thông tin giao dịch.
- Bước 3: Merchant website tạo [deeplink](/online-payment/api/setup-deeplink.md) và mở deeplink để hiển thị QR code.
- Bước 4: Khách hàng bật ứng dụng Grab, dùng tính năng QR của Moca để quét mã QR và thanh toán. Sau khi khách hàng thanh toán thành công, web browser sẽ tự động chuyển sang redirect url để hiển thị kết quả giao dịch.
- Bước 5: Merchant server [hoàn thành thanh toán](/online-payment/api/complete-payment.md) và Merchant website hiển thị thông tin giao dịch cho khách hàng.