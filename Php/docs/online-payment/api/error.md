# Bảng mã lỗi 
Đây là danh sách các lý do có thể xảy ra khi bạn kiểm tra trạng thái giao dịch. Danh sách này có thể được cập nhật theo thời gian. Do đó, điều quan trọng là hệ thống của bạn phải xử lý bất kỳ mã lý do không xác định nào trong phản hồi không được đề cập trong tài liệu này.

| Reason Code            | Description                                                                                               |
| ---------------------- | --------------------------------------------------------------------------------------------------------- |
| currency_mismatch      | Đơn vị tiền tệ được sử dụng trong yêu cầu này không khớp với cấu hình người bán.                          |
| no_record_found        | Không tìm thấy giao dịch này.                                                                             |
| authorising            | Hệ thống đang xử lý yêu cầu cho việc ủy quyền giao dịch.                                                  |
| capturing              | Hệ thống đang xử lý yêu cầu cho việc xử lí giao dịch.                                                     |
| user_fail_consent      | Người dùng đã không chấp thuận giao dịch trong thời gian cho phép.                                        |
| pending_user_consent   | Giao dịch vẫn đang chờ người dùng thanh toán.                                                             |
| pending_capture        | Giao dịch vẫn đang chờ request /charge/complete để hoàn tất thanhh toán.                                  |
| cancelling             | Giao dịch đang trong quá trình bị hủy.                                                                    |
| auth_expired           | Giao dịch bị hủy do giao dịch không được hoàn tất với request /charge/complete  trong thời gian cho phép. |
| unknown                | Giao dịch vẫn đang được xử lý, tuy nhiên không thể xác định lý do của việc này.                           |
| kyc_compliance_decline | Giao dịch không thành công do kiểm tra tuân thủ quy định.                                                 |
| transaction_decline    | Giao dịch đã bị từ chối.                                                                                  |
| insufficient_balance   | Không có đủ số dư trong ví của người dùng để hoàn tất giao dịch.                                          |