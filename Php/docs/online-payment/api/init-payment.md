# Khởi tạo thanh toán

#### Endpoint
Method: ```POST```

URL: ```https://{{api-endpoint}}/mocapay/partner/v2/charge/init```

#### Header Parameters 
| Parameter     | Type                    | Description                                                                                                                                                 |
| ------------- | ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Authorization | HMAC signature          | **Bắt buộc**. [Chữ kí HMAC](/online-payment/api/authentication.md) đã được tạo ra từ trước. <br>Example: `‘Authorization’:’partner_id:hmac_signature’` |
| Date          | GMT formatted date time | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                                 |
| Content-Type  | string                  | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                                                                      |

#### Request Parameters

| Parameter        | Type                            | Description                                                                                                                                                                                                                           |
| ---------------- | ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| partnerGroupTxID | string (max length = 32 chars)  | **Bắt buộc**. Order_id. Là số nhận dạng duy nhất cho mỗi giao dịch được tạo ra bởi đối tác.                                                                                                                                           |
| partnerTxID      | string (max length = 32 chars)  | **Bắt buộc**. partner_transaction_id, được sử dụng làm khoá tối ưu của request. Nó phải giống với partner_transaction_id được sử dụng khi tạo chữ ký                                                                                  |
| amount           | int64                           | **Bắt buộc**. Số tiền giao dịch.                                                                                                                                                                                                      |
| currency         | string (enum)                   | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại chỉ hỗ trợ `VND`.                                                                                                                                                                               |
| description      | string (max length = 255)       | Không bắt buộc. Mô tả của thanh toán.                                                                                                                                                                                                  |
| isSync           | boolean (default: false)        | Không bắt buộc. Chỉ định liệu Moca có nên trả lại trạng thái giao dịch cuối cùng ngay lập tức hay không. Để biết thêm thông tin về cách xử lý trạng thái giao dịch không xác định, hãy xem [Xử lý trạng thái giao dịch không xác định](#handling-unknown-transaction-status). |
| merchantID       | string (value provided by Moca) | **Bắt buộc**. Chỉ định tài khoản người bán để ghi có khi giao dịch thành công. Mỗi đối tác có thể có nhiều tài khoản người bán được liên kết với tài khoản đối tác. Tài khoản người bán dành riêng cho một loại tiền tệ.              |
| metaInfo         | array of object                 | Không bắt buộc. [Thông tin chi tiết](#request-parameter-meta-info)                                                                                                                                                                    |
| items            | array of object                 | Không bắt buộc. [Thông tin chi tiết](#request-parameter-items)                                                                                                                                                                        |

#### Response Parameters
| Parameter   | Type                           | Description                                                                                                                                                                                                                                                             |
| ----------- | ------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| partnerTxID | string (max length = 32 chars) | `partner_transaction_id` được sử dụng như `idempotency key` cho việc gọi các API tiếp theo.                                                                                                                                                                             |
| request     | string                         | Đây là `transaction_id` được tạo bởi Moca                                                                                                                                                                                                                               |
| txStatus    | string                         | Trả về một trong 2 trạng thái sau:  ```success``` ```failed``` <br>Tham khảo bảng Trạng thái giao dịch để biết danh sách tất cả các trạng thái giao dịch có thể có và mô tả của chúng.                                                                                  |
| reason      | string                         | Tham khảo bảng `reason Code` để biết mô tả của chúng. Lưu ý rằng `reason code` là để biết thêm thông tin về khoản thanh toán. Nó là một phần tử phản hồi tùy chọn. Trong một số trường hợp nhất định, 'reason code' có thể để trống. Ví dụ, khi `txStatus = “success”`. |

#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                          | Action                   |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------ |
| 200 OK                    | Giao dịch đã được khởi tạo thành công. Moca đã nhận được chi tiết giao dịch và tạo `transaction_id` cho giao dịch này                                           | -                        |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format                                           | Sửa lỗi là thao tác lại  |
| 401 Unauthorized          | Thông tin xác thực API không chính xác. Có thể là do chữ kí HMAC chưa đúng                                                                                      | Sửa lỗi là thao tác lại  |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                  | Sửa lỗi là thao tác lại. |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột. | Sửa lỗi là thao tác lại. |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                        | Thao tác lại             |


#### Reason Codes
Đây là danh sách các `reason code` có thể được phản hồi khi nhận được một HTTP response lỗi.

| Reason                     | Description                                                                                                 |
| -------------------------- | ----------------------------------------------------------------------------------------------------------- |
| currency_mismatch          | Đơn vị tiền tệ không khớp với đơn vị tiền tệ được chỉ định của `merchant_id`.                               |
| invalid_merchant           | `merchant_id` không hợp lệ. Nếu bạn vẫn gặp vấn đề này, vui lòng liên hệ Moca support team để kiểm tra lại. |
| transaction_already_exists | `partnerTxID` đã tồn tại trong hệ thống Moca. Bạn phải tạo một `partnerTxID` duy nhất cho mỗi giao dịch     |
| client_error               | Có một số lỗi khi xử lý yêu cầu này do một số tham số chưa đúng. Vui lòng kiểm tra lại.                     |


#### Thông tin khác

###### Request Parameter - Meta Info
Không bắt buộc. Thông tin bổ sung liên quan đến giao dịch hiện tại:
- brandName: Tên thương hiệu để hiển thị trong luồng giao dịch.
- location: Thông tin vị trí nơi giao dịch được khởi tạo.
  - ipAddress (string): Địa chỉ IP trên thiết bị.
  - longitude (double): Kinh độ của vị trí của người dùng hiện tại.
  - latitude (double): Vĩ độ của vị trí của người dùng hiện tại.
  - accuracy (double): Mức độ chính xác của vĩ độ và kinh độ, tính bằng mét.
- device: Thiết bị được sử dụng để bắt đầu giao dịch.
  - deviceID: Số nhận dạng thiết bị duy nhất.
  - deviceModel: Kiểu thiết bị.
  - deviceBrand: Thương hiệu của thiết bị.
  - iosUDID: UDID của thiết bị, chỉ định cho các thiết bị IOS.
  - imei: IMEI của thiết bị, chỉ định cho thiết bị Android.
- subMerchant: Thông tin chi tiết về người bán cung cấp các mặt hàng.
  - name: Thông tin bên bán.
  - category: danh mục của người bán.
- partnerUserInfo: Thông tin của đối tác
  - Id: Mã định danh của đối tác
  - Phone: Số điện thoại của đối tác
  - Email: Email của đối tác
- echo: Tham chiếu của chính người bán, nó sẽ được trả lại cho ngừời bán qua webhook
  
**Example:**
```json
{ 
 
    "metaInfo": { 
        "brandName":"This is my brand name",
        "location": { 
            "ipAddress":"127.0.0.1", 
            "longitude":119.4466848, 
            "latitude":-6.403699, 
            "accuracy":15.759 
        }, 
        "device": { 
            "deviceID":"abc123",
            "deviceModel":"iPhone 6 Plus",  
            "deviceBrand":"APPLE", 
            "iosUDID":"C285XXXX-XXXX-43F8-8474-7A516 B9XXXX", 
            "imei": "866261035868851" 
        }, 
        "subMerchant": { 
            "name":"this is my submerchant name",  
            "category":"Food and Beverage"  
        },
        "partnerUserInfo": {
            "id": "1234567",
            "phone": "6585557771",
            "email": "abc@gmail.com"
        }

    }
}

```

##### Request Parameter - Items
Không bắt buộc. Các mặt hàng có trong giao dịch: 
- supplierName (string): Tên của nhà cung cấp đang cung cấp mặt hàng.
- itemName (string): Tên và mô tả của mặt hàng.
- category (string): [Chỉ dành cho khả năng tương thích ngược] Danh mục của mặt hàng. Sử dụng thông số itemCategory để thay thế.
- quantity (integer): Số lượng mặt hàng đã mua.
- price (integer): Giá mỗi mặt hàng. Nếu giá trị này được cung cấp, giá trị được tính toán phải khớp với tổng số tiền.
- itemCategory (enumerated value): Danh sách chuyên mục. Xem [bảng dưới đây](#category-list) để biết danh sách đầy đủ các danh mục.
- url (string): URL mặt hàng tại trang web người bán.
- imageURL (string): URL hình ảnh của mặt hàng tại trang web người bán.
- supplierDetails: 
    - supplierID (string): Định danh duy nhất của nhà cung cấp.
    - supplierName (string): Tên của nhà cung cấp đang cung cấp mặt hàng.
    - supplierURL (string): URL của nhà cung cấp.
    - legalID (string): Mã nhận dạng duy nhất hợp pháp của nhà cung cấp.
    - supplierPhone (string): Số điện thoại nhà cung cấp
    - supplierEmail (string): Email của nhà cung cấp.
    - supplierAddress (string): Địa chỉ của nhà cung cấp.
    - supplierRating (double) Đánh giá của nhà cung cấp.

```json
{
    "items":[ 
        { 
            "supplierName":"Happy Shop", 
            "itemName":"USB Drive", 
            "category":"Electronic Device",  
            "quantity":1, 
            "price":30000, 
            "itemCategory": 10,
            "url": "url", 
            "imageURL": "imageURL", 
            "supplierDetails": { 
                "supplierID": "supplerID1",  
                "supplierName": "supplierName1",  
                "supplierUrl": "supplierUrl1",  
                "legalID": "legalID1", 
                "supplierPhone": "supplierPhone1",  
                "supplierEmail": "supplierEmail1",  
                "supplierAddress": "supplierAddress1", 
                "supplierRating": 123 
            }
        }, 
        { 
            "supplierName":"Another Shop",  
            "itemName":"Milk", 
            "category":"Beverage", 
            "quantity":1, 
            "price":5000, 
            "itemCategory": 4, 
            "url": "url2", 
            "imageURL": "imageURL2", 
            "supplierDetails": { 
                "supplierID": "supplerID2",  
                "supplierName": "supplierName2",  
                "supplierUrl": "supplierUrl2",  
                "legalID": "legalID2", 
                "supplierPhone": "supplierPhone2",  
                "supplierEmail": "supplierEmail2",  
                "supplierAddress": "supplierAddress2", 
                "supplierRating": 456 
            } 
        } 
    ]
}
```

##### Category List

| Enum value | Category name                 |
| ---------- | ----------------------------- |
| 1          | Men's Wear                    |
| 2          | Women's Apparel               |
| 3          | Men's Bag                     |
| 4          | Women's Bag                   |
| 5          | Men's Shoes                   |
| 6          | Women's Shoes                 |
| 7          | Mobile & Gadgets              |
| 8          | Beauty & Personal Care        |
| 9          | Home Appliances               |
| 10         | Home & Living                 |
| 11         | Kids Fashion                  |
| 12         | Toys, Kids & Babies           |
| 13         | Video Games                   |
| 14         | Food & Beverages              |
| 15         | Computers & Peripherals       |
| 16         | Collectibles, Hobbies & Books |
| 17         | Health & Wellness             |
| 18         | Travel & Luggage              |
| 19         | Tickets & Vouchers            |
| 20         | Watches                       |
| 21         | Jewellery & Accessories       |
| 22         | Sports & Outdoors             |
| 23         | Automotives                   |
| 24         | Cameras & Drones              |
| 25         | Pet Food & Accessories        |
| 26         | Airtime                       |
| 27         | Credit Bill                   |
| 28         | Other Bill                    |
| 29         | Miscellaneous                 |

#### Handling Unknown Transaction Status
Một số giao dịch có thể mất nhiều thời gian để xử lí. Nếu bạn không set `isSync=true` khi khởi tạo thanh toán, thì API hoàn thành thanh toán có thể trả về giá trị trạng thái trung gian, ví dụ: `status=unknown` trong khi giao dịch vẫn đang xử lí. Sau một thời gian trạng thái `unknown` sẽ chuyển thành một trong những trạng thái giao dịch cuối cùng, ví dụ: `success/failed`. Hãy đảm bảo hệ thống của bạn chỉ dựa vào trạng thái `success`, hoặc `fail` làm trạng thái giao dịch cuối cùng.

Bạn có thể xử lí trạng thái `unknown` bằng cách sau:
- Nếu doanh nghiệp của bạn nhạy cảm về việc thời gian, bạn muốn nhận được trạng thái giao dịch cuối cùng ngay lập tức, hãy chỉ định `isSync=true` khi khởi tạo thanh toán. Tuy nhiên, nếu bạn muốn hoàn tiền giao dịch, bạn vẫn cần phải xử lí trạng thái `unknown`.
- Nếu doanh nghiệp của bạn có đủ thời gian xử lí giao dịch, khi nhận được trạng thái giao dịch `unknown`, vui lòng chờ đến khi nhận được notification từ webhook trả về. Ngoài ra bạn có thể gọi API kiểm tra trạng thái thanh toán để biết trạng thái hiện tại của giao dịch (vui lòng không chạy api này nhiều hơn 2 phút/lần)
- Tuy nhiên, trong một số trường hợp hiếm gặp, bạn vẫn có thể nhận được trạng thái đang xử lí ngay cả khi `isSync=true`. Bạn có thể gọi API kiểm tra trạng thái thanh toán để biết trạng thái hiện tại của giao dịch (vui lòng không chạy api này nhiều hơn 2 phút/lần)

Lưu ý: Nếu bạn tích hợp tính năng hoàn tiền cho khách hàng, bạn vẫn cần phải xử lí trạng thái `unknown` của API hoàn tiền. Thuộc tính `isSync` không được hỗ trợ trong trường hợp này.