# Thông báo

Moca sẽ tự động gửi thông báo về mọi trạng thái giao dịch bằng cách sử dụng webhook.
Đảm bảo xác minh tính hợp lệ của thông báo đã nhận. Bạn có thể so sánh chữ ký HMAC từ `Authorization header` với chữ ký bạn đã tạo khi khởi tạo thanh toán.

#### Header Parameters
| Parameter     | Type                    | Description                                                                                                                                     |
| ------------- | ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| Authorization | Bearer Token            | Moca gửi `Authorization` với định dạng : `partner_id:hmac_signature`                                                                            |
| Date          | GMT formatted date time | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                     |
| Content-Type  | string                  | **Bắt buộc**. Moca sẽ gửi kiểu dữ liệu `application/json`. Tuy nhiên, khi bạn tính HMAC, hãy sử dụng giá trị `application/json; charset=utf-8 ` |


#### Body of the Webhook
| Parameter   | Type                           | Description                                                                                                                                                                                                                                   |
| ----------- | ------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txType      | string                         | Giai đoạn của giao dịch: `Capture/Refund/Auth/Init`. [Chi tiết](#txtype-for-webhook)                                                                                                                                                                                            |
| partnerTxID | string (max length = 32 chars) | partner_transaction_id của sự kiện                                                                                                                                                                                                            |
| txID        | string                         | Moca transactionID, a 32-bit UUID formatted string.                                                                                                                                                                                           |
| origTxID    | string                         | reference_transaction_ID, giao dịch ban đầu. Ví dụ: nếu đây là sự kiện hoàn lại tiền, `origTxID` sẽ là transaction_id thanh toán ban đầu. Nếu không có giao dịch nào liên quan đến giao dịch gốc, thì đây là một chuỗi trống. |
| amount      | int64                          | Số tiền giao dịch.                                                                                                                                                                                                                             |
| currency    | string                         | Đơn vị tiền tệ.                                                                                                                                                                                                                                |
| status      | string                         | Thông báo trạng thái của giao dịch ở giai đoạn này: `success/failed`                                                                                                                                                                          |
| completedAt | int64                          | Thời gian khi giao dịch hoàn tất ở định dạng Unix timestamp                                                                                                                                                                                   |
| createdAt   | int64                          | Thời gian tạo giao dịch ở định dạng Unix timestamp                                                                                                                                                                                   |
| payload     | jsonRaw                        | [Thông tin bổ sung](#payload-for-webhook)                                                                                                                                                                                                     |
| reason      | string                         | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`.                                   |


#### Sample Request Sent From Moca
``` curl
curl -X POST https://partner-gw.moca.vn \ 
-H 'Authorization: {{ signed_hmac }}' 
-H 'Content-Type: application/json; charset=utf-8' \ 
-H 'Date: Thu, 13 Sep 2018 08:49:37 GMT' \ 
-d '{ \ 
"txType":"Init" \
"partnerID":"{{partner ID}}",  \
"partnerTxID":"{{ idempotency_key }}", \
"txID":"{{ idempotency_key_from_moca }}", \
"origTxID":"caf9ce233b43407baa6f7a7524dfc186", \
"Amount":60000, \
"currency":"VND", \
"Status":"failed", \
"createdAt":1627022068, \
"completedAt":1627022117, \
"Payload":
{"partnerGroupTxID":"{{parnterGroupTxID}}","newStatus":"cancelled","reason":"user_fail_consent","paymentTypeID":84266149,"echo":"field specified in echo request"}} \

```

#### Sample Response 
```json
{ 
 "txID":"66f3f271cc994f1bb1e46e6fe4fd8727", 
 "status":"success", 
 "description":"" 
}
```

#### Payload for Webhook
| Parameter        | Type    | Description                                                                                                                                                                                                 |
| ---------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| partnerGroupTxID | string  | partnerGroupTxID đã được chuyển đến Moca trong quá trình giao dịch                                                                                                                                          |
| newStatus        | string  | Tham khảo bảng Trạng thái giao dịch để biết danh sách tất cả các trạng thái giao dịch có thể có và mô tả của chúng.                                                                                         |
| reason           | string  | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |
| echo             | string  | Tham chiếu người bán được chuyển đến Moca trong quá trình giao dịch.                                                                                                                                        |
| rewardMeta       | jsonRaw | Xác định thông tin liên quan đến phần thưởng và thông tin này chỉ được gửi lại trong trường hợp khuyến mại được tài trợ từ phía người bán                                                                   |

#### rewardMeta Details
| Parameter     | Type   | Description                                         |
| ------------- | ------ | --------------------------------------------------- |
| offerCode     | string | Mã khuyến mại được sử dụng trong giao dịch.         |
| offerName     | string | Tên của mã khuyến mại được sử dụng trong giao dịch. |
| offerDiscount | int64  | Tổng chiết khấu cho người dùng trong giao dịch.     |

#### txType for Webhook
| txType  | description                                                                                                                          |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| Init    | Người dùng khởi tạo giao dịch                                                                                                        |
| Auth    | Moca đã giữ lại số tiền giao dịch sau khi người dùng thực hiện thanh toán. Một txStatus thành công sẽ được nhận nếu Auth thành công. |
| Capture | Moca đã ghi lại số tiền và đã chuyển nó vào ví của người bán.                                                                        |
| Cancel  | Giao dịch xác thực đang bị hủy vì người bán không thu được số tiền xác thực.                                                         |
| Refund  | Thanh toán đang được hoàn lại.                                                                                                       |