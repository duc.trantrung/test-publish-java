# Kiểm tra trạng thái thanh toán

Có 2 endpoint để kiểm tra trạng thái giao dịch:
  - `One Time Charge Status` endpoint, được xác thực bằng chữ kí HMAC.
  - `Partner Charge Status` endpoint, được xác thực bằng OAuth token.

Có 2 điểm khác biệt chính giữa `One Time Charge Status` endpoint và `Partner Charge Status` endpoint là:

1. `One Time Charge Status` có thể được gọi bất kì lúc nào, sau khi bạn hoàn thành khởi tạo thanh toán. Còn đối với `Partner Charge Status `endpoint, bạn cần người dùng đồng ý thanh toán và lấy mã OAuth access token trước khi truy cập endpoint này.
2. `One Time Charge Status` endpoint trả về mã `code` trong response, mã `code` có thể sử dụng để lấy OAuth token trong bước [lấy OAuth Token](/online-payment/api/authentication?id=lấy-oauth-token) `Partner Charge Status `endpoint không có tính năng này.

### One time charge Status Endpoint
Kiểm tra trạng thái thanh toán cho luồng `One Time Charge`. Đảm bảo bạn cung cấp `partner_transaction_id` và `currency` mà bạn đã cung cấp tại API ```/charge/init```. Nếu không, API sẽ trả về lỗi `HTTP 404`.

Endpoint này có thể sử dụng cùng webhook để partner server truy vấn OAuth access token và hoàn thành thanh toán.

Khi người dùng đã thanh toán, merchants server có thể nhận được webhook notification về việc đó. Và merchant có thể gọi `One Time Charge Status` endpoint để nhận `oAuthCode`, lấy OAuth token và hoàn thành thanh toán.

#### Endpoint
Method: ```GET```

URL: ```https://{{api-endpoint}}/mocapay/partner/v2/one-time-charge/{partnerTxID}/status?currency={currency}```

#### Header Parameters 
| Parameter     | Type                           | Description                                                                                                                                             |
| ------------- | ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Authorization | HMAC Signature                 | **Bắt buộc**. [Chữ kí HMAC](/online-payment/api/authentication.md) đã được tạo ra từ trước. <br>Example: `‘Authorization’:’partner_id:h_mac_signature’` |
| Date          | date                           | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                             |
| Content-Type  | string                         | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                                                                      |
| partnerTxID   | string (max length = 32 chars) | **Bắt buộc**. partnerTxID được sử dụng tại ```charge/init``` endpoint.                                                                                  |
| currency      | Three letter ISO currency code | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại chỉ hỗ trợ `VND`.                                                                                                |

#### Response Elements

| Parameter | Type   | Description                                                                                                                                                                                                 |
| --------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txID      | string | `A 32-bit UUID formatted Moca transaction ID`. Để tiến hành hoàn tiền cho giao dịch này, bạn cần sử dụng ID này và chạy API hoàn tiền.                                                                      |
| oAuthCode | string | Mã oAuthCode dùng để người bán lấy mã oAuth access token và cuối cùng hoàn tất thanh toán.                                                                                                                  |
| status    | string | (không còn hỗ trợ, vui lòng sử dụng txStatus). Các trạng thái có thể: `“success” / “failed” / “unknown”`                                                                                                    |
| txStatus  | string | Các trạng thái có thể: ```Success/Failed/Processing/Transaction_already_exist```  <br>Thông tin chi tiết                                                                                                    |
| reason    | string | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |

#### Sample API Request
```curl
curl -X GET 
https://partner-gw.moca.vn/mocapay/partner/v2/one-time-charge/{partnerTxID}/status?currency=VND \ 
-H 'Authorization:partner_id:hmac_signature' 
-H 'Content-Type: application/json' \ 
-H 'Date: Thu, 13 Sep 2018 08:49:37 GMT' \
```

#### Sample API Response
```json
{
    "txID": "1234567abcdefghijklmnd0123456789",
    "oAuthCode": "57498632557928629184521",
    "status": "unknown",
    "txStatus": "authorised",
    "reason": "pending_capture"
}
```
#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                          | Action                  |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| 200 OK                    | API request được xử lý thành công.                                                                                                                              | -                       |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format.                                          | Sửa lỗi là thao tác lại |
| 401 Unauthorized          | Thông tin xác thực API không chính xác.                                                                                                                         | Sửa lỗi là thao tác lại |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                  | Sửa lỗi là thao tác lại |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột. | Sửa lỗi là thao tác lại |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                        | -                       |

### Partner Charge Status endpoint
Kiểm tra trạng thái thanh toán sử dụng `oAuth access token`. Đảm bảo bạn cung cấp `partner_transaction_id` và `currency` mà bạn đã cung cấp tại API ```/charge/init```. Nếu không, API sẽ trả về lỗi `HTTP 404`.

#### Endpoint
Method: ```GET```

URL: ```https://{{api-endpoint}}/mocapay/partner/v2/charge/{partnerTxID}/status?currency={currency}```

#### Header Parameters 
| Parameter     | Type                           | Description                                                                                           |
| ------------- | ------------------------------ | ----------------------------------------------------------------------------------------------------- |
| Authorization | Bearer Token                   | **Bắt buộc**. Header required for authorization purpose. Ví dụ:`Authorization: Bearer {access_token}` |
| X-GID-AUX-POP | token                          | **Bắt buộc**. Chữ ký HMAC được tính bằng mã truy cập oAuth.                                           |
| Date          | date                           | **Bắt buộc**. Ngày và giờ ở định dạng GMT. Ví dụ: `Fri, 14 Sep 2018 08:29:58 GMT`                     |
| Content-Type  | string                         | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                    |
| partnerTxID   | string (max length = 32 chars) | **Bắt buộc**. partnerTxID được sử dụng tại ```charge/init``` endpoint.                                |
| currency      | Three letter ISO currency code | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại chỉ hỗ trợ `VND`.                                              |

#### Response Elements

| Parameter | Type   | Description                                                                                                                                                                                                 |
| --------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txID      | string | `A 32-bit UUID formatted Moca transaction ID`. Để tiến hành hoàn tiền cho giao dịch này, bạn cần sử dụng ID này và chạy API hoàn tiền.                                                                      |
| oAuthCode | string | Mã oAuthCode dùng để người bán lấy mã oAuth access token và cuối cùng hoàn tất thanh toán.                                                                                                                  |
| status    | string | (không còn hỗ trợ, vui lòng sử dụng txStatus). Các trạng thái có thể: `“success” / “failed” / “unknown”`                                                                                                    |
| txStatus  | string | Các trạng thái có thể: ```Success/Failed/Processing/Transaction_already_exist```  <br>Thông tin chi tiết                                                                                                    |
| reason    | string | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |

#### Sample API Request
```curl
curl -X GET 
https://partner-gw.moca.vn/mocapay/partner/v2/charge/{partnerTxID}/statu s?currency=VND \ 
-H 'Authorization: Bearer {{ oAuth2_token }}' 
-H 'X-GID-AUX-POP: {{ POP_signature }}' 
-H 'Content-Type: application/json' \ 
-H 'Date: Thu, 13 Sep 2018 08:49:37 GMT' \
```
#### Sample Response
```json
{ 
 "txID":"66f3f271cc994f1bb1e46e6fe4fd8727", 
 "status":"success", 
 "description":"",
 "txStatus":"success"
}
```

#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                          | Action                  |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| 200 OK                    | API request được xử lý thành công.                                                                                                                              | -                       |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format.                                          | Sửa lỗi là thao tác lại |
| 401 Unauthorized          | Thông tin xác thực API không chính xác.                                                                                                                         | Sửa lỗi là thao tác lại |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                  | Sửa lỗi là thao tác lại |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột. | Sửa lỗi là thao tác lại |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                        | -                       |
