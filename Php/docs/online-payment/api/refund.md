# Hoàn tiền

### Điều kiện cần có
Đảm bảo bạn đã chạy `/charge/complete` API endpoint. Bạn cần lưu lại transaction_id từ endpoint đó.

#### Endpoint
Method: `POST`

URL: `https://{{api-endpoint}}/mocapay/partner/v2/refund`

#### Header Parameters 
| Parameter     | Type         | Description                                                                                           |
| ------------- | ------------ | ----------------------------------------------------------------------------------------------------- |
| Authorization | Bearer Token | **Bắt buộc**. Header required for authorization purpose. Ví dụ:`Authorization: Bearer {access_token}` |
| X-GID-AUX-POP | token        | **Bắt buộc**. Chữ ký HMAC được tính bằng mã truy cập oAuth.                                           |
| Date          | Date         | **Bắt buộc**. Ngày và giờ ở định dạng GMT. Ví dụ: `Fri, 14 Sep 2018 08:29:58 GMT`                     |
| Content-Type  | string       | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                    |

#### Request Parameters
| Parameter        | Type                            | Description                                                                                                                                                                                                              |
| ---------------- | ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| partnerGroupTxID | string (max length = 32 chars)  | **Bắt buộc**. Order_id. Là số nhận dạng duy nhất cho mỗi giao dịch được tạo ra bởi đối tác.                                                                                                                              |
| partnerTxID      | string (max length = 32 chars)  | **Bắt buộc**. partner_transaction_id, được sử dụng làm khoá tối ưu của request. Nó phải giống với partner_transaction_id được sử dụng khi tạo chữ ký                                                                     |
| amount           | int64                           | **Bắt buộc**. Số tiền giao dịch.                                                                                                                                                                                         |
| currency         | string (enum)                   | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại chỉ hỗ trợ `VND`                                                                                                                                                                  |
| description      | string (max length = 255)       | Không bắt buộc. Mô tả của thanh toán                                                                                                                                                                                     |
| merchantID       | string (value provided by Moca) | **Bắt buộc**. Chỉ định tài khoản người bán để ghi có khi giao dịch thành công. Mỗi đối tác có thể có nhiều tài khoản người bán được liên kết với tài khoản đối tác. Tài khoản người bán dành riêng cho một loại tiền tệ. |
| originTxID       | string                          | **Bắt buộc**. Giá trị TxID từ response của `/charge` endpoint. Ví dụ: nếu đây là sự kiện hoàn lại tiền, origTxID sẽ là transaction_id của API hoàn thành thanh toán.                                                                                                                                                         |

#### Response Elements
| Parameter   | Type   | Description                                                                                                                                                                                                 |
| ----------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txID        | string | `A 32-bit UUID formatted Moca transaction ID`.                                                                                                                                                              |
| status      | string | (không còn hỗ trợ, vui lòng sử dụng txStatus). Các trạng thái có thể: `“success” / “failed”`                                                                                                                |
| description | string | Nếu giao dịch thành công, description sẽ trống. Nếu giao dịch không thành công, API sẽ cung cấp mô tả về lỗi. Ví dụ: `refund_exceeds_charge_amount`.                                                        |
| txStatus    | string | Các trạng thái có thể: ```Success/Failed/Processing/Transaction_already_exist```  <br>[Thông tin chi tiết](#transaction-status-for-refunds)                                                                                                    |
| reason      | string | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |

#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                          | Action                  |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| 200 OK                    | API request được xử lý thành công.                                                                                                                              | -                       |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format.                                          | Sửa lỗi là thao tác lại |
| 401 Unauthorized          | Thông tin xác thực API không chính xác.                                                                                                                         | Sửa lỗi là thao tác lại |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                  | Sửa lỗi là thao tác lại |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột. | Sửa lỗi là thao tác lại |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                        | -                       |


#### Reason Codes for Refund Errors
Đây là danh sách các `reason code` có thể được phản hồi khi nhận được một HTTP response lỗi.

| Reason                           | Description                                                                                                 |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| concurrent_refunds_not_supported | Giao dịch hoàn lại tiền cho partnerTxID này vẫn đang được xử lý.                                            |
| invalid_merchant                 | `merchant_id` không hợp lệ. Nếu bạn vẫn gặp vấn đề này, vui lòng liên hệ Moca support team để kiểm tra lại. |
| payment_not_found                | originTxID không thuộc về bất kì khoản thanh toán nào đã sẵn sàng để hoàn tiền lại                          |
| client_error                     | Có một số lỗi khi xử lý yêu cầu này do một số tham số chưa đúng. Vui lòng kiểm tra lại.                     |

### Kiểm tra trạng thái hoàn tiền

#### Endpoint
Method: `GET`

URL: `https://{{api-endpoint}}/mocapay/partner/v2/refund/{partnerTxID}/status?currency={currency}`

#### Header Parameters 
| Parameter     | Type                           | Description                                                                                           |
| ------------- | ------------------------------ | ----------------------------------------------------------------------------------------------------- |
| Authorization | Bearer Token                   | **Bắt buộc**. Header required for authorization purpose. Ví dụ:`Authorization: Bearer {access_token}` |
| X-GID-AUX-POP | token                          | **Bắt buộc**. Chữ ký HMAC được tính bằng mã truy cập oAuth.                                           |
| Date          | Date                           | **Bắt buộc**. Ngày và giờ ở định dạng GMT. Ví dụ: `Fri, 14 Sep 2018 08:29:58 GMT`                     |
| Content-Type  | string                         | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                    |
| partnerTxID   | string (max length = 32 chars) | **Bắt buộc**. partnerTxID được sử dụng tại API hoàn tiền.                                             |
| currency      | Three letter ISO currency code | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại chỉ hỗ trợ `VND`.                                              |


#### Response Elements
| Parameter   | Type   | Description                                                                                                                                                                                                 |
| ----------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txID        | string | `A 32-bit UUID formatted Moca transaction ID`.                                                                                                                                                              |
| status      | string | (không còn hỗ trợ, vui lòng sử dụng txStatus). Các trạng thái có thể: `“success” / “failed”`                                                                                                                |
| description | string | Nếu giao dịch thành công, description sẽ trống. Nếu giao dịch không thành công, API sẽ cung cấp mô tả về lỗi. Ví dụ: `refund_exceeds_charge_amount`.                                                        |
| txStatus    | string | Các trạng thái có thể: ```Success/Failed/Processing/Transaction_already_exist```  <br>[Thông tin chi tiết](#transaction-status-for-refunds)                                                                                                    |
| reason      | string | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |

#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                          | Action                  |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------- |
| 200 OK                    | API request được xử lý thành công.                                                                                                                              | -                       |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format.                                          | Sửa lỗi là thao tác lại |
| 401 Unauthorized          | Thông tin xác thực API không chính xác.                                                                                                                         | Sửa lỗi là thao tác lại |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                  | Sửa lỗi là thao tác lại |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột. | Sửa lỗi là thao tác lại |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                        | -                       |



#### Sample API Request
```curl
curl -X GET 
https://partner-gw.moca.vn/moca/partner/v2/refund/{partnerTxID}/status?c urrency=VND \
-H 'Authorization: Bearer {{ oAuth2_token }}' 
-H 'X-GID-AUX-POP: {{ POP_signature }}' 
-H 'Content-Type: application/json' \ 
-H 'Date: Thu, 13 Sep 2018 08:49:37 GMT' \
```


#### Sample Response
```json
{ 
 "txID":"66f3f271cc994f1bb1e46e6fe4fd8727", 
 "status":"success", 
 "description":"" 
}
```

#### Transaction Status for Refunds
| txStatus                  | Description                                                                                                                                                                                                   |
| ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| success                   | Hành động thanh toán kết thúc thành công                                                                                                                                                                      |
| failed                    | Hành động thanh toán kết thúc thất bại                                                                                                                                                                        |
| processing                | Hệ thống đang xử lý giao dịch này, hãy đợi thông báo từ webhook. Ngoài ra, bạn có thể chạy API kiểm tra trạng thái biết trạng thái hiện tại của giao dịch. Tuy nhiên, đừng chạy API nhiều hơn 2 phút một lần. |
| transaction_already_exist | Hoạt động hiện tại (ví dụ: xác nhận thanh toán) đối với partnerTxID hoặc txID này đã tồn tại. Bạn có thể chạy API trạng thái kiểm tra để có được trạng thái hiện tại.                                         |