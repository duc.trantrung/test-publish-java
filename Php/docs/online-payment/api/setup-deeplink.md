# Deep link 

Deeplink giúp khởi chạy Grab app trực tiếp từ Merchant app hoặc từ Merchant website. Bạn có thể dùng deeplink để mở ứng dụng Grab và kích hoạt dịch vụ thanh toán. Trước khi thực hiện bước này, hãy đảm bảo rằng bạn có thể tạo chữ ký HMAC và Khởi tạo thanh toán thành công.

### Tạo deeplink cho Merchant Mobile App
- Bước 1: Cài đặt Grab Platform iOS/Android SDK
- Bước 2: Xử lí khi người dùng click vào "Thanh toán bằng Moca":
  - Tạo chữ kí HMAC và khởi tạo thanh toán 
  - Tạo deeplink với sự trợ giúp của SDK
  - Sử dụng deeplink để chuyển sang ứng dụng Grab
- Bước 3: Sau khi thanh toán thành công, người dùng sẽ được quay về ứng dụng của bạn. Lưu ý: để có thể quay về ứng dụng, bạn cần đăng kí deeplink tại developer home. 


### Tạo deeplink cho Mobile Browser
- Bước 1: Xử lí khi người dùng click vào "Thanh toán bằng Moca":
  - Tạo chữ kí HMAC và khởi tạo thanh toán
  - Khởi tạo Web URL
  - Sử dụng web URL để chuyển sang ứng dụng Grab.
- Bước 2: Sau khi thanh toán thành công, người dùng sẽ được quay về confirmed webpage. Lưu ý: để có thể quay về confirmed webpage, bạn cần đăng kí confirmed page URL tại developer home.

#### Khởi tạo Web URL

| Parameter             | Type              | Description                                                                                                                                                                                              |
| --------------------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| acr_values            | Enumerated string | **Bắt buộc**. Hiện tại chỉ hỗ trợ `consent_ctx`. <br> Ví dụ: `acr_values=consent_ctx:countryCode=VN,currency=VND`  <br> countryCode: Hai kí tự theo tiêu chuẩn ISO country code. Hiện tại chỉ hỗ trợ VN. |
| client_id             | string            | Mã định danh khách hàng ứng với tài khoản doanh nghiệp sử dụng cho online                                                                                                                                |
| code_challenge        | string            | Được khởi tạo từ `code_verifier`. [Chi tiết](#code_challenge)                                                                                                                                            |
| code_challenge_method | string            | Giá trị cho phép: s256. s256 là bảo mật cơ sở trong việc triển khai Moca.                                                                                                                                |
| nonce                 | string            | Một chuỗi ngẫu nhiên. Khuyến nghị sử dụng mã hóa base64 hoặc `hex encoded random string`. Số nonce phải được so sánh với giá trị trong `id_token`, khi token này được lấy từ `/token` endpoint.          |
| redirect_uri          | string            | URL nơi khách hàng được chuyển hướng khi hoàn tất thành công thanh toán qua Moca                                                                                                                         |
| request               | string            | **Bắt buộc**. Chỉ được sử dụng cho các tình huống giao dịch một lần. Giá trị này được nhận từ `JSON response` của API khởi tạo thanh toán.                                                               |
| response_type         | string            | **Bắt buộc**. Hiện tại chỉ chấp nhận giá trị `code`.                                                                                                                                                     |
| scope                 | string            | **Bắt buộc**. Phạm vi sẽ được sử dụng trong quá trình tích hợp. Hiện chỉ chấp nhận giá trị: `payment.vn.one_time_charge`                                                                                 |
| state                 | string            | **Bắt buộc**. Một chuỗi ngẫu nhiên được sử dụng để giảm thiểu các cuộc tấn công CSRF. Khuyến nghị sử dụng base64 hoặc `hex encoded random string`.                                                       |

##### Sample javascript to generate Web URL
```js
// request param get from Init 
function getAuthorizeLink(request) {
    var scope = ['openid', 'payment.vn.one_time_charge'];
    var response_type = 'code';
    var redirect_uri = 'https://yourwebserver/listener'; // Your redirect URL 
    // how to generate nonce, state and code_verifier can be seen on other example 
    var nonce = generateRandomString(16);
    var state = generateRandomString(7);
    var code_challenge_method = 'S256';
    var code_verifier = generateCodeVerifier(64);
    var code_challenge = generateCodeChallenge(code_verifier);
    var countryCode = "VN";
    var params = {
        client_id: client_id,
        scope: scope.join(' '),
        response_type: response_type,
        redirect_uri: redirect_uri,
        nonce: nonce,
        state: state,
        code_challenge_method: code_challenge_method,
        code_challenge: code_challenge,
        request: request,
        acr_values: "consent_ctx:countryCode=" + countryCode
    };
    var str = $.param(params);
    // You should get this URL from service discovery 
    var url = 'https://partner-gw.moca.vn/grabid/v1/oauth2/authorize?' + str;
    return url;
}
function generateRandomString(length) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
function base64URLEncode(str) {
    return str.toString(CryptoJS.enc.Base64).replace(/=/g,'').replace(/\+/g, '-').replace(/\//g, '_');
}
var nonce = generateRandomString(16);
var state = generateRandomString(7);
var codeVerifier = base64URLEncode(generateRandomString(64)); 
var codeChallenge = base64URLEncode(CryptoJS.SHA256(codeVerifier));

```

##### Sample URL
```url
https://partner-gw.moca.vn/grabid/v1/oauth2/authorize?acr_values=consent_ctx%3AcountryCode%3DVN,currency%3DVND&client_id=51111aaf8322403eae1122228f961a45&code_challenge=PP2Ut_vyvfkytkYEhobjNvYG7dNxil3vQNROIsm_JMU&code_challenge_method=S256&nonce=zYPZ8hIVW7CEfgCY&redirect_uri=https://www.sample.moca.com&request=eyJhbGciOiAibm9uZSJ9.eyJjbGFpbXMiOnsidHJhbnNhY3Rpb24iOnsidHhJRCI6IjNlZmQ1MGI5NmFjMTQ1NzViNzczNjU1YTEyMDRhNDNjIn19fQ.&response_type=code&scope=openid%20payment.vn.one_time_charge&state=fdgidgI
```

##### Sample Redirect URL
```
{{redirect_url}}?code=0865c8ba78c9477b92757ec21b671097&state=z9t0TkP
```

##### Thông tin khác

###### code_challenge 
Được khởi tạo từ `code_verifier`. javascript example:
```js
function generateRandomString(length) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

var codeVerifier = base64URLEncode(generateRandomString(64));
var codeChallenge = base64URLEncode(CryptoJS.SHA256(codeVerifier));

```
