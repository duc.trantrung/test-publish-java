# API Authentication

Tất cả các API request đều trải qua quá trình xác thực. Và để tăng thêm tính bảo mật, một số API cần nhiều tham số xác thực, để tăng sự an toàn và đảm bảo API request thành công. Moca sử dụng các phương pháp xác thực sau:
- Chữ kí HMAC
- OAuth2.0 token
- Chữ kí X-GID-AUX-POP HMAC


## Tạo chữ kí HMAC-SHA256

Một số request phải có chữ kí HMAC-SHA256, nếu không Moca sẽ từ chối request đó. Chữ kí được tính toán bởi **partner_secret** key.

##### Điều kiện cần có 

| Parameter      | Description                                         |
| -------------- | --------------------------------------------------- |
| partner_id     | Thông tin định danh tài khoản danh nghiệp           |
| partner_secret | Thông tin mật khẩu định danh tài khoản doanh nghiệp |

##### Các bước thực hiện

1. Băm request body: Nếu request có body thì nó cần phải được mã hoá, sử dụng thuật toán SHA-256 và hiển thị dưới dạng base64. 
2. Khai báo HTTP method. Ví dụ: ```POST```
3. Khai báo request content type. Ví dụ: ```application/json``` 
4. Tạo GMT date time. Ví dụ: ```Thu, 17 Jan 2019 02:45:06 GMT```
5. Xác định API truy cập đến. Ví dụ: ```/mocapay/partner/v2/charge/init```
6. Xây dựng string để tạo chữ kí từ các biến được tạo ra từ bước 1->5, Ví dụ:
   ```
   StringToSign = 'POST' + '\n'+\ 
               headers['Content-Type'] + '\n'+\ 
               headers['Date'] + '\n' +\ 
               '/mocapay/partner/v2/charge/init' + '\n' +\ 
               payload_hash + '\n' {{from step 1}}

   ```

##### Javascript example

```js
function generateHMACSignature(partnerID, partnerSecret, httpMethod, requestURL, contentType, requestBody, timestamp) {
    var requestPath = getPath(requestURL);
    if (httpMethod == 'GET' || !requestBody) {
        requestBody = '';
    }
    var hashedPayload = CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(requestBody));
    var requestData = [[httpMethod, contentType, timestamp, requestPath, hashedPayload].join('\n'), '\n'].join('');
    var hmacDigest = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(requestData, partnerSecret));
    return hmacDigest;
}
```

## Lấy oAuth Token

Tạo oAuth token bằng mã `code` được trả về trong [redirect URL từ Moca](/online-payment/api/setup-deeplink.md). Ví dụ: `code=0865c8ba78c9477b92757ec21b671097`

##### Endpoint
Method: ```POST```

URL: ```https://{{api-endpoint}}/grabid/v1/oauth2/token```



##### Header Parameters
| Parameter    | Type   | Description                                                                            |
| ------------ | ------ | -------------------------------------------------------------------------------------- |
| Content-Type | string | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ ```application/json``` |

##### Request Parameters
| Parameter     | Type   | Description                                                                             |
| ------------- | ------ | --------------------------------------------------------------------------------------- |
| code          | string | **Bắt buộc**. Được tạo ra từ bước trước đó.                                             |
| client_id     | string | **Bắt buộc**. Mã định danh khách hàng ứng với tài khoản doanh nghiệp sử dụng cho online |
| grant_type    | string | **Bắt buộc**. Hiện tại chỉ hỗ trợ giá trị `authorization_code`                          |
| redirect_uri  | string | **Bắt buộc**. Redirect URL đã đăng kí với Moca trước đó                                 |
| code_verifier | string | **Bắt buộc**. Mã code đã được sinh ra trong bước khởi tạo thanh toán                    |
| client_secret | string | **Bắt buộc**. Mã bảo mật cho mã định danh khách hàng                                    |

##### Sample API Request
```curl
curl -X POST https://partner-gw.moca.vn/grabid/v1/oauth2/token \ 
-H 'Content-Type: application/x-www-form-urlencoded' \ 
-d 
'code=97e9c5411c6b44cea3b903d4db2d5c3d&client_id=0208d86bfe374b8eb6f2c47915b27b29&gr ant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A23456%2FoAuth2callback&code_verifier=test&client_secret=test'
```

##### Sample Response
```json
{ 
	"access_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhdWQiOiI1NTA4OGFhZjg zMjI0MDNlYWUxMTc3OTI4Zjk2MWE0NSIsImV4cCI6MTU4MjE4NjIyMCwiaWF0IjoxNTUwNjUwMjIwLCJpbnR fc3ZjX2F1dGh6X2N0eCI6ImVjMDFlNTk5MDhkMzQ1ZjM4ZjdkMzRmYTE0YmU2YmRkIiwiaXNzIjoiaHR0cHM 6Ly9pZHAuZ3JhYi5jb20iLCJqdGkiOiJteVpIOGtnWFNxcURqalpVcTlselFnIiwibmJmIjoxNTUwNjUwMDQ wLCJwaWQiOiI1NjljOTUwMS1mNWU0LTRiZDktOGRmZS01NDFkOGYyYWY4MWUiLCJzY3AiOiJbXCJvcGVuaWR cIixcIjQyYzAxOGQzM2E2YTQ1ZjU4OWI1NDI5MTA4NmZiMTc0XCJdIiwic3ViIjoiNThmYzhlZDYtODNhNy0 0NGU3LTk5ZmQtZWVhYTUxODg5ODAxIiwic3ZjIjoiUEFTU0VOR0VSIiwidGtfdHlwZSI6ImFjY2VzcyJ9.uw xQUoX9JnVVPBqhGiK7Gis71L_OwzieOsfUSNrk0JnETT8ZGWneNyoWCpQXfBcWukNpvQiHF17kMrqAX_GRUy e97xZCY0RiSryaUJ86ywKEK8_wUsopLluGqOXOirnUHmFNuXiEIQChOq2RhTQ1sgShmU0X2xNUC0phq4BfyD n4HxiVHUDXQCICd8bttCQf8oeqXu4vnQLEdnHwoOkTLmBHNU8SuT1mhY2q3rdLyKdpM_1pIPxkI5yXvFTRtQ FxALHUFyJaisLp5yj6FBEyGBNaDmXtpj3MFl5CtxtyU1WvQ7vyoYU7lDkInB0rmjB8-0t5nSEY-hCa3bWjiTD5Wg", 
	"token_type": "Bearer", 
	"expires_in": 31535999, 
	"id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhY3IiOiJbXCJzZXJ2aWN lOlBBU1NFTkdFUlwiXSIsImF0X2hhc2giOiJYMzFXakw1bkZaVkhFLUh2N0NFa3VRIiwiYXVkIjoiNTUwODh hYWY4MzIyNDAzZWFlMTE3NzkyOGY5NjFhNDUiLCJleHAiOjE1NTA5MDk0MjAsImlhdCI6MTU1MDY1MDIyMCw iaXNzIjoiaHR0cHM6Ly9pZHAuZ3JhYi5jb20iLCJqdGkiOiJ0QjhZLTZYR1NnR1Z3dkZvcTJJekNnIiwibmJ mIjoxNTUwNjUwMjIwLCJub25jZSI6IlRJSnBNSkltZmlEZGJwSTIiLCJwaWQiOiI1NjljOTUwMS1mNWU0LTR iZDktOGRmZS01NDFkOGYyYWY4MWUiLCJzdWIiOiI1OGZjOGVkNi04M2E3LTQ0ZTctOTlmZC1lZWFhNTE4ODk 4MDEiLCJzdmMiOiJQQVNTRU5HRVIiLCJ0a190eXBlIjoiaWQifQ.hYMQxqj3tWwQi8Cf4vaN2dMzEtd-0Cbd rP7fEvgITm7LpIkDW5E0vTyy0zMf-HKs6YvNVwslDjf2bxTRkyUR3gQfXlhRp3WXe05D7X9Wle-5se_H_QI_t_fTdHq5lb2_EEw6iHk2Oh4GFr-ADyTzVcA6mmLPqdENYewX1QZflhm827CB2pUoZ8e0qx0K8PqZNeN4itgt gectM75pdgrkX52Wm2E6sihTZK9_yF4wUOf9L70x8C5RRrMTqLFDjuDTa2krSvWWRN27vA02RURQvzlpGlT9 yM7eI0sqkfRqzFdSHVPgOET2cutVa8zqj6xFw4E3sQ1v25sMenrTmOlLsA" 
}
```

## Chữ kí X-GID-AUX-POP HMAC

#### Yêu cầu 
- access_token: Được tạo từ [lấy oAuth token](#lấy-oauth-token) 
- client_secret: Mã bảo mật cho mã định danh khách hàng 

#### Các bước thực hiện
1. Lấy Unix timestamp hiện tại
2. Tạo `string_to_sign` bằng cách nối chuỗi `unix_timestamp` và `access_tokentoken`
3. Chuyển đổi chuỗi sang định dạng JSON
4. Tạo chữ kí HMAC được mã hoá base64 từ `client_secret` và `string_to_sign` trước đó 

#### Sample javascript
```js
function generatePOPSignature(clientSecret, accessToken, timestamp) {
    var timestampUnix = Math.round(timestamp.getTime() / 1000);
    console.log(timestampUnix);
    pm.environment.set('timestamp_int', timestampUnix);
    var message = timestampUnix.toString() + accessToken;
    var words = CryptoJS.enc.Utf8.parse(message);
    var utf8  = CryptoJS.enc.Utf8.stringify(words);
    var signature = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(utf8, clientSecret));
    var sub =base64URLEncode(signature);
    console.log(sub);
    
    var payload = {
        "time_since_epoch": timestampUnix,
        "sig": sub
    }
    var payloadBytes = JSON.stringify(payload)
    var result =  base64URLEncode(btoa(payloadBytes))
    console.log(result);
    return result;
}
var timestamp = new Date();
var clientSecret = 'Your_client_Secret'
var oAuthToken = 'Your_oauth2_token'
generatePOPSignature(clientSecret, oAuthToken, timestamp)
```

#### Sample Signature 
```
eyJ0aW1lX3NpbmNlX2Vwb2NoIjoxNTUxNzc5OTk1LCJzaWciOiJfajh3dm1IT0NXRFdJSGdIVE8yS0tSSkpqVHZRdW9WS2RnWGo4TE1kWm13In0
```