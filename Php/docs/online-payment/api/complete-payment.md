# Hoàn thành thanh toán

#### Endpoint
Method: ```POST```

URL: ```https://{{api-endpoint}}/mocapay/partner/v2/charge/complete```


#### Header Parameters 
| Parameter     | Type         | Description                                                                                               |
| ------------- | ------------ | --------------------------------------------------------------------------------------------------------- |
| Authorization | Bearer Token | **Bắt buộc**. Header required for authorization purpose. Ví dụ:`Authorization: Bearer {access_token}` |
| X-GID-AUX-POP | string       | **Bắt buộc**. Chữ ký HMAC được tính bằng mã truy cập oAuth.                                               |
| Date          | Date         | **Bắt buộc**. Ngày và giờ ở định dạng GMT. Ví dụ: `Fri, 14 Sep 2018 08:29:58 GMT`                     |
| Content-Type  | string       | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                        |

#### Request Parameters

| Parameter   | Type                           | Description                                                                                                                                          |
| ----------- | ------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| partnerTxID | string (max length = 32 chars) | **Bắt buộc**. partner_transaction_id, được sử dụng làm khoá tối ưu của request. Nó phải giống với partner_transaction_id được sử dụng khi tạo chữ ký |

#### Response Elements

| Parameter   | Type   | Description                                                                                                                                                                                                 |
| ----------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txID        | string | `A 32-bit UUID formatted Moca transaction ID`. Để tiến hành hoàn tiền cho giao dịch này, bạn cần sử dụng ID này và chạy API hoàn tiền.                                                                      |
| status      | string | (không còn hỗ trợ, vui lòng sử dụng txStatus). Các trạng thái có thể: `“success” / “failed” / “unknown”`                                                                                                    |
| description | string | Nếu giao dịch thành công, description sẽ trống. Nếu giao dịch không thành công, API sẽ cung cấp mô tả về lỗi. Ví dụ: `refund_exceeds_charge_amount`.                                                        |
| txStatus    | string | Các trạng thái có thể: ```Success/Failed/Processing/Transaction_already_exist```  <br>[Thông tin chi tiết](#transaction-status-for-charge)                                                                                                    |
| reason      | string | Tham khảo bảng Reason Code để biết danh sách tất cả các mô tả của chúng. Lưu ý rằng reason code là để biết thêm thông tin về khoản thanh toán. Mã lý do có thể để trống. Ví dụ, khi `txStatus = "success"`. |

#### HTTP Response Codes
| Code                      | Reason                                                                                                                                                                                                                                                                            | Action                       |
| ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------- |
| 200 OK                    | API request được xử lý thành công.                                                                                                                                                                                                                                        | -                            |
| 400 Bad Request           | `request payload` có thể bị thiếu hoặc chưa chính xác. Vui lòng kiểm tra lại và đảm bảo tất cả parameters đúng format.                                                                                                        | Sửa lỗi là thao tác lại |
| 401 Unauthorized          | Thông tin xác thực API không chính xác.   | |
| 403 Forbidden             | Cuộc gọi giao dịch này bị cấm.                                                                                                                                                                                                                                               | Sửa lỗi là thao tác lại  |
| 409 Conflict              | Có một số xung đột giữa các thông số được gửi lên với cấu hình trên máy chủ của chúng tôi. Hãy xem phần `reason code` để biết các lý do có thể xảy ra xung đột.                                                                                                                                                                    | Sửa lỗi là thao tác lại |
| 500 Internal Server Error | Có lỗi xảy ra trong dịch vụ của chúng tôi, vui lòng thử yêu cầu lại sau.                                                                                                                                                                                          | - |

#### Sample API Request
```curl
curl -X POST \ 
https://partner-gw.moca.vn/mocapay/partner/v2/charge/complete \ -H 'Authorization: Bearer {{ oAuth2_token }}' 
-H 'X-GID-AUX-POP: {{ POP_signature }}' 
-H 'Content-Type: application/json' \ 
-H 'Date: Thu, 13 Sep 2018 08:49:37 GMT' \ 
-d '{ 
 "partnerTxID":"{{ idempotency_key }}" 
}'

```

#### Sample response
```json
{ 
 "txID":"66f3f271cc994f1bb1e46e6fe4fd8727", 
 "status":"success", 
 "description":"" 
}
```

#### Transaction Status for Charge
| txStatus               | Description                                                                                                                                                                                                   |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| success                | Hành động thanh toán kết thúc thành công                                                                                                                                                                      |
| failed                 | Hành động thanh toán kết thúc thất bại                                                                                                                                                                        |
| processing             | Hệ thống đang xử lý giao dịch này, hãy đợi thông báo từ webhook. Ngoài ra, bạn có thể chạy API kiểm tra trạng thái biết trạng thái hiện tại của giao dịch. Tuy nhiên, đừng chạy API nhiều hơn 2 phút một lần. |
| cancelled              | Xảy ra khi <br>1) Người dùng không nhấp vào Thanh toán <br>2) API `/charge/complete` không được gọi và giao dịch đã hết hạn.                                                                                  |
| authorised             | Xác định giao dịch thanh toán được ủy quyền nhưng chưa hoàn tất.                                                                                                                                              |
| authorisation_declined | Xác định ủy quyền giao dịch thanh toán là bị từ chối.                                                                                                                                                         |