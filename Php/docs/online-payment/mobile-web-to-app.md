# Mobile Web to App

### Luồng xử lí

<img src="/merchants/docs/_assets/OnA-mobile-web-to-app.png" alt="OnA-app-to-app" style="width: 70%"/>


- Bước 1: Khách hàng chọn thanh toán bằng Grab/Moca, và nhấn nút Thanh toán.
- Bước 2: Merchant server [khởi tạo thanh toán](/online-payment/api/init-payment.md), gửi thông tin sang Moca server, nhận về thông tin giao dịch.
- Bước 3: Merchant website tạo [deeplink](/online-payment/api/setup-deeplink.md) để mở Grab app, khách hàng thực hiện thanh toán bằng Moca trên Grab app
- Bước 4: Sau khi khách hàng thanh toán, Grab app sẽ mở lại Merchant web để hiển thị kết quả giao dịch.
- Bước 5: Merchant server [hoàn thành thanh toán]((/online-payment/api/complete-payment.md)) và Merchant app hiển thị thông tin giao dịch cho khách hàng.

