# Cách hoạt động của luồng thanh toán online

[commentt]: <> (should use .svg file)
![Luồng giao dịch](_assets/ona-flow.png)

[POSTMAN API](https://www.getpostman.com/collections/bbb7cc27071b0aa0441f)

- Đối với giao dịch online trên website ta sẽ gọi lần lượt các hàm sau:
1. createDeeplinkUrl: để lấy link và hiển thị QR trên mobile browser hay desktop
2. oAuthToken: Lấy access token để chuẩn bị cho việc confirm và kiểm tra trạng thái giao dịch.
3. chargeComplete: Để hoàn tất giao dịch online
4. getChargeStatus: Để kiểm tra trạng thái của giao dịch online.
5. refundTxnOnA: Sử dụng nếu muốn hoàn trả lại một giao dịch online đã thành công
6. getRefundStatus: Kiểm tra trạng thái của giao dịch refund online.
7. getOtcStatus: Sử dụng lấy code giao dịch khi user thanh toán thành công trên grab app nhưng chưa nhận được thông tin trên redirect url.
