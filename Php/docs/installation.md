# Cài đặt

## PHP SDK

### Yêu cầu tương thích
Để sử dụng Moca Merchant PHP SDK, hệ thống của bạn cần hỗ trợ các thông số tối thiểu sau:
- **PHP:** phiên bản 7.0 trở lên
- **Composer:** mọi phiên bản

### 1 bước cài đặt
Moca Merchant PHP SDK cho được phát hành thông qua Composer. Do đó bạn có thể cài đặt nhanh chóng và dễ dàng chì với 1 dòng lệnh:
```bash
composer require mocavn/merchant
```


## .Net

### Yêu cầu tương thích
- **.NET: ** Framework >=2.0 

### Cài đặt 
- Tải [File Dll](_assets/MerchantSdk.dll)