# Tạo deeplink URL cho thanh toán online

```php
		use Moca\Merchant\MocaTransaction;
		$partnerTxID = md5(uniqid(rand(), true));
        $this->state = $this->generateRandomString(7);
        $call = new MocaTransaction();
    
        $webLink = $call->setPartnerTxID($partnerTxID)
            ->setAmount(6000)
            ->setCurrency("VND")
            ->setDescription("testing otc")
            ->setBrandName("")
            ->setState($this->state)
            ->createDeeplinkUrl();
```

`$deeplinkUrl` sẽ là giá trị String có dạng như sau:

`https://weblogin.stg-paysi.moca.vn/paxapp?acr_values=consent_ctx%3AcountryCode%3DVN%2Ccurrency%3DVND&browser=Chrome%3A87.0&client_id=3c7538460f08423c9fc4aece6bf4e6fe&code_challenge=6fbffa3e616a98f5700097f815c7d126154ab70d9b57ee6bc0d12d81026ce31d&code_challenge_method=S256&ctx_id=8508bd11fce2462cbc3e15522262d48e&device_type=Computer&forwardedHost=stg-paysi.moca.vn&gw=moca&login_method=qrcode&nonce=H4Q2Ag2DLnO4pqXl&redirect_uri=https%3A%2F%2Fyoumed.test.davido.vn&request=eyJhbGciOiAibm9uZSJ9.eyJjbGFpbXMiOnsidHJhbnNhY3Rpb24iOnsidHhJRCI6IjdhOGJkMjZkNTU4ZTQwOTI5YzQ3MjgyOGRkODQ2ZGZjIn19fQ.&request_id=1b4444b4-17cf-4fc0-a8da-59f86356b032&response_type=code&scope=payment.vn.one_time_charge&state=nhtjLT1`

Bạn cần redirect trang web của bạn tới địa chỉ `deeplinkUrl` trên.
- Trong trường hợp khách hàng sử dụng trang web của bạn trên điện thoại di động, hệ thống sẽ tự động mở ứng dụng Grab và nhập sẵn thông tin thanh toán. Khách hàng chỉ cần xác nhận giao dịch trên ứng dụng Grab để hoàn tất việc thanh toán cho bạn.
- Trong trường hợp khách hàng sử dụng trang web của bạn trên máy tính, `deeplinkURL` sẽ hiển thị màn hình thanh toán của Moca với QRCode tương ứng với giao dịch. Khách hàng cần mở ứng dụng Grab và quét QRCode để hoàn tất việc thanh toán cho bạn.

Sau khi khách hàng thực hiện bước xác nhận hoàn tất thanh toán trên, hệ thống Moca sẽ gọi vào [Redirect URL](/ona-webhook.md) của bạn để thông báo khách hàng đã thanh toán cho giao dịch (code và state).
