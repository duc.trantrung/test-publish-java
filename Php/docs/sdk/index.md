<!-- # Danh sách SDK

### Mobile app SDK

| Type    | Link                                             |
| ------- | ------------------------------------------------ |
| iOS     | https://github.com/grab/grabplatform-sdk-ios     |
| Android | https://github.com/grab/grabplatform-sdk-android |


### Backend SDK

| Type    | Link                                             |
| ------- | ------------------------------------------------ |
| PHP     | https://packagist.org/packages/mocavn/merchant   |
| Android | https://github.com/grab/grabplatform-sdk-android |

#### Ví dụ file .env

Sau khi cài đặt SDK, bạn cần cấu hình các thông số cần thiết bằng cách cập nhật file `.env` trong thư mục gốc của dự án. Ví dụ:

```
MOCA_MERCHANT_ENVIRONMENT=STAGING
MOCA_MERCHANT_PARTNER_ID=d68260a5-9ae3-4cea-aa6b-649a8450bfa3
MOCA_MERCHANT_PARTNER_SECRET=LjhFtKQmPLahiu_P
MOCA_MERCHANT_GRAB_ID=71edc3b8-2451-48aa-b195-35817e2cd5b1
MOCA_MERCHANT_TERMINAL_ID=
MOCA_MERCHANT_CLIENT_ID=3c7538460f08423c9fc4aece6bf4e6fe
MOCA_MERCHANT_CLIENT_SECRET=321mAlckJp9AGMHf
MOCA_MERCHANT_REDIRECT_URI=https://youmed.test.davido.vn
``` -->