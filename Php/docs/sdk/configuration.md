<!-- # Cấu hình Moca Merchant PHP SDK

Sau khi [Cài đặt Moca Merchant PHP SDK](/installation), bạn cần cấu hình các thông số cần thiết bằng cách cập nhật file `.env` trong thư mục gốc của dự án.

## Các thông số cần cấu hình

| STT | Thông số  | Ý nghĩa | Giá trị được chấp nhận |
|----:|-----------|---------|-------|
| 1   | MOCA_MERCHANT_ENVIRONMENT    | Môi trường cần kết nối đến | `PRODUCTION`,`STAGING` |
| 2   | MOCA_MERCHANT_TYPE    | Môi trường cần kết nối đến | `ONLINE`,`OFFLINE` |
| 3   | MOCA_MERCHANT_PARTNER_ID     | Thông số `PartnerID` được cung cấp bởi Moca khi đăng ký |       |
| 4   | MOCA_MERCHANT_PARTNER_SECRET | Thông số `PartnerSecret` được cung cấp bởi Moca khi đăng ký |       |
| 5   | MOCA_MERCHANT_GRAB_ID        | Thông số `GrabID` được cung cấp bởi Moca khi đăng ký |       |
| 6   | MOCA_MERCHANT_TERMINAL_ID    | Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký |       |
| 7   | MOCA_MERCHANT_CLIENT_ID      | Thông số `ClientID` được cung cấp bởi Moca khi đăng ký (chỉ bắt buộc nếu hệ thống của bạn chấp nhận thanh toán online) |       |
| 8   | MOCA_MERCHANT_CLIENT_SECRET  | Thông số `ClientSecret` được cung cấp bởi Moca khi đăng ký (chỉ bắt buộc nếu hệ thống của bạn chấp nhận thanh toán online) |       |
| 9   | MOCA_MERCHANT_REDIRECT_URI   | URL của API nhận kết quả phản hồi của Moca sau khi người dùng thanh toán cho bạn (chỉ bắt buộc nếu hệ thống của bạn chấp nhận thanh toán online) | https://your-domain.com/your-web-hook-uri |

## Ví dụ file .env
Dưới đây là ví dụ config của Moca. Các params sẽ thay đỗi tương ứng với mỗi partner.

```FILE
MOCA_MERCHANT_ENVIRONMENT=STAGING
MOCA_MERCHANT_TYPE=ONLINE
MOCA_MERCHANT_PARTNER_ID=fd092e5b-900c-4969-8c2f-48ab29ef9d67
MOCA_MERCHANT_PARTNER_SECRET=nRrOISCpbpgFx3D_
MOCA_MERCHANT_CLIENT_ID=e9b5560b0be844a2ad55c6afa8b23fbb
MOCA_MERCHANT_CLIENT_SECRET=BDGSPQYYUqLXNkmy
MOCA_MERCHANT_REDIRECT_URI=http://developer.moca.vn/merchants/sample/result
```

```FILE
MOCA_MERCHANT_ENVIRONMENT=STAGING
MOCA_MERCHANT_PARTNER_ID=d68260a5-9ae3-4cea-aa6b-649a8450bfa3
MOCA_MERCHANT_PARTNER_SECRET=LjhFtKQmPLahiu_P
MOCA_MERCHANT_GRAB_ID=71edc3b8-2451-48aa-b195-35817e2cd5b1
MOCA_MERCHANT_TERMINAL_ID=
MOCA_MERCHANT_CLIENT_ID=3c7538460f08423c9fc4aece6bf4e6fe
MOCA_MERCHANT_CLIENT_SECRET=321mAlckJp9AGMHf
MOCA_MERCHANT_REDIRECT_URI=https://youmed.test.davido.vn
``` -->
