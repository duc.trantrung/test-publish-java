<!-- # Xác nhận hoàn tất giao dịch Online Và Offline
Gọi function refundTxnOnA() đối với Online
Gọi function refundPosTxn() đối với Offline

## Nội dung request body gửi vào Moca(JSON)
* Cho online:
```
    $call = new MocaTransaction();
    $partnerTxID = md5(uniqid(rand(), true));
    $call->setPartnerTxID($partnerTxID);
    $call->setPartnerGroupTxID($partnerTxID);
    $call->setCurrency("VND");
    $call->setAmount(1000);
    $call->setDescription("test refund");
    $call->setCode($partnerTxID);
    $call->setState($partnerTxID);
```    
* Cho offline:
```
    $call = new MocaTransaction();
    $partnerTxID = md5(uniqid(rand(), true));

    $call->setPartnerTxID($partnerTxID);
    $call->setPartnerGroupTxID($partnerTxID);
    $call->setCurrency("VND");
    $call->setAmount(1000);
    $call->setDescription("test refund");

    $call->refundPosTxn();
```  

## Nội dung response:
JSON với cấu trúc như sau:
```
{
    msgID: "",
    TxID : "e136b31b187f4baa845c7b36778919f1",
    status: "success",
    description: "test refund",
    TxStatus: "success",
    reson: "",
}
``` -->
