<!-- 
## Hướng dẫn cài đặt

1. Clone project: ``` git clone https://gitlab.com/moca.vn/merchant-sdk/mex-backend-sdk-php.git```
2. Copy file environment: ``` cp .env.example .env ```
3. Chỉnh sửa lại thông tin môi trường test được cung cấp trong file .env
4. Run docker: ``` docker-compose up -d ```
5. Gen key: ``` sudo docker-compose exec app php artisan key:generate ```
6. Paste link ``` http://localhost:8888/ ``` trên browser để xem sample app. -->