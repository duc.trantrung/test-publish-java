# Example Controller.

```php
use Illuminate\Http\Request;
use Moca\Merchant\MocaTransaction;
use MocaMerchant;

class DemoControllers extends Controller
{

    protected $code, $state, $access_token, $partnerTxIDOrig, $partnerRefundTxID ;

    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $partnerTxID = md5(uniqid(rand(), true));
        session(['partnerTxID' => $partnerTxID]);
        $this->state = $this->generateRandomString(7);
        $call = new MocaTransaction();

        $webLink = $call->setPartnerTxID($partnerTxID)
            ->setAmount(6000)
            ->setCurrency("VND")
            ->setDescription("testing otc")
            ->setBrandName("")
            ->setState($this->state)
            ->createDeeplinkUrl();

        if (is_array($webLink)) {
            return view('card',$webLink);
        } else {
            return redirect($webLink);
        }
    }
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function GetResponse(Request $request) {
        $code = $request->code;
        $result = null;

        $call = new MocaTransaction();

        $resp = $call->setPartnerTxID(session('partnerTxID'))
            ->setCode($code)
            ->oAuthToken();

        if($resp->code == 200) {
            $access_token = $resp->body->access_token;
            session(['access_token' => $access_token]);

            $respComplete = $call->setPartnerTxID(session('partnerTxID'))
                ->setAccessToken($access_token)
                ->chargeComplete();

            if($respComplete->code == 200) {
                $partnerTxIDOrig = session('partnerTxID');
                $result = "success";
                session(['partnerTxIDOrig' => $partnerTxIDOrig]);
            }

            return view('result',['result' =>$respComplete]);
        } else {
            $result = "failt";
            return view('result',['result' =>$result]);
        }
    }

    public function getChargeStatus() {
        $call = new MocaTransaction();

        return $call->setPartnerTxID($this->partnerTxIDOrig)
            ->setAccessToken($this->access_token)
            ->setCurrency("VND")
            ->getChargeStatus();
    }

    public function refundOnA() {
        $call = new MocaTransaction();
        $partnerTxID = md5(uniqid(rand(), true));

        $resp = $call->setPartnerTxID($partnerTxID)
            ->setAccessToken($this->access_token)
            ->setCurrency("VND")
            ->setAmount(1000)
            ->setDescription("test refund")
            ->refundTxnOnA();

        if($resp->code ==200) {
            $this->partnerRefundTxID = $this->partnerTxID;
        }

        return $resp;
    }

    public function getRedundStatus() {
        $call = new MocaTransaction();

        return $call->setPartnerTxID($this->partnerRefundTxID)
            ->setAccessToken($this->access_token)
            ->setCurrency("VND")
            ->getRefundStatus();
    }

    private function generateRandomString($length) {
        $text = '';
        $possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( $i = 0; $i < $length; $i++) {
            $text .= $possible[rand(0, strlen($possible)-1)];
        }
        return $text;
    }
}
```
