# Thực hiện giao dịch
API dành cho luồng Merchant quét mã QR của khách hàng. The endpoint performs a payment transaction based on the consumer presented. The transaction charges the wallet associated with the request QR code and complete a payout to the request merchant grabID.

Endpoint thực hiện thanh toán giao dịch dựa trên mã QR người tiêu dùng xuất trình. Giao dịch tính phí ví được liên kết với mã QR yêu cầu và hoàn tất thanh toán cho người bán.

#### Endpoint URL
Method: ```POST```

URL: ```https://{{api-endpoint}}/mocapay/partners/v1/terminal/transaction/perform```

#### Header Parameters 
| Parameter    | Type   | Description                                                                        |
| ------------ | ------ | ---------------------------------------------------------------------------------- |
| Content-Type | string | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json` |

#### Request Parameters
| Parameter      | Type                            | Description                                                                             |
| -------------- | ------------------------------- | --------------------------------------------------------------------------------------- |
| msgID          | string (max length = 32 chars)  | **Bắt buộc**. Một UUID duy nhất của request.                                            |
| grabID         | string                          | **Bắt buộc**. Thông số `GrabID` được cung cấp bởi Moca khi đăng ký.                      |
| terminalID     | string                          | **Bắt buộc**. Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký.                  |
| currency       | string                          | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`. |
| amount         | int64                           | **Bắt buộc**. Số tiền giao dịch.                                                        |
| partnerTxID    | string  (max length = 32 chars) | **Bắt buộc**. Số thứ tự được tạo ra bởi các thiết bị đầu cuối.                          |
| code           | string                          | **Bắt buộc**. Mã QR được hiển thị tại thiết bị của người dùng.                           |
| additionalInfo | array                           | Các thông tin bổ sung cần thiết. Ví dụ: `amountBreakdown`                               |

Lưu ý: Theo mặc định, request timeout là 6 giây.

#### Response Parameters
| Parameter      | Type   | Description                                                                                                 |
| -------------- | ------ | ----------------------------------------------------------------------------------------------------------- |
| msgID          | string | **Bắt buộc**. Một UUID duy nhất của request.                                                                |
| txID           | string | **Bắt buộc**. unique_transaction_ID được tạo bởi Moca.                                                      |
| status         | string | **Bắt buộc**. Thông báo trạng thái của giao dịch ở giai đoạn này: `success/failed/unknown/pending/bad_debt` |
| currency       | string | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                     |
| amount         | int64  | **Bắt buộc**. Số tiền giao dịch.                                                                            |
| updated        | int64  | **Bắt buộc**. Thời gian giao dịch được cập nhật ở định dạng Unix timestamp.                             |
| errMsg         | string | Một thông báo lỗi cho các giao dịch không thành công.                                                       |
| additionalInfo | array  | Các thông tin bổ sung cần thiết. Ví dụ: `{"amountBreakdown":{"discountAmount":300,"paidAmount":700}}`       |

#### HTTP Response Codes
| Code   | Reason                             |
| ------ | ---------------------------------- |
| 200 OK | API request được xử lý thành công. |
