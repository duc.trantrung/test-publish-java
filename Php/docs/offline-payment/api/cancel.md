# Huỷ đơn hàng 

Hủy một khoản thanh toán đang chờ xử lý. Giao dịch có thể bị hủy khi trạng thái thanh toán là `unknown` sau 30 giây. Không thể hủy các khoản thanh toán đã được tính phí thành công, thay vào đó hãy sử dụng phương thức hoàn lại tiền.

#### Endpoint URL
Method: ```PUT```

URL: ```https://{{api-endpoint}}/mocapay/partners/v1/terminal/transaction/{origPartnerTxID}/cancel```

#### Header Parameters 
| Parameter     | Type                    | Description                                                                                                                                            |
| ------------- | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Authorization | HMAC signature          | **Bắt buộc**. [Chữ kí HMAC](/online-payment/api/authentication.md) đã được tạo ra từ trước. <br>Example: `‘Authorization’:’partner_id:hmac_signature’` |
| Date          | GMT formatted date time | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                                |
| Content-Type  | string                  | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                                                                     |

#### Request Parameters
| Parameter       | Type                            | Description                                                                                                                                           |
| --------------- | ------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------- |
| msgID           | string (max length = 32 chars)  | **Bắt buộc**. Một UUID duy nhất của request.                                                                                                          |
| grabID          | string                          | **Bắt buộc**. Thông số `GrabID` được cung cấp bởi Moca khi đăng ký                                                                                    |
| terminalID      | string                          | **Bắt buộc**. Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký                                                                                |
| currency        | string                          | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                                                               |
| partnerTxID     | string                          | **Bắt buộc**. partner_transaction_id, được sử dụng làm khoá tối ưu của request. Nó phải giống với partner_transaction_id được sử dụng khi tạo chữ ký. |
| origPartnerTxID | string  (max length = 32 chars) | **Bắt buộc**. Id giao dịch ban đầu được tạo bởi thiết bị đầu cuối.                                                                             |


Lưu ý: Theo mặc định, request timeout là 6 giây.

#### Response Parameters
No response elements returned.

#### HTTP Response Codes
| Code           | Reason               |
| -------------- | -------------------- |
| 204 Successful | No content returned. |
