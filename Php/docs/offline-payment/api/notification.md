# Notification

Moca sẽ thông báo cho máy chủ của merchant về giao dịch thanh toán và giao dịch hoàn tiền. Merchant server phải chuẩn bị endpoint để nhận HTTP requests từ Moca.

Notification header sẽ chứa chữ kí HMAC được kí bởi partner_secret, đảm bảo rằng request gửi đến là hợp lệ. Timestamp cũng sẽ được xác nhận hợp lệ. Chữ ký HMAC được tạo bằng thuật toán tương tự như đã đề cập trong [Tạo chữ ký HMAC](/online-payment/api/authentication?id=tạo-chữ-kí-hmac-sha256).

Để xác nhận bạn đã nhận được notification, merchant server cần phản hồi notification với `status code 204` và `empty response`.
Moca sẽ gửi lại thông báo cho đến khi thông báo được chấp nhận. Các lần gửi lại sẽ diễn ra 8 lần, vào lúc: 5 phút, 10 phút, 20 phút, 40 phút, 80 phút, 160 phút, và 320 phút.

#### Endpoint URL
Method: `POST`

URL: `{{partner_endpoint}}`

#### Header Parameters 
| Parameter    | Type   | Description                                                                        |
| ------------ | ------ | ---------------------------------------------------------------------------------- |
| Content-Type | string | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json` |
| Signature    | string | **Bắt buộc**. Bạn phải sử dụng `HMAC` cho param này                                |


#### Request Parameters
| Parameter   | Type                            | Description                                                                                                                                                                                                                                 |
| ----------- | ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| txType      | string                          | **Bắt buộc**. Loại giao dịch. Nó có thể là giao dịch P2M hoặc refund.                                                                                                                                                                        |
| partnerID   | string                          | **Bắt buộc**. Thông tin định danh tài khoản danh nghiệp.                                                                                                                                                                                    |
| partnerTxID | string  (max length = 32 chars) | **Bắt buộc**. Số thứ tự được tạo ra bởi các thiết bị đầu cuối.                                                                                                                                                                              |
| txID        | string  (max length = 32 chars) | **Bắt buộc**. transaction_ID do Moca tạo.                                                                                                                                                                                                   |
| origTxID    | string  (max length = 32 chars) | **Bắt buộc**. reference_transaction_ID, giao dịch ban đầu. Ví dụ: nếu đây là sự kiện hoàn lại tiền, `origTxID` sẽ là transaction_id thanh toán ban đầu. Nếu không có giao dịch nào liên quan đến giao dịch gốc, thì đây là một chuỗi trống. |
| amount      | integer                         | **Bắt buộc**. Số tiền giao dịch.                                                                                                                                                                                                            |
| currency    | string                          | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                                                                                                                                                                                                              |
| status      | string                          | **Bắt buộc**. Thông báo trạng thái của giao dịch ở giai đoạn này: `success/failed/unknown/pending/bad_debt`                                                                                                                                 |
| createdAt   | string                          | **Bắt buộc**. Thời gian tạo giao dịch được khởi tạo ở định dạng Unix timestamp.                                                                                                                                   |
| completedAt | array                           | **Bắt buộc**. Thời gian khi giao dịch hoàn tất ở định dạng Unix timestamp.                                                                                                                                             |
| payload     | JSON object                          | **Bắt buộc**.  dành riêng cho P2M, thông tin bổ sung về giao dịch, nó có thể chứa các thông tin sau: số tiền được hoàn lại, terminal_id, mocapay_id, payer_name.                                              |

Lưu ý: Theo mặc định, request timeout là 6 giây.
