# Đối soát giao dịch
Trả về chi tiết cho một giao dịch thanh toán hoặc một giao dịch hoàn tiền.

#### Endpoint URL
Method: ```GET```

URL: ```https://{{api-endpoint}}/mocapay/partners/v1/terminal/transaction/{partnerTxID}```
#### Header Parameters 
| Parameter    | Type   | Description                                                                                                        |
| ------------ | ------ | ------------------------------------------------------------------------------------------------------------------ |
| Content-Type | string | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Bạn phải sử dụng  `application/x-www-form-urlencoded` |
| Authorization | HMAC signature          | **Bắt buộc**. [Chữ kí HMAC](/online-payment/api/authentication.md) đã được tạo ra từ trước. <br>Example: `‘Authorization’:’partner_id:hmac_signature’` |
| Date          | GMT formatted date time | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                                 |
#### Request Parameters
| Parameter      | Type                             | Description                                                                                                                                          |
| -------------- | -------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| msgID          | string   (max length = 32 chars) | **Bắt buộc**. Một UUID duy nhất của request.                                                                                                         |
| grabID         | string                           | **Bắt buộc**. Thông số `GrabID` được cung cấp bởi Moca khi đăng ký.                                                                                   |
| terminalID     | string                           | **Bắt buộc**. Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký.                                                                               |
| currency       | string                           | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                                                          |
| txType      | string                          | **Bắt buộc**. Loại giao dịch. Nó có thể là giao dịch P2M hoặc refund.                                                                                                                                                                        |
| partnerTxID    | string  (max length = 32 chars)  | **Bắt buộc**. Là tham chiếu thanh toán hoặc hoàn lại tiền duy nhất được gửi từ thiết bị đầu cuối. |
| additionalInfo | array                            | Các thông tin bổ sung . Ví dụ: `amountBreakdown.`                                                                             |

Lưu ý: Theo mặc định, request timeout là 6 giây.

#### Response Parameters
| Parameter      | Type    | Description                                                                                                         |
| -------------- | ------- | ------------------------------------------------------------------------------------------------------------------- |
| msgID          | string  | **Bắt buộc**. Một UUID duy nhất của request.                                                                        |
| txID           | string  | **Bắt buộc**. unique_transaction_ID được tạo bởi Moca.                                                              |
| status         | string  | **Bắt buộc**. Thông báo trạng thái của giao dịch ở giai đoạn này: `success/failed/unknown/pending/bad_debt`         |
| currency       | string  | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                         |
| amount         | integer | **Bắt buộc**. Số tiền giao dịch.                                                                                    |
| updated        | integer | **Bắt buộc**. Thời gian tạo giao dịch được cập nhật ở định dạng Unix timestamp.                                                 |
| errMsg         | string  | Một thông báo lỗi cho các giao dịch không thành công.                                                               |
| additionalInfo | array   | Các thông tin bổ sung cần thiết. Ví dụ: `{"amountBreakdown":{"discountAmount":300,"paidAmount":700}}` |


#### HTTP Response Codes
| Code   | Reason                             |
| ------ | ---------------------------------- |
| 200 OK | API request được xử lý thành công. |
