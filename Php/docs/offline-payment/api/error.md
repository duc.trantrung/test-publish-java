# Bảng mã lỗi 

### Invalid Request

status code: 400

| Field      | Description                                   |
| ---------- | --------------------------------------------- |
| DevMessage | Yêu cầu không hợp lệ và trả về thông báo lỗi. |
| Arg        | Yêu cầu chứa một tham số không hợp lệ.        |

### Logic Error

status code: 4xx, 5xx


| Field  | Description                                   |
| ------ | --------------------------------------------- |
| code   | Một lỗi xảy ra có thể do code.                |
| reason | Một thông báo giải thích nguyên nhân của lỗi. |


### Error Code HTTP Code Mapping

| Error Code | HTTP Code | Description                                         |
| ---------- | --------- | --------------------------------------------------- |
| 5003       | 500       | Lỗi dịch vụ database                                |
| 4040       | 404       | Không tìm thấy giao dịch                            |
| 4041       | 404       | Khách hàng không tồn tại                            |
| 4097       | 409       | Giao dịch vi phạm quy tắc tuân thủ                  |
| 40011      | 400       | Giao dịch không được hỗ trợ để hủy                  |
| 40012      | 400       | Hoàn trả một phần không được phép cho giao dịch này |
| 40912      | 409       | Mã người tiêu dùng không hợp lệ                     |
| 40913      | 409       | Mã người tiêu dùng đã hết hạn                       |
