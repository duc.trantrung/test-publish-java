# Tạo mã thanh toán
API dành cho flow khách hàng quét mã QR từ máy POS. Endpoint này khởi tạo thanh toán với một `txID` duy nhất và trả về mã QR Code đã được mã hoá gồm thông tin của người bán, số tiền, và txID.

#### Endpoint URL
Method: ```POST```

URL: ```https://{{api-endpoint}}/mocapay/partners/v1/terminal/qrcode/create```

#### Header Parameters 
| Parameter     | Type                    | Description                                                                                                                                            |
| ------------- | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| Authorization | HMAC signature          | **Bắt buộc**. [Chữ kí HMAC](/online-payment/api/authentication.md) đã được tạo ra từ trước. <br>Example: `‘Authorization’:’partner_id:hmac_signature’` |
| Date          | GMT formatted date time | **Bắt buộc**. Ngày và giờ ở định dạng GMT. <br>Example: `Fri, 14 Sep 2018 08:29:58 GMT`                                                                |
| Content-Type  | string                  | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json`                                                                     |

#### Request Parameters

| Parameter   | Type                           | Description                                                                                                                                                                                                                     |
| ----------- | ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| msgID       | string (max length = 32 chars) | **Bắt buộc**. Một UUID duy nhất của request.                                                                                                                                                                                    |
| grabID      | string                         | **Bắt buộc**. Thông số `GrabID` được cung cấp bởi Moca khi đăng ký                                                                                                                                                              |
| terminalID  | string                         | **Bắt buộc**. Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký                                                                                                                                                          |
| currency    | string                         | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`.                                                                                                                                         |
| amount      | int64                          | **Bắt buộc**. Số tiền giao dịch.                                                                                                                                                                                                |
| partnerTxID | string (max length = 32 chars) | **Bắt buộc**. Là số đơn đặt hàng duy nhất được tạo bởi thiết bị đầu cuối còn được gọi là partner_transaction_id, được sử dụng làm khoá tối ưu của request. Nó phải giống với partner_transaction_id được sử dụng khi tạo chữ ký |

Lưu ý: Theo mặc định, request timeout là 6 giây.

#### Response Parameters
| Parameter | Type   | Description                                                                                                       |
| --------- | ------ | ----------------------------------------------------------------------------------------------------------------- |
| msgID     | string | **Bắt buộc**. Một UUID duy nhất của request.                                                                      |
| qrcode    | string | **Bắt buộc**. Mã QR được hiển thị tại thiết bị đầu cuối để người tiêu dùng quét và thanh toán bằng ứng dụng Grab. |
| txID      | string | **Bắt buộc**. unique_transaction_ID được tạo bởi Moca.                                                            |

#### HTTP Response Codes
| Code   | Reason                             |
| ------ | ---------------------------------- |
| 200 OK | API request được xử lý thành công. |
