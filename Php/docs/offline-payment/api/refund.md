# Hoàn tiền
Hoàn lại khoản thanh toán đã thành công trước đó. Bạn có thể hoàn lại toàn bộ số tiền, hoặc một phần cho bất kỳ giao dịch nào đã được thanh toán bằng Moca.

Nhiều khoản hoàn lại (một phần) sẽ được chấp nhận miễn là tổng số tiền của chúng không vượt quá số tiền đã tính phí. Bạn chỉ có thể yêu cầu hoàn lại tiền trong vòng 30 ngày kể từ ngày bị tính phí.

#### Endpoint URL
Method: ```PUT```

URL: ```https://{{api-endpoint}}/mocapay/partners/v1/terminal/transaction/{origPartnerTxID}/refund```

#### Header Parameters 
| Parameter    | Type   | Description                                                                        |
| ------------ | ------ | ---------------------------------------------------------------------------------- |
| Content-Type | string | **Bắt buộc**. Kiểu dữ liệu của request body.<br>Moca chỉ hỗ trợ `application/json` |

#### Request Parameters
| Parameter      | Type                            | Description                                                                                 |
| -------------- | ------------------------------- | ------------------------------------------------------------------------------------------- |
| msgID          | string  (max length = 32 chars) | **Bắt buộc**. Một UUID duy nhất của request.                                                |
| grabID         | string                          | **Bắt buộc**. Thông số `GrabID` được cung cấp bởi Moca khi đăng ký.                          |
| terminalID     | string                          | **Bắt buộc**. Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký.                      |
| currency       | string                          | **Bắt buộc**. Đơn vị tiền tệ. Hiện tại hỗ trợ `IDR, KHR, MMK, MYR, PHP, SGD, THB, VND`. |
| amount         | integer                         | **Bắt buộc**. Số tiền giao dịch.                                                            |
| partnerTxID    | string  (max length = 32 chars) | **Bắt buộc**. Số thứ tự được tạo ra bởi các thiết bị đầu cuối.                              |
| origPartnerTxID | string  (max length = 32 chars) | **Bắt buộc**. Id giao dịch ban đầu được tạo bởi thiết bị đầu cuối.                                                                             |
| reason          | string                          | Giải thích lý do việc hoàn tiền.                                                                                                                     |

Lưu ý: Theo mặc định, request timeout là 6 giây.

#### Response Parameters
| Parameter      | Type    | Description                                                                                                         |
| -------------- | ------- | ------------------------------------------------------------------------------------------------------------------- |
| msgID          | string  | **Bắt buộc**. Một UUID duy nhất của request.                                                                        |
| txID           | string  | **Bắt buộc**. unique_transaction_ID được tạo bởi Moca.                                                              |
| status         | string  | **Bắt buộc**. Thông báo trạng thái của giao dịch: `success/failed/unknown/pending/bad_debt`         |

#### HTTP Response Codes
| Code   | Reason                             |
| ------ | ---------------------------------- |
| 200 OK | API request được xử lý thành công. |
