# Khách hàng quét mã QR tĩnh tại quầy thanh toán

### Luồng xử lí

- Bước 1: Khách hàng quét mã QR tĩnh, sau đó nhập số tiền cần thanh toán, và ấn nút thanh toán.
- Bước 2: Merchant xác nhận giao dịch thành công dựa vào [portal](https://merchant.grab.com/) được Moca cung cấp.
