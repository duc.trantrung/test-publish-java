# Khách hàng quét mã QR của Merchant

### Luồng xử lí

<img src="/merchants/docs/_assets/offline/merchant-presented-qr.png" alt="" style="width: 70%"/>


- Bước 1: Khách hàng gửi yêu cầu thanh toán bằng cách sử dụng ứng dụng Grab ở cửa hàng của đối tác.
- Bước 2: Thu ngân nhập vào số tiền giao dịch. Backend của đối tác tạo ra partnerTxID duy nhất và gọi đến [Khởi tạo đơn hàng](/offline-payment/api/create-order.md). Moca trả về Moca txID và QR code cho giao dịch này. 
- Bước 3: Khách hàng mở ứng dụng Grab và quyét mã QR của Merchant.
- Bước 4: Sau khi quyét mã QR, số tiền giao dịch được điền sẵn ở trên ứng dụng Grab. Moca backend sẽ xử lý giao dịch và ở ứng dụng Grab sẽ hiện thị "Giao dịch thành công".
- Buớc 5: Moca backend sẽ gửi [thông báo](/offline-payment/api/notification.md) đến backend của đối tác với trạng thái "success". Máy POS của đối tác sẽ hiển thị "Giao dịch thành công"
- Bước 6: Backend của đối tác cũng có goi đến [đối soát đơn hàng](/offline-payment/api/inquiry.md) để truy vấn trạng thái của giao dịch với một khoảng thời gian nhất định.
    - Nếu *status = "success"*, ứng dụng sẽ hiện thị "Giao dịch thành công" cho nhân viên thu ngân.
    - Nếu *status = "failed"*, ứng dụng sẽ hiển thị "Giao dịch bị từ chối" cho nhân viên thu ngân.
    - Nếu giao dịch bị timeout hoặc trở thành unknown sau một khoảng thời gian (60 giây), Backend của đối tác sẽ gọi đến [Huỷ bỏ đơn hàng](/offline-payment/api/cancel.md)
  
