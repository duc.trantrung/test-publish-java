# Merchant quét mã QR của khách hàng

### Luồng xử lí

<img src="/merchants/docs/_assets/offline/consumer-presented-qr.png" alt="" style="width: 70%"/>


- Bước 1: Khách hàng gửi yêu cầu thanh toán bằng cách sử dụng Moca trên ứng dụng Grab ở cửa hàng của đối tác hoặc chọn Moca ở Kiosk
- Bước 2: Khách hàng mở app Grab, sau đó hiển thị mã vạch và QR của Moca (Thanh toán -> Quét để thanh toán -> Mã QR và mã vạch)
- Bước 3: Thu ngân hoặc Kiosk sẽ quyét mã code, sồ tiền đặt hàng sẽ được gửi tới backend của đối tác. Backend của đối tác sẽ tạo ra 1 mã `partnerTxID` duy nhất và gửi request đến [thực hiện giao dịch](/offline-payment/api/perform-transaction.md). Moca sẽ trả về một mã `txTD` duy nhất và trạng thái của giao dịch
- Bước 4: Nếu Perform Transaction timeout, đối có hai cách để kiểm tra trạng thái của giao dịch:
    - [Thông báo](/offline-payment/api/notification.md)
    - [Đối soát giao dịch](/offline-payment/api/inquiry.md)
- Bước 5: Nếu giao dich vẫn trả về time out hoặc sau một khoảng thời gian nhất định(60 giấy), backend của đối tác có thể gọi [huỷ bỏ giao dịch](/offline-payment/api/cancel.md) để chấm dứt giao dịch


