# Huỷ giao dịch Offline
Partner call function cancelTxn();

```php
    $call = new MocaTransaction();
    $call->setCurrency("VND")
         ->setAmount(1000)
         ->setOriginTxID("e136b31b187f4baa845c7b36778919f1")
         ->setPartnerTxID($partnerTxID)
         ->cancelTxn();
```

Sẽ nhận được thông báo từ [Webhook](/mpqr-webhook)
