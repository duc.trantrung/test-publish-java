# Quy trình tích hợp thanh toán Moca

Các bước cơ bản để tích hợp với Moca:

- Đăng ký tài khoản doanh nghiệp và ký NDA giữa 2 bên. Bạn cần hoàn thành quá trình đăng ký với đầy đủ thông tin, trạng thái mặc định của doanh nghiệp sẽ là chưa xác thực. Thông tin tích hợp mặc định sẽ là môi trường Test
- Tham khảo và lựa chọn phương thức thanh toán áp dụng cho dịch vụ của đơn vị kinh doanh.
- Tiến hành tích hợp theo tài liệu của từng phương thức thanh toán.
- Đơn vị tiến hành kiểm thử phần mềm, tham khảo các testcase của Moca cung cấp để kiểm tra các lỗi phổ biến trong quá trình thanh toán.
- Sau khi đơn vị kinh doanh hoàn thành tích hợp và kiểm thử, Moca sẽ xác thực dịch vụ của bạn trên môi trường test trước khi lên production (dựa vào video nghiệm thu trên môi trường test).
- Sau khi được xác nhận, tài khoản doanh nghiệp của bạn sẽ được chuyển sang trạng thái đã xác thực.
- Thay đổi các thông tin tích hợp theo môi trường production.
- Triển khai dịch vụ thanh toán cho khách hàng.
- Tham khảo tài nguyên: Logo, Brand name, ...

## Thông tin tích hợp
Moca cung cấp cho đơn vị kinh doanh hai môi trường để tích hợp với Moca API:

- Testing: Sử dụng trong quá trình tích hợp: xây dựng tính năng, kiểm thử, debug, v.v..
- Production: Sử dụng để triển khai cho end user thanh toán dịch vụ.

## Đăng ký với Moca
Với hàng nghìn địa điểm chấp nhận, hỗ trợ thẻ quốc tế và ATM nội địa, Moca thay thế hoàn toàn cách bạn thanh toán hằng ngày chỉ với chiếc điện thoại thông minh.

## Nhận bộ thông tin định danh đơn vị chấp nhận thanh toán Moca
Sau khi đăng ký trở thành điểm chấp nhận thanh toán Moca, bạn sẽ nhận được bộ thông tin định danh cửa hàng bao gồm:

| STT | Thông tin | Ý nghĩa |
|----:|-----------|---------|
| 1   | Partner ID     | Thông tin định danh tài khoản danh nghiệp     |
| 2   | Partner secret        | Thông tin mật khẩu định danh tài khoản doanh nghiệp      |
| 3   | Merchant ID       | Mã merchant của tài khoản doanh nghiệp     |
| 4   | Store ID      | Mã store tương ứng với tài khoản doanh nghiệp.      |
| 5   | Client ID      | Mã định danh khách hàng ứng với tài khoản doanh nghiệp sử dụng cho online      |
| 6   | Client secret      | Mã bảo mật cho mã định danh khách hàng     |
| 7   | Redirect uri android      | Link uri do grab cung cấp để trả về thông tin trên android   |
| 8   | Redirect uri ios           | Link uri do grab cung cấp để trả về thông tin trên IOS|

## Cấu hình 

HTTP Request

|Key          | Value|
|-------------|------|
|Content-Type |	application/json|
|Method       |	POST/GET/PUT|
|Domain       |	Production:   https://partner-gw.moca.vn <br />Staging:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;https://stg-paysi.moca.vn|

## Postman cho Moca API:

- Online: https://www.getpostman.com/collections/bbb7cc27071b0aa0441f
- Offline: https://www.getpostman.com/collections/b6ec372a67d9a22e7828

Import file postman trên và tạo environment với các tham số như sau:

- Online: partner_id, partner_secret, client_id, client_secret, merchant_vn, redirect_uri.
- Offline: partner_id, partner_seret.

## Tích hợp sản phẩm thanh toán Moca vào hệ thống bán hàng của bạn
Với bộ thông tin định danh trên đây, bạn đã sẵn sàng tích hợp trải nghiệm thanh toán Moca vào hệ thống bán hàng một cách đơn giản thông qua việc sử dụng bộ thư viện:

[Moca Merchant PHP SDK](https://packagist.org/packages/mocavn/merchant) <br />
[Moca_Merchant_Java_SDK]() <br />
[Moca_Merchant_NET_SDK]() <br />