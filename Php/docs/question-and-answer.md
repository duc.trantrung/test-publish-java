# Một số câu hỏi thường gặp

1.  Sau bao lâu thì một giao dịch sẽ bị hết hạn? 
   
   Sau 15 phút, nếu giao dịch không được xử lí, hệ thống sẽ tự động huỷ giao dịch đó.

2.  Khi setup Moca SDK, có yêu cầu thông tin cho 2 fields là `com.grab.partner.sdk.Scope` và `com.grab.partner.sdk.ServiceDiscoveryUrl`. Mình cần điền thông tin gì vào 2 fields này?
   
   `Scope = payment.vn.one_time_charge openid`
   
   `ServiceDiscoveryUrl = https://stg-paysi.moca.vn/grabid/v1/oauth2/.well-known/openid-configuration`

