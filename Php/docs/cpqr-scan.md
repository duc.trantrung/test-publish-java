# Quét QRCode của khách hàng

  Thu ngân sẽ quét QR của khách hàng được tạo trên ví Grab (Thanh toán-> Pay -> Tạo QR code) sẽ có được mã thanh toán và tiếp tục

## Gọi API perform để thanh toán

```php
  use Moca\Merchant\MocaTransaction;
  $partnerTxID = md5(uniqid(rand(), true));
  $call = new MocaTransaction();  
  $response = $call->setPartnerTxID($partnerTxID)
      ->setAmount(6000)
      ->setCurrency("VND")
      ->setCode("000000000000000000")
      ->performTxn();
```
