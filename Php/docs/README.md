# Tổng quan sản phẩm 

Ví điện tử Moca hỗ trợ đa dạng phương thức thanh toán, giúp các doanh nghiệp dễ dàng tích hợp và sử dụng.

### Các phương thức thanh toán
#### Thanh toán online
- [Thanh toán App to App](/online-payment/app-to-app): Khách hàng chọn thanh toán bằng Grab/Moca trên ứng dụng điện thoại, Moca trên Grab app sẽ được gọi để thanh toán.
- [Thanh toán mobile web to app](/online-payment/mobile-web-to-app.md): Khách hàng chọn thanh toán bằng Grab/Moca trên mobile browser, Moca trên Grab app sẽ được gọi để thanh toán.
- [Thanh toán trên website qua QR code](/online-payment/qr-code.md): Khách hàng bật Grab app, sử dụng QR của Moca quét mã QR hiển thị trên website của merchant.

#### Thanh toán tại cửa hàng 
Có 3 cách để người dùng có thể thanh toán tại cửa hàng như sau:
- [Thanh toán qua mã QR của merchant](/offline-payment/merchant-presented-qr.md): Khách hàng quét mã QR hiển thị trên máy POS của merchant.
- [Thanh toán qua mã QR của khách hàng](/offline-payment/consumer-presented-qr.md): Merchant quét mã QR Moca của khách hàng trên ứng dụng Grab.
- [Thanh toán qua mã QR tĩnh](/offline-payment/static-qr.md): Khách hàng quét mã QR tĩnh tại quầy thanh toán của merchant.


### Quy trình tích hợp thanh toán
- Doanh nghiệp đăng ký tài khoản và ký NDA giữa 2 bên.
- Lựa chọn những phương thức thanh toán áp dụng cho dịch vụ và tiến hành tích hợp theo tài liệu của từng phương thức thanh toán. 
- Kiểm thử phần mềm, tham khảo các testcase của Moca cung cấp để đảm bảo hệ thống hoạt động chính xác.
- Moca xác thực dịch vụ của bạn hoạt động chính xác trên môi trường test (dựa vào video nghiệm thu trên môi trường test).
- Sau khi được xác nhận, Moca sẽ cũng cấp môi trường production cho doanh nghiệp.
- Doanh nghiệp triển khai dịch vụ thanh toán tới khách hàng với môi trường production.


### Môi trường tích hợp thanh toán
Moca cung cấp cho đơn vị kinh doanh hai môi trường để tích hợp với Moca API:

- Staging: Sử dụng trong quá trình tích hợp: xây dựng tính năng, kiểm thử, debug, v.v..
- Production: Sử dụng để triển khai cho end user thanh toán dịch vụ.

| Environment | Domain                     |
| ----------- | -------------------------- |
| Staging     | https://stg-paysi.moca.vn  |
| Production  | https://partner-gw.moca.vn |

### Bộ thông tin định danh đơn vị chấp nhận thanh toán online
Sau khi đăng ký trở thành điểm chấp nhận thanh toán Moca, bạn sẽ nhận được bộ thông tin định danh cửa hàng bao gồm:

|   No | Name                 | Description                                                               |
| ---: | -------------------- | ------------------------------------------------------------------------- |
|    1 | partner_id           | Thông tin định danh tài khoản danh nghiệp                                 |
|    2 | partner_secret       | Thông tin mật khẩu định danh tài khoản doanh nghiệp                       |
|    3 | merchant_id          | Mã merchant của tài khoản doanh nghiệp                                    |
|    4 | store_id             | Mã store tương ứng với tài khoản doanh nghiệp.                            |
|    5 | client_id            | Mã định danh khách hàng ứng với tài khoản doanh nghiệp sử dụng cho online |
|    6 | client_secret        | Mã bảo mật cho mã định danh khách hàng                                    |
|    7 | redirect_uri_android | Link uri do Grab cung cấp để trả về thông tin trên android                |
|    8 | redirect_uri_ios     | Link uri do Grab cung cấp để trả về thông tin trên IOS                    |


### Bộ thông tin định danh đơn vị chấp nhận thanh toán offline
|   No | Name           | Description                                              |
| ---: | -------------- | -------------------------------------------------------- |
|    1 | partner_id     | Thông tin định danh tài khoản danh nghiệp                |
|    2 | partner_secret | Thông tin mật khẩu định danh tài khoản doanh nghiệp      |
|    3 | grab_id        | Thông số `GrabID` được cung cấp bởi Moca khi đăng ký     |
|    4 | terminal_id    | Thông số `TerminalID` được cung cấp bởi Moca khi đăng ký |


### Bộ thông tin merchant cần cung cấp

|   No | Name               | Description                                                                           |
| ---: | ------------------ | ------------------------------------------------------------------------------------- |
|    1 | PIC                | Họ tên nhân viên tích hợp hệ thống                                                    |
|    2 | PIC_phone_number   | Số điện thoại của PIC                                                                 |
|    3 | webhook            | Moca sẽ gọi đến webhook thông bao về trạng thái của các giao dịch đang được thực hiện |
|    4 | redirect_uri       | URL nhận kết quả phản hồi của Moca sau khi người dùng thanh toán online               |
|    4 | test_phone_numbers | Các số điện thoại được sử dụng để đăng kí tài khoản test                              |