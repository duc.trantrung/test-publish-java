# Tạo QR code Offline.
Partner call function createQrCode();

``` php
    use Moca\Merchant\MocaTransaction;
    $partnerTxID = md5(uniqid(rand(), true));
    $call = new MocaTransaction();

    $webLink = $call->setPartnerTxID($partnerTxID)
        ->setAmount(6000)
        ->setCurrency("VND")
        ->createQrCode();
```

Sau đó sẽ nhận được QR code từ Moca sau đó đối tác sẽ parse qr code đó ra hình ảnh và show lên cho khách hàng sử dụng app Moca hoặc Grab thanh toán.
Mã QR code có hỗ trợ UTF-8.
