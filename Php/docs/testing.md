# Kiểm thử phần mềm

### Môi trường staging
1. Bạn vui lòng cung cấp email, Moca team sẽ gửi Grab app cho môi trường staging để bạn thực hiện cài đặt trên thiết bị Android.
2. Bạn vui lòng đăng kí tài khoản thông qua app được cung cấp và gửi lại danh sách email đã đăng kí, Moca sẽ nạp tiền vào các tài khoản đó để bạn thực hiện việc thanh toán trên môi trường staging.

### Môi trường production
Hiện tại Moca chưa hỗ trợ việc testing trên môi trường production.
