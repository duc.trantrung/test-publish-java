# Xác nhận hoàn tất giao dịch

```file
    use Moca\Merchant\MocaTransaction;

    $code = $request->code;
    $result = null;

    $call = new MocaTransaction();

    $resp = $call->setPartnerTxID(session('partnerTxID'))
        ->setCode($code)
        ->oAuthToken();

    if($resp->code == 200) {
        $access_token = $resp->body->access_token;
        session(['access_token' => $access_token]);

        $respComplete = $call->setPartnerTxID(session('partnerTxID'))
            ->setAccessToken($access_token)
            ->chargeComplete();

        if($respComplete->code == 200) {
            $partnerTxIDOrig = session('partnerTxID');
            $result = "success";
            session(['partnerTxIDOrig' => $partnerTxIDOrig]);
        }

        return view('result',['result' =>$respComplete]);
    } else {
        $result = "failt";
        return view('result',['result' =>$result]);
    }
```

## Nội dung return

JSON với cấu trúc như sau:
```JSON
{
    msgID: "",
    TxID : "e136b31b187f4baa845c7b36778919f1",
    status: "success",
    description: "",
    TxStatus: "success",
    reson: "",
}
```
