<?php
namespace Moca\Merchant;
use Moca\Merchant\MerchantIntegrationOffline;
use Moca\Merchant\MerchantIntegrationOnline;
use PHPUnit\Framework\TestCase;

class MerchantIntegrationTest extends TestCase {
    public function testOnaChargeInit() {
        $partnerTxID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');

        $resp = $call->onaChargeInit($partnerTxID, $partnerTxID, 6000, 'VND',"testing otc", false);

        $this->assertEquals($resp->code,200);
    }

    public function testGenerateRandomString(){
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');

        $result = $call->generateRandomString(7);
        $this->assertEquals(strlen($result),7);

        $result1 = $call->generateRandomString(32);
        $this->assertEquals(strlen($result1),32);

        $result2 = $call->generateRandomString(64);
        $this->assertEquals(strlen($result2),64);
    }

    public function testOnaCreateWebUrl() {
        $partnerTxID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $state = $call->generateRandomString(7);
        $resp = $call->onaCreateWebUrl($partnerTxID, $partnerTxID, 6000, 'VND',"testing otc", false, [], [], [], [], $state );

        $this->assertNotNull($resp->body);
    }

    public function testOnaOAuth2Token() {
        $partnerTxID = md5(uniqid(rand(), true));
        // $state = MerchantIntegrationOnline::generateRandomString(7);
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $state = $call->generateRandomString(7);
        $resp = $call->onaOAuth2Token($partnerTxID, 'fake');

        $this->assertGreaterThanOrEqual($resp->code,400);
    }

    public function testOnaChargeComplete() {
        $partnerTxID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');

        $resp = $call->onaChargeComplete($partnerTxID, 'fake');

        $this->assertGreaterThanOrEqual(400,$resp->code);
    }

    public function testOnaGetRefundStatus() {
        $partnerTxID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');

        $resp = $call->onaGetRefundStatus($partnerTxID, 'VND','fake');

        $this->assertGreaterThanOrEqual(400,$resp->code);
    }

    public function testPosCreateQRCode() {
        $partnerTxID = md5(uniqid(rand(), true));
        $msgID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOffline('STAGING','Vn','a965c5e5-5efa-4950-b217-1afadc81f811','JBlllk4rZv3Yf1j7','1c334ed5-2acf-471d-9eaa-ca84e6815948','d3949374fa4d4ca9b9449668b');

        $resp = $call->posCreateQRCode($partnerTxID, $msgID, 1000,'VND');

        $this->assertEquals($resp->code,200);
    }

    public function testPosPerformQRCode() {
        $partnerTxID = md5(uniqid(rand(), true));
        $msgID = md5(uniqid(rand(), true));
        $call = new MerchantIntegrationOffline('STAGING','Vn','651b909f-2262-4478-bc23-2b2ee32be627','zV9qnHiR7hNDceHD','84dfaba5-7a1b-4e91-aa1c-f7ef93895266','terminal-xixi');

        $resp = $call->posPerformQRCode($partnerTxID, $msgID, 'SGD', 10000, '000000000000000000');

        $this->assertEquals($resp->code,401);
    }

    public function testBase64URLEncode() {
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $result = $call->base64URLEncode('test=');
        $this->assertEquals(strlen($result),4);

        $result = $call->base64URLEncode('test+');
        $this->assertEquals(strlen($result),5);

        $result = $call->base64URLEncode('test/');
        $this->assertEquals(strlen($result),5);
    }
}