<?php
namespace Moca\Merchant;
use Moca\Merchant\MerchantIntegrationOnline;
use Moca\Merchant\MerchantIntegrationOffline;
use Moca\Merchant\RestClient;
use PHPUnit\Framework\TestCase;

class MerchantIntegrationMockTest extends TestCase{

    public function testOnaChargeInit() {
        $chargeInit = json_encode(array(
                "partnerTxID" => "8b7797298a834f85a346d44b7cfd40d2",
                "request" => "eyJhbGciOiAibm9uZSJ9.eyJjbGFpbXMiOnsidHJhbnNhY3Rpb24iOnsidHhJRCI6ImI5MjYwMGE5NzY1NjRkODg5MzY2NTA2Yjg3MGY3MTE5In19fQ."
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaChargeInit')
             ->willReturn($chargeInit);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($chargeInit, $stub->onaChargeInit($partnerTxID, $partnerTxID, 6000, 'VND',"testing otc", false));
    }
    public function testOnaCreateWebUrl() {
        $webUrl = "https://stg-paysi.moca.vn/grabid/v1/oauth2/authorize?acr_values=consent_ctx%3AcountryCode%3DVN,currency%3DVND&client_id=d07f7cdc3cc543b0b798975b0492543d&code_challenge=BYms1v0CwSArfub9jhi1relMFiPWm1osV1OZ5VUoiss&code_challenge_method=S256&nonce=JJ6v7jHKF1xF5n8F&redirect_uri=https://eats-staging.deliany.co/redirect/checkout-success-page&request=eyJhbGciOiAibm9uZSJ9.eyJjbGFpbXMiOnsidHJhbnNhY3Rpb24iOnsidHhJRCI6ImI5MjYwMGE5NzY1NjRkODg5MzY2NTA2Yjg3MGY3MTE5In19fQ.&response_type=code&scope=payment.vn.one_time_charge&state=DSwL55D";
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaCreateWebUrl')
             ->willReturn($webUrl);

        $partnerTxID = md5(uniqid(rand(), true));
        // $state = MerchantIntegrationOnline::generateRandomString(7);
        $state = $stub->generateRandomString(7);
        $this->assertSame($webUrl, $stub->onaCreateWebUrl($partnerTxID, $partnerTxID, 6000, 'VND',"testing otc", false, [], [], [], [], $state ));
    }
    public function testOnaOAuth2Token() {
        $respOAuthToken = json_encode(array(
            "access_token" =>
            "eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhdWQiOiI1NTA4OGFhZjgzMjI0MDNlYWUxMTc3OTI4Zjk6VXY0NSIsImV4cCI6MTU4MjE4NjIyMCwiaWF0IjoxNTUwNjUwMjIwLCJpbnR
            fc3ZjX2F1dGh6X2N0eCI6ImVjMDFlNTk5MDhkMzQ1ZjM4ZjdkMzRmYTE0YmU2YmRkIiwiaXNzIjoiaHR0cHM6Ly9pZHAuZ3JhYi5jb20iLCJqdGkiOiJteVpIOGtnWFNxcURqalpVcTlselFnIiwibmJmIjoxNTUwNjUwMDQ
            wLCJwaWQiOiI1NjljOTUwMS1mNWU0LTRiZDktOGRmZS01NDFkOGYyYWY4MWUiLCJzY3AiOiJbXCJvcGVuaWRcIixcIjQyYzAxOGQzM2E2YTQ1ZjU4OWI1NDI5MTA4NmZiMTc0XCJdIiwic3ViIjoiNThmYzhlZDYtODNhNy0
            0NGU3LTk5ZmQtZWVhYTUxODg5ODAxIiwic3ZjIjoiUEFTU0VOR0VSIiwidGtfdHlwZSI6ImFjY2VzcyJ9.uwxQUoX9JnVVPBqhGiK7Gis71L_OwzieOsfUSNrk0JnETT8ZGWneNyoWCpQXfBcWukNpvQiHF17kMrqAX_GRUy
            e97xZCY0RiSryaUJ86ywKEK8_wUsopLluGqOXOirnUHmFNuXiEIQChOq2RhTQ1sgShmU0X2xNUC0phq4BfyDn4HxiVHUDXQCICd8bttCQf8oeqXu4vnQLEdnHwoOkTLmBHNU8SuT1mhY2q3rdLyKdpM_1pIPxkI5yXvFTRtQ
            FxALHUFyJaisLp5yj6FBEyGBNaDmXtpj3MFl5CtxtyU1WvQ7vyoYU7lDkInB0rmjB8-0t5nSEY-hCa3bWjiTD5Wg",
             "token_type" => "Bearer",
             "expires_in" => 31535999,
             "id_token" =>
            "eyJhbGciOiJSUzI1NiIsImtpZCI6Il9kZWZhdWx0IiwidHlwIjoiSldUIn0.eyJhY3IiOiJbXCJzZXJ2aWNlOlBBU1NFTkdFUlwiXSIsImF0X2hhc2giOiJYMzFXakw1bkZaVkhFLUh2N0NFa3VRIiwiYXVkIjoiNTUwODh
            hYWY4MzIyNDAzZWFlMTE3NzkyOKH3XjFhNDUiLCJleHAiOjE1NTA5MDk0MjAsImlhdCI6MTU1MDY1MDIyMCwiaXNzIjoiaHR0cHM6Ly9pZHAuZ3JhYi5jb20iLCJqdGkiOiJ0QjhZLTZYR1NnR1Z3dkZvcTJJekNnIiwibmJ
            mIjoxNTUwNjUwMjIwLCJub25jZSI6IlRJSnBNSkltZmlEZGJwSTIiLCJwaWQiOiI1NjljOTUwMS1mNWU0LTRiZDktOGRmZS01NDFkOGYyYWY4MWUiLCJzdWIiOiI1OGZjOGVkNi04M2E3LTQ0ZTctOTlmZC1lZWFhNTE4ODk
            4MDEiLCJzdmMiOiJQQVNTRU5HRVIiBVC0a190eXBlIjoiaWQifQ.hYMQxqj3tWwQi8Cf4vaN2dMzEtd-0CbdrP7fEvgITm7LpIkDW5E0vTyy0zMf-HKs6YvNVwslDjf2bxTRkyUR3gQfXlhRp3WXe05D7X9Wle-5se_H_QI_
            t_fTdHq5lb2_EEw6iHk2Oh4GFr-ADyTzVcA6mmLPqdENYewX1QZflhm827CB2pUoZ8e0qx0K8PqZNeN4itgtgectM75pdgrkX52Wm2E6sihTZK9_yF4wUOf9L70x8C5RRrMTqLFDjuDTa2krSvWWRN27vA02RURQvzlpGlT9
            yM7eI0sqkfRqzFdSHVPgOET2cutVa8zqj6xFw4E3sQ1v25sPaqrTmOlLsA"
            
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaOAuth2Token')
             ->willReturn($respOAuthToken);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($respOAuthToken, $stub->onaOAuth2Token($partnerTxID, "testing otc"));
    }
    public function testOnaChargeComplete() {
        $chargeComplete = json_encode(array(
            "txID"=>"66f3f271cc994f1bb1e46e6fe4fd8727",
            "status"=>"success",
            "paymentMethod"=>"INSTALMENT_4",
            "description"=>"",
            "txStatus"=> "success",
            "reason"=> ""
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaChargeComplete')
             ->willReturn($chargeComplete);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($chargeComplete, $stub->onaChargeComplete($partnerTxID,"testing otc"));
    }
    public function testOnaGetChargeStatus() {
        $getChargeStatus = json_encode(array(
            "txID"=>"66f3f271cc994f1bb1e46e6fe4fd8727",
            "status"=>"success",
            "paymentMethod"=>"INSTALMENT_4",
            "description"=>"",
            "txStatus"=> "success",
            "reason"=> ""
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaGetChargeStatus')
             ->willReturn($getChargeStatus);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($getChargeStatus, $stub->onaGetChargeStatus($partnerTxID, 'VND',"testing otc"));
    }
    public function testonaRefund() {
        $refund = json_encode(array(
            "txID"=>"66f3f271cc994f1bb1e46e6fe4fd8727",
            "status"=>"success",
            "paymentMethod"=>"INSTALMENT_4",
            "description"=>"",
            "txStatus"=> "success",
            "reason"=> ""
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaRefund')
             ->willReturn($refund);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($refund, $stub->onaRefund($partnerTxID, $partnerTxID, 6000, 'VND',"testing otc", '',''));
    }
    public function testOnaGetRefundStatus() {
        $refundStatus = json_encode(array(
            "txID"=>"66f3f271cc994f1bb1e46e6fe4fd8727",
            "status"=>"success",
            "paymentMethod"=>"INSTALMENT_4",
            "description"=>"",
            "txStatus"=> "success",
            "reason"=> "",
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaGetRefundStatus')
             ->willReturn($refundStatus);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($refundStatus, $stub->onaGetRefundStatus($partnerTxID, 'VND',"testing otc"));
    }
    public function testonaGetOTCStatus() {
        $OtcStatus = json_encode(array(
            "txID"=> "1234567abcdefghijklmnd0123456789",
            "oAuthCode"=> "57498632557928629184521",
            "status"=> "unknown",
            "paymentMethod" => "GPWALLET",
            "txStatus"=> "authorised",
            "reason"=> "pending_capture"
        ));
        $stub = $this->createStub(MerchantIntegrationOnline::class);
        $stub->method('onaGetOTCStatus')
             ->willReturn($OtcStatus);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($OtcStatus, $stub->onaGetOTCStatus($partnerTxID, 'VND'));
    }

    public function testPosCreateQRCode() {
        $createQR = json_encode(array(
            "msgID"=> "359c530dd7f741762a5e6c930c800rwt",
            "qrcode"=> "00020101021226522222com.grab01363ad48f98-35d5-4441-ae2b-cda98b1c36f35204799953036085402105802PH5913Paynamics POS6011LamitanCity610634534462400507CEaNxFh0725ed62668ceb1efac8e879c218a64211112EN0113Paynamics POS6304B335",
            "txID"=> "119e9e3ef86c433a9918ce62b6004a33"
        ));
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posCreateQRCode')
             ->willReturn($createQR);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($createQR, $stub->posCreateQRCode($partnerTxID, $partnerTxID, 6000, 'VND'));
    }
    public function testPosCancel() {
        $cancel = json_encode(array());
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posCancel')
             ->willReturn($cancel);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($cancel, $stub->posCancel($partnerTxID, $partnerTxID, $partnerTxID,"testing", 'VND'));
    }
    public function testposRefund() {
        $refund = json_encode(array(
            "msgID"=> "b543f1b9d938443486d76206a2df5a51",
            "txID"=> "bcf1a8bf24f348da94786c6cdd07e2d7",
            "originTxID"=> "119e9e3ef86c433a9918ce62b6004a33",
            "status"=> "success",
            "description"=> "",
            "additionalInfo"=> array(
                "amountBreakdown"=> array(
                   "paidAmount"=> 7000,
                   "merchantAcquisitionPromoAmount"=> 3000,
                   "bonusPointsMultiplier"=> "3x",
                   "bonusPointsAwarded"=> 210
                )
            )
        ));
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posRefund')
             ->willReturn($refund);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($refund, $stub->posRefund($partnerTxID, $partnerTxID, 6000, 'VND','test',"testing"));
    }
    public function testPosPerformQRCode() {
        $performQR = json_encode(array(
            "msgID"=> "e24106b077ef4d1aa734e0d007092e79",
            "txID"=> "7a3ca25265a90133bc525ca46bf2273a",
            "status"=> "success",
            "amount"=> 10000,
            "updated"=> 1596779878,
            "currency"=> "SGD",
            "additionalInfo"=> array(
                "amountBreakdown"=> array(
                   "paidAmount"=> 7000,
                   "merchantAcquisitionPromoAmount"=> 3000,
                   "bonusPointsMultiplier"=> "3x",
                   "bonusPointsAwarded"=> 210
                    )
            )
        ));
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posPerformQRCode')
             ->willReturn($performQR);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($performQR, $stub->posPerformQRCode($partnerTxID, $partnerTxID, 6000, 'VND',"testing"));
    }
    public function testposGetTxnStatus() {
        $chargeStatus = json_encode(array(
            "msgID"=> "b543f1b9d938443486d76206a2df5a51",
            "txID"=> "bcf1a8bf24f348da94786c6cdd07e2d7",
            "originTxID"=> "119e9e3ef86c433a9918ce62b6004a33",
            "status"=> "success",
            "description"=> "",
            "additionalInfo"=> array(
                "amountBreakdown"=> array(
                   "paidAmount"=> 7000,
                   "merchantAcquisitionPromoAmount"=> 3000,
                   "bonusPointsMultiplier"=> "3x",
                   "bonusPointsAwarded"=> 210
                )
            )
        ));
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posGetTxnStatus')
             ->willReturn($chargeStatus);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($chargeStatus, $stub->posGetTxnStatus($partnerTxID, $partnerTxID, 'VND'));
    }
    public function testPosGetRefundStatus() {
        $refundStatus = json_encode(array(
            "msgID"=> "b543f1b9d938443486d76206a2df5a51",
            "txID"=> "bcf1a8bf24f348da94786c6cdd07e2d7",
            "originTxID"=> "119e9e3ef86c433a9918ce62b6004a33",
            "status"=> "success",
            "description"=> "",
            "additionalInfo"=> array(
                "amountBreakdown"=> array(
                   "paidAmount"=> 7000,
                   "merchantAcquisitionPromoAmount"=> 3000,
                   "bonusPointsMultiplier"=> "3x",
                   "bonusPointsAwarded"=> 210
                )
            )
        ));
        $stub = $this->createStub(MerchantIntegrationOffline::class);
        $stub->method('posGetRefundStatus')
             ->willReturn($refundStatus);

        $partnerTxID = md5(uniqid(rand(), true));

        $this->assertSame($refundStatus, $stub->posGetRefundStatus($partnerTxID, $partnerTxID, 'VND'));
    }
}
?>
