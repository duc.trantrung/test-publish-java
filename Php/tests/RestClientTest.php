<?php
namespace Moca\Merchant;
use Moca\Merchant\RestClient;
use Moca\Merchant\MerchantIntegrationOnline;
use Moca\Merchant\MerchantIntegrationOffline;
use PHPUnit\Framework\TestCase;

class RestClientTest extends TestCase
{
    public function testSha256()
    {
        $result = RestClient::sha256('OFFLINE');
        $this->assertEquals(strlen($result),44);
    }

    public function testBase64URLEncode() {
        $result = RestClient::base64URLEncode('test=');
        $this->assertEquals(strlen($result),4);

        $result = RestClient::base64URLEncode('test+');
        $this->assertEquals(strlen($result),5);

        $result = RestClient::base64URLEncode('test/');
        $this->assertEquals(strlen($result),5);
    }

    public function testGenerateHmac() {
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $env = $call->getpartnerInfo();
        $result = RestClient::generateHmac($env, 'GET', '/some-path', 'application/json', [], 'Sun, 07 Nov 2021 03:59:44 GMT');
        $this->assertEquals($result,"ohkn+cI+2Ewr6IIwyHSIbLalPJkWTQ5y1HL02O7T0yg=");
    }

    public function testGeneratePOPSig() {
        $call = new MerchantIntegrationOnline('STAGING','Vn','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $env = $call->getpartnerInfo();
        $result = RestClient::generatePOPSig($env, 'some-access-token', 1636258588.112487);          
        $this->assertEquals($result,"eyJ0aW1lX3NpbmNlX2Vwb2NoIjoxNjM2MjU4NTg4LjExMjQ4Nywic2lnIjoiWWphQ0U2NXZ2a1A2c1VlbXlNQ1pqNWRuYmJPNWNaTW5QclllaWNNNzF4ayJ9");
    }

    public function testSendRequest() {
        // case 1: get and offline request
        $offline = new MerchantIntegrationOffline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','6a6f98dd2dd903f9da03f2139');
        $envOffline = $offline->getpartnerInfo();
        $partnerTxID = $offline->generateRandomString(32);
        $currency = 'VND';
        $url = str_replace("{PartnerTxID}",$partnerTxID,$envOffline['posChargeStatus']);
        $url = str_replace("{currency}",$currency,$url);
        $result = RestClient::sendRequest($envOffline,'GET', $url, 'application/json','', 'OFFLINE','');
        if($result) {
            $this->assertGreaterThanOrEqual(400,$result->code);
        }
        // test put
        $bodyCancel = array(
            "msgID" => md5(uniqid(rand(), true)),
            "grabID" => "84dfaba5-7a1b-4e91-aa1c-f7ef93895266",
            "terminalID" => "terminal25-meow-meow-meow",
            "currency" => "VND",
            "origTxID" => "partner-xi-1",
            "partnerTxID" => md5(uniqid(rand(), true)) 
        );
        $result2 = RestClient::sendRequest($envOffline,'PUT', $envOffline['cancelQrTxn'],'application/json', $bodyCancel, 'OFFLINE','');
        if($result2) {
            $this->assertGreaterThanOrEqual(400,$result2->code);
        }
        // test post
        $call = new MerchantIntegrationOnline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $envOnline = $call->getpartnerInfo();
        $requestBody = array(
            'partnerTxID'       => md5(uniqid(rand(), true)),
            'partnerGroupTxID'  => md5(uniqid(rand(), true)),
            'amount'            => 10000,
            'currency'          => 'VND',
            'merchantID'        => $envOnline['merchantID'],
            'description'       => 'test',
            'isSync'            => false
        );
        $result3 = RestClient::sendRequest($envOnline,'POST', $envOnline['chargeInit'],'application/json',$requestBody, 'ONLINE',''); 
        if($result3) {
            $this->assertEquals($result3->code,200);
        }

        // test oauth code
        $respOAuthCode = RestClient::sendRequest($envOnline,'POST',$envOnline['OAuth2Token'],'application/json',array('partnerTxID'=> md5(uniqid(rand(), true)),'code' =>'fake'), 'ONLINE','');
        if($respOAuthCode) {
            $this->assertGreaterThanOrEqual(400,$respOAuthCode->code);
        }

        // test get charge status
        $respChargeStatus = RestClient::sendRequest($envOnline,'POST',$envOnline['onaChargeStatus'],'application/json',array('partnerTxID'=> md5(uniqid(rand(), true)),'currency'=> 'VND','accessToken' =>'fake'), 'ONLINE','');
        if($respChargeStatus) {
            $this->assertGreaterThanOrEqual(400,$respChargeStatus->code);
        }

    }

    public function testGet() {
        $offline = new MerchantIntegrationOffline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','6a6f98dd2dd903f9da03f2139');
        $env = $offline->getpartnerInfo();
        $partnerTxID = $offline->generateRandomString(32);
        $currency = 'VND';
        $url = str_replace("{PartnerTxID}",$partnerTxID,$env['posChargeStatus']);
        $url = str_replace("{currency}",$currency,$url);
        $result = RestClient::get($env, $url, 'application/json', 'OFFLINE');
        if($result) {
            $this->assertGreaterThanOrEqual(400,$result->code);
        }
    }

    public function testPost() {
        $call = new MerchantIntegrationOnline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result');
        $env = $call->getpartnerInfo();
        $requestBody = array(
            'partnerTxID'       => md5(uniqid(rand(), true)),
            'partnerGroupTxID'  => md5(uniqid(rand(), true)),
            'amount'            => 10000,
            'currency'          => 'VND',
            'merchantID'        => $env['merchantID'],
            'description'       => 'test',
            'isSync'            => false
        );
        $result = RestClient::post($env, $env['chargeInit'],$requestBody, 'ONLINE'); 
        if($result) {
            $this->assertEquals($result->code,200);
        }
    }

    public function testPut() {
        $offline = new MerchantIntegrationOffline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','6a6f98dd2dd903f9da03f2139');
        $env = $offline->getpartnerInfo();
        $bodyCancel = array(
            "msgID" => md5(uniqid(rand(), true)),
            "grabID" => "84dfaba5-7a1b-4e91-aa1c-f7ef93895266",
            "terminalID" => "terminal25-meow-meow-meow",
            "currency" => "VND",
            "origTxID" => "partner-xi-1",
            "partnerTxID" => md5(uniqid(rand(), true)) 
        );
        $result = RestClient::put($env, $env['cancelQrTxn'], $bodyCancel,'application/json', 'OFFLINE');
        if($result) {
            $this->assertEquals($result->code,400);
        }
    
    }
}
