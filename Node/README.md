# How to use SDK

## Prerequisite

Before using this SDK, you should run command below to install all needed package.
```terminal
    npm install
```

For using this SDK, You might copy all files in [src](./src) folder and paste it to your project. 

# How to run the sample follow
For One-time Payments
```terminal
    npm run exampleOnA
```

For Payments using QR Code
```terminal
    npm run examplePos
```

# How to run unit test
```terminal
    npm run test
```    

# Config:
```
    # environment : accept value 'STG','PRD'
    # country: which country you live(VN,SG, ID, MY,...)
    # partnerId: the partnerId from integration team share to merchant
    # partnerSecret: the partnerSecret from integration team share to merchant
    # merchantId: the merchantId from integration team share to merchant
    # terminalId: the terminalId from integration team share to merchant. It need config when use offline transaction
    # clientId: the clientId from integration team share to merchant. It need config when use online transaction
    # clientSecret: the clientSecret from integration team share to merchant. It need config when use online transaction
    # redirectUri: the redirectUri from integration team share to merchant. It need config when use online transaction
```
# Sequence diagram 


# How to configure for online acception

```
    const clientOnline = new MerchantIntegrationOnline('stage','country','partner_id','partner_secret','merchant_id','client_id','client_secret','redirectUri')
```

# How to configure for POS
```
     const clientOffline = new MerchantIntegrationOffline('stage','country','partner_id','partner_secret','merchant_id','terminal_id')
```
# Detail for API online acception 

1. Charge Init: 
```
    respChargeInit = clientOnline.onaChargeInit(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods)
```

2. Create Web Url: 
```
    respChargeInit = clientOnline.onaCreateWebUrl(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods, state)
```

3. get OAuth Token

```
    respAuthCode = clientOnline.onaOAuthToken(partnerTxID,code)
```

4. Charge Complete
```
    respChargeComplete = clientOnline.onaChargeComplete(partnerTxID,respAuthCode.access_token)
```

5 Get Charge status
```
    respChargeStatus = clientOnline.onaGetChargeStatus(partnerTxID, currency, respAuthCode.accessToken)
```

6. refund online
```
    response = clientOnline.onaRefund(refundPartnerTxID, amount, origPartnerTxID, description, respAuthCode.accessToken, currency)
```

7. Get Refund Status
```
    response = clientOnline.onaGetRefundStatus(partnerTxID, respAuthCode.accessToken, currency)
```

8. Get One time charge status
```
    response = clientOnline.onaGetOtcStatus(partnerTxID, respAuthCode.accessToken, currency)
```

# Detail for API offline

1. Create QR Code
```
    respCreateQRCode= clientOffline.posCreateQRCode(msgID, partnerTxID, amount, currency)
```

2. Get transaction detail:
```
    resp= clientOffline.posGetTxnDetails(msgID, partnerTxID, currency)
```

3. Cancel a Transaction
```file
    resp= clientOffline.posCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency)
```

4. Refund a POS Payment
```file
    resp= clientOffline.posRefund(msgID, refundPartnerTxID, amount, currency, origPartnerTxID, description)
```

5. Get refund transaction detail:
```
    resp= clientOffline.posGetRefundDetails(msgID, refundPartnerTxID, currency)
```

6. Perform a Transaction
```file
    resp= clientOffline.posPerformQRCode(msgID, partnerTxID, amount, currency, code)
```
