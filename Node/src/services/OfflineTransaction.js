import * as Utils from '../utils/Utils';

export const apiCreateQrCode = async (payload, config, requester) => {
  const credentials = getCredentials(config);
  const data = {
    ...credentials,
    ...payload,
  };

  return requester
      .hmacRequest({
        url: config.urls.POS_CREATE_QR_CODE,
        method: 'POST',
        data,
      });
};

export const apiPerformQRCode = async (payload, config, requester) => {
  const credentials = getCredentials(config);
  const data = {
    ...credentials,
    ...payload,
  };

  return requester
      .hmacRequest({
        url: config.urls.POS_PERFORM_TRANSACTION,
        method: 'POST',
        data,
      });
};

export const apiCancel = async (payload, config, requester) => {
  const credentials = getCredentials(config);
  const data = {
    ...credentials,
    ...payload,
  };
  const {origPartnerTxID} = payload;
  const url = Utils.replaceUrl(config.urls.POS_CANCEL_TRANSACTION, {
    origPartnerTxID,
  });

  return requester
      .hmacRequest({
        url,
        method: 'PUT',
        data
      });
};

export const apiRefund = async (payload, config, requester) => {
  const credentials = getCredentials(config);
  const { origPartnerTxID, ...req } = payload;
  const url = Utils.replaceUrl(config.urls.POS_REFUND_TRANSACTION, {
    origPartnerTxID,
  });
  const data = {
    ...credentials,
    ...req,
  };

  return requester
      .hmacRequest({
        url,
        method: 'PUT',
        data
      });
};

export const apiGetTxnDetails = async (payload, config, requester) => {
  const credentials = getCredentials(config);
  const { partnerTxID, currency, txType } = payload;
  const url = Utils.replaceUrl(config.urls.POS_GET_TXN_DETAIL, {
    partnerTxID,
  });

  return requester
      .hmacRequest({
        url,
        method: 'GET',
        params: {
          currency,
          txType,
          ...credentials,
        }
      });
};

const getCredentials = (config) => {
  return {
    msgID: Utils.generateMsgId(),
    grabID: config.merchantId,
    terminalID: config.terminalId
  }
};
