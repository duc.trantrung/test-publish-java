import CryptoJS from 'crypto-js';
import queryString from 'query-string';

import * as Utils from '../utils/Utils';

export const apiChargeInit = async (payload, config, requester) => {
  const { merchantId } =  config.getConfigs();
  const data = {
    ...payload,
    merchantID: merchantId,
  };

  return requester
      .hmacRequest({
        url: config.urls.ONA_CHARGE_INIT,
        method: 'POST',
        data,
      });
};

export const apiOAuth2Token = (payload, config, requester) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { partnerTxID, code } = payload;
      const { clientId, clientSecret, redirectUrl } = config.getConfigs();

      const data = {
        'grant_type'    : 'authorization_code',
        'client_id'     : clientId,
        'client_secret' : clientSecret,
        'code_verifier' : Utils.base64URLEncode(partnerTxID+partnerTxID),
        'redirect_uri'  : redirectUrl,
        'code'          : code
      };

      const response = await requester
          .request({
            url: config.urls.OAUTH_TOKEN,
            headers: {
              'Content-Type': 'application/json',
            },
            method: 'POST',
            data,
          });

      resolve(response)
    } catch (error) {
      console.log(error.response.data)
      reject(error)
    }
  })
};

export const createWebUrl = async (payload, config) => {
  const { clientId, redirectUrl, scope } = config.getConfigs();
  const { countryCode, currency, request, state, partnerTxID } = payload;
  let codeVerifier = Utils.base64URLEncode(partnerTxID + partnerTxID);
  let codeChallenge = Utils.base64URLEncode(CryptoJS.SHA256(codeVerifier));
  
  const params = {
    'acr_values':  "consent_ctx:countryCode=" + countryCode + ",currency=" + currency,
    'client_id': clientId,
    'code_challenge': codeChallenge,
    'code_challenge_method': 'S256',
    'nonce': Utils.generateRandomString(16),
    'redirect_uri': redirectUrl,
    'request': request,
    'response_type': 'code',
    'scope' : scope,  
    'state' : state
  };

  return [
    config.envs.baseUrl,
    `/grabid/v1/oauth2/authorize?`,
    queryString.stringify(params)
  ].join('');
};

export const apiChargeComplete = async (payload, config, requester) => {
  const { accessToken, ...data } = payload;
  return requester
      .request({
        url: config.urls.ONA_CHARGE_COMPLETE,
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Date': Utils.getGMTString(),
          'X-GID-AUX-POP': Utils.generatePOPSignature(accessToken, config.clientSecret),
          'Authorization': `Bearer ${accessToken}`
        },
        data,
      });
};

export const apiGetChargeStatus = async (payload, config, requester) => {
  const { accessToken, currency, partnerTxID } = payload;
  const url = Utils.replaceUrl(config.urls.ONA_CHARGE_STATUS, {
    partnerTxID,
  });
  return requester
      .request({
        url: url,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Date': Utils.getGMTString(),
          'X-GID-AUX-POP': Utils.generatePOPSignature(accessToken, config.clientSecret),
          'Authorization': `Bearer ${accessToken}`
        },
        params: {
          currency,
        }
      });
};

export const apiRefund = async (payload, config, requester) => {
  const { merchantId } = config.getConfigs();
  const { accessToken, ...data } = payload;
  data['merchantID'] = merchantId;

  return requester
      .request({
        url: config.urls.ONA_REFUND,
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Date': Utils.getGMTString(),
          'X-GID-AUX-POP': Utils.generatePOPSignature(accessToken, config.clientSecret),
          'Authorization': `Bearer ${accessToken}`
        },
        data,
      });
};

export const apiGetRefundStatus = async (payload, config, requester) => {
  const { accessToken, refundPartnerTxID, currency } = payload;
  const url = Utils.replaceUrl(config.urls.ONA_REFUND_STATUS, {
    refundPartnerTxID,
  });

  return requester
      .request({
        url,
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Date': Utils.getGMTString(),
          'X-GID-AUX-POP': Utils.generatePOPSignature(accessToken, config.clientSecret),
          'Authorization': `Bearer ${accessToken}`
        },
        params: {
          currency,
        }
      });
};

export const apiGetOTCStatus = async (payload, config, requester) => {
  const { partnerTxID, currency } = payload;
  const url = Utils.replaceUrl(config.urls.ONA_ONE_TIME_CHARGE_STATUS, {
    partnerTxID,
  });

  return requester
      .hmacRequest({
        url,
        method: 'GET',
        params: {
          currency,
        }
      });
};
