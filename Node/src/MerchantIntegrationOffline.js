import Requester from './utils/Requester';
import Config from './configs/Config';
import { SDK_VERSION } from './configs/Version';
import { OfflineTransactionService } from './services';

class MerchantIntegrationOffline {
  _requester;
  _config;

  /**
   * Constructor
   *
   * @param environment
   * @param country
   * @param partnerId
   * @param partnerSecret
   * @param merchantId
   * @param terminalId
   */
  constructor(environment, country, partnerId, partnerSecret, merchantId, terminalId) {
    country = country.toLowerCase();
    environment = environment.toLowerCase();
    this._config = new Config(environment, country, partnerId, partnerSecret, merchantId, terminalId, "", "", "");
    this._requester = new Requester(this._config);
  }

  /**
   * Create QR Code
   *
   * @param msgID
   * @param partnerTxID
   * @param amount
   * @param currency
   * @returns {Promise<*>}
   */
   async posCreateQRCode(msgID, partnerTxID, amount, currency) {
    return OfflineTransactionService.apiCreateQrCode({
      msgID,
      partnerTxID,
      amount,
      currency
    }, this._config, this._requester);
  }

  /**
   * Perform a Transaction
   *
   * @param msgID
   * @param partnerTxID
   * @param amount
   * @param currency
   * @param code
   * @returns {Promise<*>}
   */
  async posPerformQRCode(msgID, partnerTxID, amount, currency, code) {
    return OfflineTransactionService.apiPerformQRCode({
      msgID,
      partnerTxID,
      amount,
      currency,
      code,
    }, this._config, this._requester);
  }

  /**
   * Cancel a Transaction
   *
   * @param msgID
   * @param partnerTxID
   * @param origPartnerTxID
   * @param origTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency) {
    return OfflineTransactionService.apiCancel({
      msgID,
      partnerTxID,
      origPartnerTxID,
      origTxID,
      currency,
    }, this._config, this._requester);
  }

  /**
   * Refund a POS Payment
   *
   * @param msgID
   * @param refundPartnerTxID
   * @param amount
   * @param currency
   * @param origPartnerTxID
   * @param description
   * @returns {Promise<*>}
   */
  async posRefund(msgID, refundPartnerTxID, amount, currency, origPartnerTxID, description) {
    return OfflineTransactionService.apiRefund({
      msgID,
      partnerTxID: refundPartnerTxID,
      amount,
      currency,
      origPartnerTxID,
      reason: description,
    }, this._config, this._requester);
  }

  /**
   * Check Transaction Details
   *
   * @param msgID
   * @param partnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posGetTxnDetails(msgID, partnerTxID, currency) {
    return OfflineTransactionService.apiGetTxnDetails({
      msgID,
      partnerTxID,
      currency,
      txType: 'P2M',
    }, this._config, this._requester);
  }

  /**
   * Check Refund Details
   *
   * @param msgID
   * @param refundPartnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posGetRefundDetails(msgID, refundPartnerTxID, currency) {
    return OfflineTransactionService.apiGetTxnDetails({
      msgID,
      partnerTxID: refundPartnerTxID,
      currency,
      txType: 'Refund',
    }, this._config, this._requester);
  }
  
  /**
   * Get SDK Version
   *
   * @returns {string}
   */
  getVersion() {
    return SDK_VERSION
  }
}

export default MerchantIntegrationOffline;
