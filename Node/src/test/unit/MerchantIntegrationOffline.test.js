import { OfflineTransactionService } from '../../services';
import MerchantIntegrationOffline from '../../MerchantIntegrationOffline';
import * as Utils from '../../utils/Utils';

const partnerTxID = Utils.generateRandomString(32);
const msgID = Utils.generateMsgId();
const origPartnerTxID = Utils.generateRandomString(32);

describe('MerchantIntegrationOffline test', () => {
  let client;

  beforeAll(() => {
    client = new MerchantIntegrationOffline('stg', 'VN', 'partner-id', 'partner-secret','merchant-id', 'termination-id');
  });

  it('Test posCreateQRCode', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiCreateQrCode');
    client.posCreateQRCode(msgID, partnerTxID, 100000, 'VND');
    await expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          amount: 100000,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posPerformQRCode', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiPerformQRCode');
    client.posPerformQRCode(msgID, partnerTxID, 100000, 'VND', 'code');
    await expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          amount: 100000,
          code: 'code',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posCancel', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiCancel');
    client.posCancel(msgID, partnerTxID, origPartnerTxID, 'txi',  "VND");
    await expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          origPartnerTxID,
          msgID,
          origTxID: 'txi',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posRefund', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiRefund');
    client.posRefund(msgID, partnerTxID, 100000, 'VND',  origPartnerTxID, "refund");
    await expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          reason: 'refund',
          msgID,
          amount: 100000,
          origPartnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posGetTxnDetails', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiGetTxnDetails');
    client.posGetTxnDetails(msgID, partnerTxID, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          txType: 'P2M',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posGetRefundDetails', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiGetTxnDetails');
    client.posGetRefundDetails(msgID, partnerTxID, 'VND');
    await expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          txType: 'Refund',
        },
        expect.anything(),
        expect.anything()
    );
  });
});
