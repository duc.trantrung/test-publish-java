import Requester from '../../utils/Requester';
import Config from '../../configs/Config';
import {SDK_SIGNATURE, SDK_VERSION} from "../../configs/Version";
import * as Utils from "../../utils/Utils"
import sinon from 'sinon';

describe('Requests tests', () => {
    let testRequest;
    let config;

    beforeAll(()=>{
        config = new Config('stg', 'vn', 'partnerId', 'partnerSecret', 'merchantId', '', 'clientId', 'clientSecret', 'redirectUrl');
        testRequest = new Requester(config);
    });


    it('Test request', () =>{
        let data = {
            partnerID : 'partner_id',
            partnerSecret : 'partner_secret'
        };

        let header = {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }

        let inputConfig = {
            url : '/this/is/url',
            method : 'POST',
            headers : header,
            data
        };

        header = {
            ...header,
            'X-Sdk-Version': SDK_VERSION,
            'X-Sdk-Country': config.country,
            'X-Sdk-Language': 'nodejs',
            'X-Sdk-Signature': SDK_SIGNATURE
        }

        const spy = jest.spyOn(testRequest._requester, 'request');
        testRequest.request(inputConfig);
        expect(spy).toHaveBeenCalledWith({
            url : '/this/is/url',
            method : 'POST' ,
            data,
            headers : header
        });
    });

    it('Test hmacRequest', () =>{
        let data = {
            partnerID : 'partner_id',
            partnerSecret : 'partner_secret'
        };

        let header = {
            'Accept' : 'application/json',
            'Content-Type': 'application/json'
        }

        let inputConfig = {
            url : '/this/is/url',
            method : 'POST',
            headers : header,
            data
        };

        const date = 'Mon, 22 Dec 2021 11:55:00 GMT';
        const hmac = 'this_is_hmac';
        header = {
            ...header,
            'Date': date,
            'Authorization': `partnerId:${hmac}`,
            'X-Sdk-Version': SDK_VERSION,
            'X-Sdk-Country': config.country,
            'X-Sdk-Language': 'nodejs',
            'X-Sdk-Signature': SDK_SIGNATURE
        }

        const spy = jest.spyOn(testRequest._requester, 'request');
        sinon.stub(Utils, 'generateHmac').returns(hmac);
        sinon.stub(Utils, 'getGMTString').returns(date);

        // test post
        testRequest.hmacRequest(inputConfig);
        expect(spy).toHaveBeenCalledWith({
            url : '/this/is/url',
            method : 'POST' ,
            data,
            headers : header
        });

        //test get
        inputConfig.method = 'GET';
        testRequest.hmacRequest(inputConfig);
        expect(spy).toHaveBeenCalledWith({
            url : '/this/is/url',
            method : 'GET' ,
            headers : header
        });
    });

});