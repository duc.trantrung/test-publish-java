import {OnlineTransactionService} from '../../services';
import MerchantIntegrationOnline from '../../MerchantIntegrationOnline';
import * as Utils from '../../utils/Utils';

const partnerTxID = Utils.generateRandomString(32);
const partnerGroupTxID = Utils.generateRandomString(32);

describe('MerchantIntegrationOnline test', () => {
  let client;

  beforeAll(() => {
    client = new MerchantIntegrationOnline('stg', 'VN', 'partner-id', 'partner-secret','merchant-id', 'client-id', 'client-secret', 'http://localhost:8888/result');
  });

  it('Test onaChargeInit', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiChargeInit');
    client.onaChargeInit(partnerTxID, partnerGroupTxID, 100000, 'VND', false, 'testing otc', null, null, null, null);
    expect(spy).toHaveBeenCalledWith(
      {
        partnerTxID,
        partnerGroupTxID,
        'amount': 100000,
        'currency': 'VND',
        'isSync': false,
        'description': 'testing otc'
      },
      expect.anything(),
      expect.anything()
    );
  });

  // hidenpaymentmethod != null
  it('Test onaChargeInit', async () => {
    const hidePaymentMethods = ['POSTPAID'];
    const spy = jest.spyOn(OnlineTransactionService, 'apiChargeInit');
    client.onaChargeInit(partnerTxID, partnerGroupTxID, 100000, 'VND', false, 'testing otc', null, null, null, hidePaymentMethods);
    expect(spy).toHaveBeenCalledWith(
      {
        partnerTxID,
        partnerGroupTxID,
        'amount': 100000,
        'currency': 'VND',
        'isSync': false,
        'description': 'testing otc',
        hidePaymentMethods
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaCreateWebUrl', async () => {
    const state = Utils.generateRandomString(7);
    const spyApiChargeInit = jest.spyOn(OnlineTransactionService, 'apiChargeInit').mockReturnValue(Promise.resolve({request: 'token'}));
    client.onaCreateWebUrl(partnerTxID, partnerGroupTxID, 100000, 'VND', false, 'testing otc', null, null, null,null, state);
    expect(spyApiChargeInit).toHaveBeenCalledWith(
      {
        partnerTxID,
        partnerGroupTxID,
        'amount': 100000,
        'currency': 'VND',
        'description': 'testing otc',
        'isSync': false
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaOAuth2Token', async () => {
    const spyApiOAuth2Token = jest.spyOn(OnlineTransactionService, 'apiOAuth2Token');
    client.onaOAuth2Token(partnerTxID, 'testing');

    expect(spyApiOAuth2Token).toHaveBeenCalledWith(
        {
          partnerTxID,
          'code': 'testing'
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaChargeComplete', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiChargeComplete').mockReturnValue(Promise.resolve({request: 'token'}));
    client.onaChargeComplete(partnerTxID, 'testing');
    expect(spy).toHaveBeenCalledWith(
        {
          partnerTxID,
          'accessToken': 'testing'
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaGetChargeStatus', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiGetChargeStatus');
    client.onaGetChargeStatus(partnerTxID, 'VND', 'testing');
    expect(spy).toHaveBeenCalledWith(
        {
          accessToken: 'testing',
          currency: 'VND',
          partnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaRefund', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiRefund');
    client.onaRefund(partnerTxID, partnerGroupTxID, 100000, 'VND', '34234', 'test', 'token');
    expect(spy).toHaveBeenCalledWith(
      {
        'accessToken': 'token',
        partnerTxID,
        partnerGroupTxID,
        amount: 100000,
        currency: 'VND',
        description: 'test',
        originTxID: '34234'
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaGetOTCStatus', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiGetOTCStatus');
    client.onaGetOTCStatus(partnerTxID, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });
});