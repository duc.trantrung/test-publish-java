import {OnlineTransactionService, OfflineTransactionService} from '../../services';
import MerchantIntegration from '../../MerchantIntegration';
import * as Utils from '../../utils/Utils';

const partnerTxID = Utils.generateRandomString(32);
const partnerGroupTxID = Utils.generateRandomString(32);
const msgID = Utils.generateMsgId();
const origPartnerTxID = Utils.generateRandomString(32);

describe('MerchantIntegration test', () => {
  let client;

  beforeAll(() => {
    client = new MerchantIntegration('stg', 'VN', 'partner-id', 'partner-secret','merchant-id', 'termination-id', 'client-id', 'client-secret', 'http://localhost:8888/result');
  });

  it('Test onaChargeInit', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiChargeInit');
    client.onaChargeInit(partnerTxID, partnerGroupTxID, 100000, 'VND', false, 'testing otc', null, null, null, null);
    expect(spy).toHaveBeenCalledWith(
      {
        'amount': 100000,
        'currency': 'VND',
        'description': 'testing otc',
        'isSync': false,
        'partnerGroupTxID': partnerGroupTxID,
        'partnerTxID': partnerTxID
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaCreateWebUrl', async () => {
    const state = Utils.generateRandomString(7);
    const spyApiChargeInit = jest.spyOn(OnlineTransactionService, 'apiChargeInit').mockReturnValue(Promise.resolve({request: 'token'}));
    client.onaCreateWebUrl(partnerTxID, partnerGroupTxID, 100000, 'VND', false, 'testing otc', null, null, null, null, state);
    expect(spyApiChargeInit).toHaveBeenCalledWith(
      {
        'amount': 100000,
        'currency': 'VND',
        'description': 'testing otc',
        'isSync': false,
        'partnerGroupTxID': partnerGroupTxID,
        'partnerTxID': partnerTxID
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaOAuth2Token', async () => {
    const spyApiOAuth2Token = jest.spyOn(OnlineTransactionService, 'apiOAuth2Token');
    client.onaOAuth2Token(partnerTxID, 'testing');

    expect(spyApiOAuth2Token).toHaveBeenCalledWith(
        {
          'code': 'testing',
          'partnerTxID': partnerTxID
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaChargeComplete', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiChargeComplete').mockReturnValue(Promise.resolve({request: 'token'}));
    client.onaChargeComplete(partnerTxID, 'testing');
    expect(spy).toHaveBeenCalledWith(
        {
          'accessToken': 'testing',
          'partnerTxID': partnerTxID
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaGetChargeStatus', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiGetChargeStatus');
    client.onaGetChargeStatus(partnerTxID, 'VND', 'testing');
    expect(spy).toHaveBeenCalledWith(
        {
          accessToken: 'testing',
          currency: 'VND',
          partnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test onaRefund', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiRefund');
    client.onaRefund(partnerTxID, partnerGroupTxID, 100000, 'VND', '34234', 'test', 'token');
    expect(spy).toHaveBeenCalledWith(
      {
        'accessToken': 'token',
        partnerTxID,
        partnerGroupTxID,
        amount: 100000,
        currency: 'VND',
        description: 'test',
        originTxID: '34234'
      },
      expect.anything(),
      expect.anything()
    );
  });

  it('Test onaGetOTCStatus', async () => {
    const spy = jest.spyOn(OnlineTransactionService, 'apiGetOTCStatus');
    client.onaGetOTCStatus(partnerTxID, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posCreateQRCode', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiCreateQrCode');
    client.posCreateQRCode(msgID, partnerTxID, 100000, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          amount: 100000,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posPerformQRCode', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiPerformQRCode');
    client.posPerformQRCode(msgID, partnerTxID, 100000, 'VND', 'code');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          amount: 100000,
          code: 'code',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posCancel', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiCancel');
    client.posCancel(msgID, partnerTxID, origPartnerTxID, 'txi',  "VND");
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          origPartnerTxID,
          msgID,
          origTxID: 'txi',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posRefund', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiRefund');
    client.posRefund(msgID, partnerTxID, 100000, 'VND',  origPartnerTxID, "refund");
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          reason: 'refund',
          msgID,
          amount: 100000,
          origPartnerTxID,
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posGetTxnDetails', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiGetTxnDetails');
    client.posGetTxnDetails(msgID, partnerTxID, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          txType: 'P2M',
        },
        expect.anything(),
        expect.anything()
    );
  });

  it('Test posGetRefundDetails', async () => {
    const spy = jest.spyOn(OfflineTransactionService, 'apiGetTxnDetails');
    client.posGetRefundDetails(msgID, partnerTxID, 'VND');
    expect(spy).toHaveBeenCalledWith(
        {
          currency: 'VND',
          partnerTxID,
          msgID,
          txType: 'Refund',
        },
        expect.anything(),
        expect.anything()
    );
  });
});
