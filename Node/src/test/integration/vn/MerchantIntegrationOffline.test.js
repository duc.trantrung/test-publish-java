import MerchantIntgrationOffline from "../../../MerchantIntegrationOffline";
import * as Utils from "../../../utils/Utils";

const env = 'STG';
const country = 'VN';
const partnerID = '939e5338-c610-4d09-b3f1-392a3c88a27f';
const partnerSecret = 'JX9CcWUUIhJzQS-h';
const merchantID = 'f68c4f68-f55f-4214-a2cc-ef9d756941e6';
const terminalID = '6c6693bb4b370f3626060ba96';

describe('Test VN MerchantIntegrationOffline', () =>{
    let client;
    let _partnerTxID;
    let _partnerGroupTxID;
    let _msgID ;
    let _origPartnerTxID;
    beforeAll(()=>{
        client = new MerchantIntgrationOffline(
            env,
            country,
            partnerID,
            partnerSecret,
            merchantID,
            terminalID
        )
    });

    beforeEach(()=>{
        _partnerTxID = Utils.generateRandomString(32);
        _partnerGroupTxID = Utils.generateRandomString(32);
        _msgID = Utils.generateMsgId();
        _origPartnerTxID = Utils.generateRandomString(32);
    });

    it('Test createQrCode', async ()=>{
        let amount = 2000;
        let currency = 'VND';

        let response = await client.posCreateQRCode(_msgID, _partnerTxID, amount, currency);

        expect(response).toBeDefined();

        expect(Object.keys(response).length).toEqual(5);
    });
});

