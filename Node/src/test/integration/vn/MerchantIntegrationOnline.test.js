import MerchantIntegrationOnline from "../../../MerchantIntegrationOnline";
import * as Utils from "../../../utils/Utils";

const env = 'STG';
const country = 'VN';
const partnerID = 'fd092e5b-900c-4969-8c2f-48ab29ef9d67';
const partnerSecret = 'nRrOISCpbpgFx3D_';
const merchantID = '0a46279c-c38c-480b-9fda-1466a5700445';
const clientID = 'e9b5560b0be844a2ad55c6afa8b23fbb';
const clientSecret = 'BDGSPQYYUqLXNkmy';
const redirectUrl = 'http://localhost:8888/result';

describe('Test VN MerchantIntegrationOnline', () =>{
    let client;
    let _partnerTxID;
    let _partnerGroupTxID;

    beforeAll(()=>{
       client = new MerchantIntegrationOnline(
           env,
           country,
           partnerID,
           partnerSecret,
           merchantID,
           clientID,
           clientSecret,
           redirectUrl
       )
    });

    beforeEach(()=>{
        _partnerTxID = Utils.generateRandomString(32);
        _partnerGroupTxID = Utils.generateRandomString(32);
    });

    it('Test onaChargeInit', async ()=>{
       let amount = 2000;
       let currency = 'VND';
       let isSync = false;
       let description = "this is testing";
       let response = await client.onaChargeInit(_partnerTxID, _partnerGroupTxID, amount, currency, isSync, description, null, null, null, null);
       expect(response).toBeDefined();
       const { request, partnerTxID } = response;
       expect(request).toBeDefined();
       expect(partnerTxID).toEqual(_partnerTxID);
    });

    it('Test hidenPaymentMethods onaChargeInit', async () =>{
        let amount = 2000;
        let currency = 'VND';
        let isSync = false;
        let description = "this is testing";
        let hidenPaymentMethods = ['POSTPAID'];
        let response = await client.onaChargeInit(_partnerTxID, _partnerGroupTxID, amount, currency, isSync, description, null, null, null, hidenPaymentMethods);
        expect(response).toBeDefined();
        const { request, partnerTxID } = response;
        expect(request).toBeDefined();
        expect(partnerTxID).toEqual(_partnerTxID);
    });
});

