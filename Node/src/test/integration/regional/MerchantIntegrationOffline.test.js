import MerchantIntgrationOffline from "../../../MerchantIntegrationOffline";
import * as Utils from "../../../utils/Utils";

const env = 'STG';
const country = 'PH';
const partnerID = '679db50f-3943-4fc1-af87-93e075a32869';
const partnerSecret = 'ZRh3iPhLpaAbBANN';
const merchantID = '6ea846c7-afb8-4a03-8c7c-5ddedda57a88';
const terminalID = '6ac42887732823c9d9298c868';

describe('Test Regional MerchantIntegrationOffline', () =>{
    let client;
    let _partnerTxID;
    let _partnerGroupTxID;
    let _msgID ;
    let _origPartnerTxID;
    beforeAll(()=>{
        client = new MerchantIntgrationOffline(
            env,
            country,
            partnerID,
            partnerSecret,
            merchantID,
            terminalID
        )
    });

    beforeEach(()=>{
        _partnerTxID = Utils.generateRandomString(32);
        _partnerGroupTxID = Utils.generateRandomString(32);
        _msgID = Utils.generateMsgId();
        _origPartnerTxID = Utils.generateRandomString(32);
    });

    it('Test createQrCode', async ()=>{
        let amount = 2000;
        let currency = 'PHP';

        let response = await client.posCreateQRCode(_msgID, _partnerTxID, amount, currency);

        expect(response).toBeDefined();

        expect(Object.keys(response).length).toEqual(5);

        const { msgID, qrcode, txID, qrid, expiryTime } = response;
        expect(msgID).toEqual(_msgID);
        expect(qrcode).isPrototypeOf(String);
        expect(txID).isPrototypeOf(String);
        expect(qrid).isPrototypeOf(String);
        expect(expiryTime).isPrototypeOf(String);
    });
});

