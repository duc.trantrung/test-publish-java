import MerchantIntegrationOnline from "../../../MerchantIntegrationOnline";
import * as Utils from "../../../utils/Utils";

const env = 'STG';
const country = 'MY';
const partnerID = 'b597c141-58e6-41c0-a362-027eed96fc81';
const partnerSecret = 'w_iQ9ls106MTxP1V';
const merchantID = 'ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379';
const clientID = '8888ef0060c24f41929ca41bfc702032';
const clientSecret = 'K6J1aznYAfT8ckyn';
const redirectUrl = 'http://172.104.183.204/index.php';

describe('Test Regional MerchantIntegrationOnline', () =>{
    let client;
    let _partnerTxID;
    let _partnerGroupTxID;
    const amount = 2000;
    const isSync = false;
    const description = "this is testing";
    const currency = 'MYR';

    beforeAll(()=>{
       client = new MerchantIntegrationOnline(
           env,
           country,
           partnerID,
           partnerSecret,
           merchantID,
           clientID,
           clientSecret,
           redirectUrl
       )
    });

    beforeEach(()=>{
        _partnerTxID = Utils.generateRandomString(32);
        _partnerGroupTxID = Utils.generateRandomString(32);
    });

    it('Test onaChargeInit', async ()=>{
       let response = await client.onaChargeInit(_partnerTxID, _partnerGroupTxID, amount, currency, isSync, description, null, null, null, null);
       expect(response).toBeDefined();
       const { request, partnerTxID } = response;
       expect(request).toBeDefined();
       expect(partnerTxID).toEqual(_partnerTxID);
    });

    it('Test hidenPaymentMethods onaChargeInit', async () =>{
        const hiddenPaymentMethods = ['POSTPAID'];
        let response = await client.onaChargeInit(_partnerTxID, _partnerGroupTxID, amount, currency, isSync, description, null, null, null, hiddenPaymentMethods);
        expect(response).toBeDefined();
        const { request, partnerTxID } = response;
        expect(request).toBeDefined();
        expect(partnerTxID).toEqual(_partnerTxID);
    });
});

