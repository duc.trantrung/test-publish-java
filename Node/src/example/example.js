import readline from 'readline';

import MerchantIntegration from '../MerchantIntegration'
import * as Utils from '../utils/Utils'

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const partnerTxID = Utils.generateRandomString(32);
const partnerGroupTxID = Utils.generateRandomString(32);
const currency = 'MYR';
const hidePaymentMethods = ['POSTPAID']
async function example() {
  const client = new MerchantIntegration('STG','MY','b597c141-58e6-41c0-a362-027eed96fc81','w_iQ9ls106MTxP1V','ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379','','8888ef0060c24f41929ca41bfc702032','K6J1aznYAfT8ckyn','http://172.104.183.204/index.php');
  // Generate webUrl
  const deepLink = await client.onaCreateWebUrl(partnerTxID, partnerGroupTxID, 1000, currency, false, 'testing otc', null, null, null, hidePaymentMethods, Utils.generateRandomString(7));
  if(deepLink) {
    console.log("Plz copy and paste it on browser: " + deepLink);
    rl.question('Plz input code: ', async (code) => {
      // --------Start get access token and confirm payment
      console.log("Process oauth ");
      const {access_token: accessToken} = await client.onaOAuth2Token(partnerTxID, code);

      console.log("Process charge complete ");
      const respChargeComplete = await client.onaChargeComplete(partnerTxID, accessToken);
      console.log('respChargeComplete ', respChargeComplete);
      // const refundPartnerTxID =  Utils.generateRandomString(32);
      const refundPartnerTxID = partnerTxID;
      rl.question('Please enter number refund amount: ', async (refundAmount) => {
        const respRefund = await client.onaRefund(refundPartnerTxID, partnerGroupTxID, parseInt(refundAmount),currency, respChargeComplete['txID'], 'test refund', accessToken );
        console.log('respRefund ', respRefund);
        const respGetRefundStatus = await client.onaGetRefundStatus(refundPartnerTxID, currency, accessToken);
        console.log('respRefund status ' , respGetRefundStatus);
        rl.close();
      });
      //--------end get access token and confirm payment
    });
    
  } else {
    console.log("Error happen");
  }
  
}
try {
  example()
} catch (e) {
  console.log(e)
}
