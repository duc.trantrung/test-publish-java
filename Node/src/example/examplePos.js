import readline from "readline";
import MerchantIntegrationOffline from '../MerchantIntegrationOffline'
import * as Utils from '../utils/Utils'
const partnerTxID = Utils.generateRandomString(32);
const msgID = Utils.generateMsgId();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

async function examplePos() {
  rl.question('Please enter amount: ', async (amount) => {
    try {
      const client = new MerchantIntegrationOffline('STG', 'VN', '939e5338-c610-4d09-b3f1-392a3c88a27f', 'JX9CcWUUIhJzQS-h', 'f68c4f68-f55f-4214-a2cc-ef9d756941e6', '6c6693bb4b370f3626060ba96');
      const respCreateQRCode = await client.posCreateQRCode(msgID, partnerTxID, parseInt(amount), 'VND');
      console.log('respCreateQRCode ', respCreateQRCode);
      const respGetStatus = await client.posGetTxnDetails(msgID, partnerTxID, 'VND');
      console.log('respGetStatus ', respGetStatus);
    } catch (e) {
      console.log('err ', e.response.data)
    }
    rl.close();
  })
}
try {
  examplePos()
} catch (e) {
  console.log(e)
}

