import readline from 'readline';
import MerchantIntegrationOnline from '../MerchantIntegrationOnline'
import * as Utils from '../utils/Utils'

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const partnerTxID = Utils.generateRandomString(32);
const partnerGroupTxID = Utils.generateRandomString(32);

async function exampleOnline() {
  const client = new MerchantIntegrationOnline('STG','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','http://localhost:8888/result');
  // Generate webUrl
  const deepLink = await client.onaCreateWebUrl(partnerTxID, partnerGroupTxID, 2000,'VND', false, 'testing otc', null, null, null,null, Utils.generateRandomString(7));
  if(deepLink) {
    console.log("Plz copy and paste it on browser: " + deepLink);
    rl.question('Plz input code: ', async (code) => {
      // --------Start get access token and confirm payment
      const {access_token: accessToken} = await client.onaOAuth2Token(partnerTxID, code);
      const respChargeComplete = await client.onaChargeComplete(partnerTxID, accessToken);
      console.log('respChargeComplete ', respChargeComplete);
      // const refundPartnerTxID =  Utils.generateRandomString(32);
      const refundPartnerTxID = partnerTxID;
      rl.question('Please enter number refund amount: ', async (refundAmount) => {
        const respRefund = await client.onaRefund(refundPartnerTxID, partnerGroupTxID, Number(refundAmount),'VND', respChargeComplete['txID'], 'test refund', accessToken );
        console.log('respRefund ', respRefund);
        const respGetRefundStatus = await client.onaGetRefundStatus(refundPartnerTxID, 'VND', accessToken);
        console.log('respRefund status ' , respGetRefundStatus);
        rl.close();
      });
      //--------end get access token and confirm payment
    });
    
  } else {
    console.log("Error happen");
  }
  
}
try {
  exampleOnline()
} catch (e) {
  console.log(e)
}

