import Requester from './utils/Requester';
import Config from './configs/Config';
import { SDK_VERSION } from './configs/Version';
import { OnlineTransactionService, OfflineTransactionService } from './services';

class MerchantIntegration {
  _requester;
  _config;

  /**
   * Constructor
   *
   * @param environment
   * @param country
   * @param partnerId
   * @param partnerSecret
   * @param merchantId
   * @param terminalId
   * @param clientId
   * @param clientSecret
   * @param redirectUrl
   */
  constructor(environment, country, partnerId, partnerSecret, merchantId, terminalId, clientId, clientSecret, redirectUrl) {
    country = country.toLowerCase();
    environment = environment.toLowerCase();
    this._config = new Config(environment, country, partnerId, partnerSecret, merchantId, terminalId, clientId, clientSecret, redirectUrl);
    this._requester = new Requester(this._config);
  }

  /**
   * Init a payment
   *
   * @param partnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param isSync
   * @param description
   * @param metaInfo
   * @param items
   * @param shippingDetails
   * @returns {Promise<*>}
   */
   async onaChargeInit(partnerTxID, partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods) {
    const payload = {
      partnerTxID,
      partnerGroupTxID,
      amount,
      currency,
      isSync,
      description,
      metaInfo,
      items,
      shippingDetails,
      hidePaymentMethods
    };
    if (!metaInfo) {
      delete payload.metaInfo
    }
    if (!items) {
      delete payload.items
    }
    if (!shippingDetails) {
      delete payload.shippingDetails
    }

    if (!hidePaymentMethods){
      delete payload.hidePaymentMethods
    }

    return OnlineTransactionService.apiChargeInit(payload, this._config, this._requester);
  }

  /**
   * Generate Web URL
   *
   * @param partnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param isSync
   * @param description
   * @param metaInfo
   * @param items
   * @param shippingDetails
   * @param state
   * @returns {Promise<*>}
   */
  async onaCreateWebUrl(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods, state) {
    const { request } = await this.onaChargeInit(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods);
    const payload = {
      request,
      countryCode: this._config.country.toUpperCase(),
      currency,
      state,
      partnerTxID
    };

    return OnlineTransactionService.createWebUrl(payload, this._config);
  }

  /**
   * Get OAuth token
   *
   * @param {string} partnerTxID
   * @param {string} code
   */
  async onaOAuth2Token(partnerTxID, code) {
    const payload = {
      partnerTxID,
      code
    };

    return OnlineTransactionService.apiOAuth2Token(payload, this._config, this._requester);
  }

  /**
   * Complete payments
   *
   * @param partnerTxID
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaChargeComplete(partnerTxID, accessToken) {
    return OnlineTransactionService.apiChargeComplete({
      partnerTxID,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Get Partner Charge Status
   *
   * @param partnerTxID
   * @param currency
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaGetChargeStatus(partnerTxID, currency, accessToken) {
    return OnlineTransactionService.apiGetChargeStatus({
      partnerTxID,
      currency,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Generate refunds
   *
   * @param refundPartnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param originTxID
   * @param description
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaRefund(refundPartnerTxID, partnerGroupTxID, amount, currency, originTxID, description, accessToken) {
    return OnlineTransactionService.apiRefund({
      partnerTxID: refundPartnerTxID,
      partnerGroupTxID,
      amount,
      currency,
      originTxID,
      description,
      accessToken,
    }, this._config, this._requester);
  }

  /**
   * Get refund status
   *
   * @param refundPartnerTxID
   * @param currency
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaGetRefundStatus(refundPartnerTxID, currency, accessToken) {
    return OnlineTransactionService.apiGetRefundStatus({
      refundPartnerTxID,
      currency,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Get One Time Charge Status
   *
   * @param partnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async onaGetOTCStatus(partnerTxID, currency) {
    return OnlineTransactionService.apiGetOTCStatus({
      partnerTxID,
      currency
    }, this._config, this._requester);
  }

  /**
   * Create QR Code
   *
   * @param msgID
   * @param partnerTxID
   * @param amount
   * @param currency
   * @returns {Promise<*>}
   */
  async posCreateQRCode(msgID, partnerTxID, amount, currency) {
    return OfflineTransactionService.apiCreateQrCode({
      msgID,
      partnerTxID,
      amount,
      currency
    }, this._config, this._requester);
  }

  /**
   * Perform a Transaction
   *
   * @param msgID
   * @param partnerTxID
   * @param amount
   * @param currency
   * @param code
   * @returns {Promise<*>}
   */
  async posPerformQRCode(msgID, partnerTxID, amount, currency, code) {
    return OfflineTransactionService.apiPerformQRCode({
      msgID,
      partnerTxID,
      amount,
      currency,
      code,
    }, this._config, this._requester);
  }

  /**
   * Cancel a Transaction
   *
   * @param msgID
   * @param partnerTxID
   * @param origPartnerTxID
   * @param origTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency) {
    return OfflineTransactionService.apiCancel({
      msgID,
      partnerTxID,
      origPartnerTxID,
      origTxID,
      currency,
    }, this._config, this._requester);
  }

  /**
   * Refund a POS Payment
   *
   * @param msgID
   * @param refundPartnerTxID
   * @param amount
   * @param currency
   * @param origPartnerTxID
   * @param description
   * @returns {Promise<*>}
   */
  async posRefund(msgID, refundPartnerTxID, amount, currency, origPartnerTxID, description) {
    return OfflineTransactionService.apiRefund({
      msgID,
      partnerTxID: refundPartnerTxID,
      amount,
      currency,
      origPartnerTxID,
      reason: description,
    }, this._config, this._requester);
  }

  /**
   * Check Transaction Details
   *
   * @param msgID
   * @param partnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posGetTxnDetails(msgID, partnerTxID, currency) {
    return OfflineTransactionService.apiGetTxnDetails({
      msgID,
      partnerTxID,
      currency,
      txType: 'P2M',
    }, this._config, this._requester);
  }

  /**
   * Check Refund Details
   *
   * @param msgID
   * @param refundPartnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async posGetRefundDetails(msgID, refundPartnerTxID, currency) {
    return OfflineTransactionService.apiGetTxnDetails({
      msgID,
      partnerTxID: refundPartnerTxID,
      currency,
      txType: 'Refund',
    }, this._config, this._requester);
  }

  /**
   * Get SDK Version
   *
   * @returns {string}
   */
  getVersion() {
    return SDK_VERSION
  }
}

export default MerchantIntegration;
