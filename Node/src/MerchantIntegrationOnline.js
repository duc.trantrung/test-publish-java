import Requester from './utils/Requester';
import Config from './configs/Config';
import { SDK_VERSION } from './configs/Version';
import { OnlineTransactionService } from './services';

class MerchantIntegrationOnline {
  _requester;
  _config;

  /**
   * Constructor
   *
   * @param environment
   * @param country
   * @param partnerId
   * @param partnerSecret
   * @param merchantId
   * @param clientId
   * @param clientSecret
   * @param redirectUrl
   */
  constructor(environment, country, partnerId, partnerSecret, merchantId, clientId, clientSecret, redirectUrl) {
    country = country.toLowerCase();
    environment = environment.toLowerCase();
    this._config = new Config(environment, country, partnerId, partnerSecret, merchantId, '', clientId, clientSecret, redirectUrl);
    this._requester = new Requester(this._config);
  }

  /**
   * Init a payment
   *
   * @param partnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param isSync
   * @param description
   * @param metaInfo
   * @param items
   * @param shippingDetails
   * @param hidePaymentMethods
   * @returns {Promise<*>}
   */
   async onaChargeInit(partnerTxID, partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails, hidePaymentMethods) {
    const payload = {
      partnerTxID,
      partnerGroupTxID,
      amount,
      currency,
      isSync,
      description,
      metaInfo,
      items,
      shippingDetails,
      hidePaymentMethods
    };
    if (!metaInfo) {
      delete payload.metaInfo
    }
    if (!items) {
      delete payload.items
    }
    if (!shippingDetails) {
      delete payload.shippingDetails
    }
    if (!hidePaymentMethods){
      delete payload.hidePaymentMethods
    }

    return OnlineTransactionService.apiChargeInit(payload, this._config, this._requester);
  }

  /**
   * Generate Web URL
   *
   * @param partnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param isSync
   * @param description
   * @param metaInfo
   * @param items
   * @param shippingDetails
   * @param hidePaymentMethods
   * @param state
   * @returns {Promise<*>}
   */
  async onaCreateWebUrl(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails,hidePaymentMethods, state) {
    const { request } = await this.onaChargeInit(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails,hidePaymentMethods);
    const payload = {
      request,
      countryCode: this._config.country.toUpperCase(),
      currency,
      state,
      partnerTxID
    };

    return OnlineTransactionService.createWebUrl(payload, this._config);
  }

  /**
   * Get OAuth token
   *
   * @param {string} partnerTxID
   * @param {string} code
   */
  async onaOAuth2Token(partnerTxID, code) {
    const payload = {
      partnerTxID,
      code
    };

    return OnlineTransactionService.apiOAuth2Token(payload, this._config, this._requester);
  }

  /**
   * Complete payments
   *
   * @param partnerTxID
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaChargeComplete(partnerTxID, accessToken) {
    return OnlineTransactionService.apiChargeComplete({
      partnerTxID,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Get Partner Charge Status
   *
   * @param partnerTxID
   * @param currency
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaGetChargeStatus(partnerTxID, currency, accessToken) {
    return OnlineTransactionService.apiGetChargeStatus({
      partnerTxID,
      currency,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Generate refunds
   *
   * @param refundPartnerTxID
   * @param partnerGroupTxID
   * @param amount
   * @param currency
   * @param originTxID
   * @param description
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaRefund(refundPartnerTxID, partnerGroupTxID, amount, currency, originTxID, description, accessToken) {
    return OnlineTransactionService.apiRefund({
      partnerTxID: refundPartnerTxID,
      partnerGroupTxID,
      amount,
      currency,
      originTxID,
      description,
      accessToken,
    }, this._config, this._requester);
  }

  /**
   * Get refund status
   *
   * @param refundPartnerTxID
   * @param currency
   * @param accessToken
   * @returns {Promise<*>}
   */
  async onaGetRefundStatus(refundPartnerTxID, currency, accessToken) {
    return OnlineTransactionService.apiGetRefundStatus({
      refundPartnerTxID,
      currency,
      accessToken
    }, this._config, this._requester);
  }

  /**
   * Get One Time Charge Status
   *
   * @param partnerTxID
   * @param currency
   * @returns {Promise<*>}
   */
  async onaGetOTCStatus(partnerTxID, currency) {
    return OnlineTransactionService.apiGetOTCStatus({
      partnerTxID,
      currency
    }, this._config, this._requester);
  }

  /**
   * Get SDK Version
   *
   * @returns {string}
   */
  getVersion() {
    return SDK_VERSION
  }
}

export default MerchantIntegrationOnline;
