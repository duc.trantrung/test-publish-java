const URLs = {
  vn: {
    OAUTH_TOKEN: '/grabid/v1/oauth2/token',
    POS_CREATE_QR_CODE: '/mocapay/partners/v1/terminal/qrcode/create',
    POS_CANCEL_TRANSACTION: '/mocapay/partners/v1/terminal/transaction/{origPartnerTxID}/cancel',
    POS_REFUND_TRANSACTION: '/mocapay/partners/v1/terminal/transaction/{origPartnerTxID}/refund',
    POS_PERFORM_TRANSACTION: '/mocapay/partners/v1/terminal/transaction/perform',
    POS_GET_TXN_DETAIL: '/mocapay/partners/v1/terminal/transaction/{partnerTxID}',
    ONA_CHARGE_INIT: '/mocapay/partner/v2/charge/init',
    ONA_CHARGE_COMPLETE: '/mocapay/partner/v2/charge/complete',
    ONA_CHARGE_STATUS: '/moca/partner/v2/charge/{partnerTxID}/status',
    ONA_REFUND: '/mocapay/partner/v2/refund',
    ONA_REFUND_STATUS: '/mocapay/partner/v2/refund/{refundPartnerTxID}/status',
    ONA_ONE_TIME_CHARGE_STATUS: '/mocapay/partner/v2/one-time-charge/{partnerTxID}/status'
  },
  default: {
    OAUTH_TOKEN: '/grabid/v1/oauth2/token',
    POS_CREATE_QR_CODE: '/grabpay/partner/v1/terminal/qrcode/create',
    POS_CANCEL_TRANSACTION: '/grabpay/partner/v1/terminal/transaction/{origPartnerTxID}/cancel',
    POS_REFUND_TRANSACTION: '/grabpay/partner/v1/terminal/transaction/{origPartnerTxID}/refund',
    POS_PERFORM_TRANSACTION: '/grabpay/partner/v1/terminal/transaction/perform',
    POS_GET_TXN_DETAIL: '/grabpay/partner/v1/terminal/transaction/{partnerTxID}',
    ONA_CHARGE_INIT: '/grabpay/partner/v2/charge/init',
    ONA_CHARGE_COMPLETE: '/grabpay/partner/v2/charge/complete',
    ONA_CHARGE_STATUS: '/grabpay/partner/v2/charge/{partnerTxID}/status',
    ONA_REFUND: '/grabpay/partner/v2/refund',
    ONA_REFUND_STATUS: '/grabpay/partner/v2/refund/{refundPartnerTxID}/status',
    ONA_ONE_TIME_CHARGE_STATUS: '/grabpay/partner/v2/one-time-charge/{partnerTxID}/status'
  }
};

export function getUrls(country) {
  return URLs[country] || URLs['default']
}
