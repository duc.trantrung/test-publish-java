const ENV = {
  vn: {
    stg: {
      baseUrl: 'https://stg-paysi.moca.vn'
    },
    prd: {
      baseUrl: 'https://partner-gw.moca.vn'
    }
  },
  default: {
    stg: {
      baseUrl: 'https://partner-api.stg-myteksi.com'
    },
    prd: {
      baseUrl: 'https://partner-api.grab.com'
    }
  }
};

export function getEnvs(country, environment) {
  country = country.toLowerCase();
  environment = environment.toLowerCase();
  const env = ENV[country] || ENV['default'];

  return env[environment]
}
