import {getUrls} from './ApiUrls';
import {getEnvs} from "./Environment";

const scope = {
  'vn': 'payment.vn.one_time_charge',
  'default': ['openid','payment.one_time_charge'].join(' ')
};

export default class Config {
  constructor(environment, country, partnerId, partnerSecret, merchantId, terminalId, clientId, clientSecret, redirectUrl) {
    this._environment = environment;
    this._country = country;
    this._partnerId = partnerId;
    this._partnerSecret = partnerSecret;
    this._merchantId = merchantId;
    this._terminalId = terminalId;
    this._clientId = clientId;
    this._clientSecret = clientSecret;
    this._redirectUrl = redirectUrl;
    this._urls = getUrls(this._country);
    this._envs = getEnvs(country, environment);
    this._scope = scope[country] || scope['default'];
  }

  getConfigs() {
    return {
      environment: this._environment,
      country: this._environment,
      partnerId: this._partnerId,
      partnerSecret: this._partnerSecret,
      terminalId: this._terminalId,
      clientId: this._clientId,
      clientSecret: this._clientSecret,
      redirectUrl: this._redirectUrl,
      merchantId: this._merchantId,
      scope: this._scope,
    }
  }

  get environment() {
    return this._environment;
  }

  set environment(value) {
    this._environment = value;
  }

  get country() {
    return this._country;
  }

  set country(value) {
    this._country = value;
  }

  get partnerId() {
    return this._partnerId;
  }

  set partnerId(value) {
    this._partnerId = value;
  }

  get partnerSecret() {
    return this._partnerSecret;
  }

  set partnerSecret(value) {
    this._partnerSecret = value;
  }

  get terminalId() {
    return this._terminalId;
  }

  set terminalId(value) {
    this._terminalId = value;
  }

  get clientId() {
    return this._clientId;
  }

  set clientId(value) {
    this._clientId = value;
  }

  get clientSecret() {
    return this._clientSecret;
  }

  set clientSecret(value) {
    this._clientSecret = value;
  }

  get redirectUrl() {
    return this._redirectUrl;
  }

  set redirectUrl(value) {
    this._redirectUrl = value;
  }


  get urls() {
    return this._urls;
  }

  set urls(value) {
    this._urls = value;
  }


  get merchantId() {
    return this._merchantId;
  }

  set merchantId(value) {
    this._merchantId = value;
  }

  get envs() {
    return this._envs;
  }

  set envs(value) {
    this._envs = value;
  }


  get scope() {
    return this._scope;
  }

  set scope(value) {
    this._scope = value;
  }
}
