import axios from 'axios';
import merge from 'lodash/merge'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty';
import queryString from 'query-string';
import { SDK_VERSION, SDK_SIGNATURE } from '../configs/Version';
import * as Utils from './Utils';

export default class Requester {
  constructor(config) {
    this._requester = axios.create({ baseURL: config.envs.baseUrl });
    this._config = config;
  }

  async hmacRequest(inputConfig) {
    const {headers = {}, method, data = '', params} = inputConfig;
    let url = inputConfig.url;
    const contentType = headers['Content-Type'] || 'application/json';
    const timestamp = Utils.getGMTString();
    if (!isEmpty(params)) {
      url = `${url}?${queryString.stringify(params)}`;
    }
    const hmac = Utils.generateHmac(method, url, contentType, timestamp, data, this._config.partnerSecret);
    if (method.toLowerCase() === 'get') {
      return this
        .request({
          url,
          method,
          headers: {
            'Content-Type': contentType,
            'Date': timestamp,
            'Authorization': `${this._config.partnerId}:${hmac}`,
            ...headers,
          }
        }).catch(function(){});
    }

    return this
        .request({
          url,
          method,
          headers: {
            'Content-Type': contentType,
            'Date': timestamp,
            'Authorization': `${this._config.partnerId}:${hmac}`,
            ...headers,
          },
          data,
        }).catch(function(){});
  }

  async request(inputConfig) {
    const config = merge({}, inputConfig, {});
    config.headers = {
      ...config.headers,
      'X-Sdk-Version': SDK_VERSION,
      'X-Sdk-Country': this._config.country,
      'X-Sdk-Language': 'nodejs',
      'X-Sdk-Signature': SDK_SIGNATURE
    };
    const response = await this._requester.request(config).catch(function(){});
    return get(response, 'data', {});    
  }
}
