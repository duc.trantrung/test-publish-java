import CryptoJS from 'crypto-js';
const Buffer = require('buffer/').Buffer;

const btoa = function(str){ return Buffer.from(str).toString('base64'); }

export function base64URLEncode (str) {
  return str.toString(CryptoJS.enc.Base64).replace(/=/g, '').replace(/\+/g, '-').replace(/\//g, '_');
}

export function generateHmac(httpMethod, requestURL, contentType, timestamp, requestBody, secret) {
  let hashedPayload = CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(JSON.stringify(requestBody)));
  if (httpMethod === 'GET' || !requestBody) {
    hashedPayload = '';
  }

  const requestData = [[httpMethod, contentType, timestamp, requestURL, hashedPayload].join('\n'), '\n'].join('');

  return CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(requestData, secret));
}

export function generatePOPSignature(accessToken, clientSecret) {
  const timestamp = new Date();
  const timestampUnix = Math.round(timestamp.getTime() / 1000);
  const message = timestampUnix.toString() + accessToken;
  const words = CryptoJS.enc.Utf8.parse(message);
  const utf8  = CryptoJS.enc.Utf8.stringify(words);
  const signature = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(utf8, clientSecret));
  const sub = base64URLEncode(signature);
  const payload = {
    "time_since_epoch": timestampUnix,
    "sig": sub
  };
  const payloadBytes = JSON.stringify(payload);

  return base64URLEncode(btoa(payloadBytes));
}


export function generateRamdomString32Character() {
  return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) { let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16); });
}

export function generateRandomString(length) {
  let text = '';
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

export function generateMsgId() {
  return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) { let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16); });
}

export function replaceUrl(url, args) {
  const regex = /{([a-zA-Z]+)}/gm;

  return url.replace(regex, (matches, item) => {
    return args[item] || ''
  });
}

export function getGMTString() {
  const timestamp = new Date();
  return timestamp.toGMTString();
}

export function getParams (query = window.location.search) {
  return query.replace(/^\?|#/, '').split('&').reduce((json, item) => {
    if (item) {
      const splitItem = item.split('=').map((value) => decodeURIComponent(value))
      json[splitItem[0]] = splitItem[1]
    }
    return json
  }, {})
}
