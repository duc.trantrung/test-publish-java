# Merchant Integration SDK

Merchant Integration SDK implements a set of functions, which used to call to GrabPay backend APIs for payment integration.

## Scenario

As usual, merchant's tech team will have to read official documents, write own code to interact with GrabPay backend, debug code when issues happen, contact with Integration team for help to check issues. Most common reasons of bugs are using wrong format, especially in generating HMAC, or generating invalid unique keys. 

With the SDK, most common errors go away. All the merchant need to do is understanding functions, calling functions with correct parameters. Merchant also spends less time for interacting with backend due to using SDK. 

## Version

This SDK version is currently at 1.0.0. 

## What is supported

For each function, we support merchant to generate headers, which include: authorization, date, SDK version, country. We also help to correct path, http method, parameters in request to GrabPay backend. 

In this version, SDK supports 2 types of payment: online and offline

### Online

| Function | Description | 
| :------- | :---------- |
| onaChargeInit | To init new transaction  |	
| onaCreateWebUrl |	To generate web URL from API charge init |
| onaOAuth2Token | To get the access_token for using later |
| onaChargeComplete | To complete transaction |
| onaGetChargeStatus | To get status of the transaction, which already completed |
| onaRefund | To refund the amount of a specific success transaction |
| onaGetRefundStatus | To check the status of the refund transaction | 
| onaGetOTCStatus | To get the OAuthCode after user's payment success on IOS | 


### Offline

| Function | Description | 
| :------- | :---------- |
| posCreateQRCode |	To create new QR code |
| posPerformQRCode | To support payment from customer's QR code |
| posCancel | To cancel transaction, do not pay |
| posRefund | To refund the amount of a specific success transaction |
| posGetTxnDetails | To check the status of the specific transaction |
| posGetRefundDetails | To check the status of the refund transaction  |


## Instruction

We support several languages like Python, Php, Java, Node, .Net. Merchant can choose the appropriate language to use. 

For each language, we provide guideline to call functions: 


- [Php](Php/README.md) 
- [Python](Python/README.md)
- [Java](Java/README.md)
- [Node](Node/README.md)
- [Net](Net/README.MD)


## Official document

The official document for GrabPay:  [online payment](https://developer.grab.com/docs/paysi-agent-partner-v2-otc/) and [offline payment](https://developer.grab.com/docs/paysi-partner/)

The official document for Moca: https://developer.moca.vn/merchants/docs