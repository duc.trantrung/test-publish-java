# How to run the sample follow

```terminal
    cd src
    python3 testOnA.py
    python3 testPos.py
    python3 testOnARegional.py
    python3 testPosRegional.py
```
# How to run unit test
```terminal
    cd src
    python3 testRestClient.py __main__
    python3 testMerchantIntegration.py __main__
```    

# How to use SDK

For using this SDK, You might copy all files in [src](./src) folder and paste it to your project.

# How to config moca package:
    On pipeline

# How to config environment on SDK:
```python
    # state : accept value 'STAGING','PRODUCTION', 'STG', 'PRD'
    # country: which country you live
    # partnerID: the partnerID from integration team share to merchant
    # partnerSecret: the partnerSecret from integration team share to merchant
    # merchantID: the merchantID from integration team share to merchant
    # terminalID: the terminalID from integration team share to merchant. It need config when use offline transaction
    # clientID: the clientID from integration team share to merchant. It need config when use online transaction
    # clientSecret: the clientSecret from integration team share to merchant. It need config when use online transaction
    # redirectUri: the redirectUri from integration team share to merchant. It need config when use online transaction
```

# How to configure for online acception

```python
    # Example for online
    from MerchantIntegrationOnline import MerchantIntegrationOnline
    callOna = MerchantIntegrationOnline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','https://developer.moca.vn/merchants/sample/result')
```

# How to configure for POS
```python
    # Example for offline 
    from MerchantIntegrationOffline import MerchantIntegrationOffline
    callPos = MerchantIntegrationOffline('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','6a6f98dd2dd903f9da03f2139')
```
# Detail for API online acception 

1. Charge Init: 
```python
    respChargeInit = callOna.onaChargeInit(partnerTxID,  partnerGroupTxID, amount, currency, isSync, description, metaInfo, items, shippingDetails,[])
```

2. Create Web Url: 
```python
    respChargeInit = callOna.onaCreateWebUrl(partnerGroupTxID, partnerTxID, amount, description, currency, state, metaInfo, items, shippingDetails, isSync,[])
```

3. get OAuth Token

```python
    respAuthCode = callOna.onaOAuthToken(partnerTxID,code)
```

4. Charge Complete
```python
    respChargeComplete = callOna.onaChargeComplete(partnerTxID,respAuthCode.access_token)
```

5 Get Charge status
```python
    respChargeStatus = callOna.onaGetChargeStatus(partnerTxID, currency, respAuthCode.accessToken)
```

6. refund online
```python
    response = callOna.onaRefund(refundPartnerTxID, amount, origPartnerTxID, description, respAuthCode.accessToken, currency)
```

7. Get Refund Status
```python
    response = callOna.onaGetRefundStatus(partnerTxID, respAuthCode.accessToken, currency)
```

8. Get One time charge status
```python
    response = callOna.onaGetOtcStatus(partnerTxID, respAuthCode.accessToken, currency)
```

# Detail for API offline

1. Create QR Code
```python
    respCreateQRCode= callPos.posCreateQRCode(msgID, partnerTxID, amount, currency)
```

2. Get transaction status:
```python
    resp= callPos.posGetTxnStatus(msgID, partnerTxID, currency)
```

3. cancelTxn
```file
    resp= callPos.posCancel(msgID, partnerTxID, origPartnerTxID, origTxID, currency)
```

4. refund Pos transaction
```file
    resp= callPos.posRefund(msgID, partnerTxID, originTxID, amount, description,currency)
```

5. Get refund transaction status:
```python
    resp= callPos.posGetRefundStatus(msgID, partnerTxID, currency)
```

6. performTxn
```file
    resp= callPos.posPerformQRCode(msgID, partnerTxID, amount, code, currency)
```
