import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="moca-merchant-sdk",
    version="0.0.1",
    author="Phuoc Nguyen Thanh",
    author_email="phuocnt@moca.vn",
    description="Support merchant integration",
    long_description= "This Sdk for support merchant integration by python with Grab Moca API easly." + long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/moca.vn/merchant-sdk/MerchantSdkPython",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)