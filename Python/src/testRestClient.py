import os
import base64
import hashlib
import json
from datetime import datetime
from RestClient import RestClient
from Config import Config
from unittest.mock import Mock

config = Config('STAGING','VN','fd092e5b-900c-4969-8c2f-48ab29ef9d67','nRrOISCpbpgFx3D_','0a46279c-c38c-480b-9fda-1466a5700445','6a6f98dd2dd903f9da03f2139','e9b5560b0be844a2ad55c6afa8b23fbb','BDGSPQYYUqLXNkmy','http://localhost:8888/result')    

def test_sha256():
    result = RestClient.sha256("abc")    
    assert result == "ungWv48Bz+pBQUDeXa4iI7ADYaOWF3qctBD/YfIAFa0=", "Should be something else"

def test_base64URLEncode():
    result = RestClient.base64URLEncode("abc")        
    assert result == "abc", "Should be something else"

    result = RestClient.base64URLEncode("+++")        
    assert result == "---", "Should be something else"
    # todo: add more case

def test_randomString():
    result = RestClient.randomString(5)        
    assert len(result) == 5, "Length should be 5"
    
    result = RestClient.randomString(0)        
    assert len(result) == 0, "Length should be 0"

    result = RestClient.randomString(1000)        
    assert len(result) == 1000, "Length should be 1000"

def test_merge_two_dicts():
    result = RestClient.merge_two_dicts({'a': 1}, {'b': 2})        
    assert len(result) == 2, "Length should be 2"

    result = RestClient.merge_two_dicts({'a': 1}, {})        
    assert len(result) == 1, "Length should be 1"

    result = RestClient.merge_two_dicts({}, {})        
    assert len(result) == 0, "Length should be 0"

def test_fmt_date():
    timestamp = datetime.now().timestamp()    
    result = RestClient.fmt_date(timestamp)    
    assert len(result) == 29, "length of now should be 29"

def test_generateHmac():    
    result = RestClient.generateHmac(config, 'GET', '/some-path', '', {}, 'Sun, 07 Nov 2021 03:59:44 GMT')                
    assert result == 'shty3NjxHbqI3Xyjx8OsGrdTwy+9t4XgMLXXSyqYd/U=', "Should be something else"
    assert len(result) == 44, "Length should be 44"

def test_generatePOPSign():
    result = RestClient.generatePOPSign(config, 'some-access-token', 1636258588.112487)          
    assert result == 'eyJ0aW1lX3NpbmNlX2Vwb2NoIjogMTYzNjI1ODU4OCwgInNpZyI6ICItbTZ5dFlSNXg3UnpoTWgwWUo2Q3RMVzk4dERYcVREZzlQZEtfRmtFVW5jIn0', "Should be something else"

def test_http_build_query():
    bodyTest = {
        'action': 'add',
        'controller': 'invoice',
        'code': 'debtor',
    }
    result = RestClient.http_build_query(bodyTest);
    assert result == 'action=add&controller=invoice&code=debtor', "Ressult should be action=add&controller=invoice&code=debtor"

def test_prepareRequest():
    # case 1 test get
    partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    msgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    requestCreateQrCode = {
        'amount' : 1000,
        'currency' : 'VND',
        'partnerTxID' : partnerTxID
    }
    resultGet = RestClient.prepareRequest(config, 'GET', config.pathPosCreateQRCode, 'application/json', requestCreateQrCode, 'OFFLINE','')
    resultGet = json.loads(json.dumps(resultGet))
    if(resultGet) :
        assert resultGet['headers']['Authorization'].find(config.partnerID) != -1, "It need to have partnerID in header"
        assert resultGet['body'] == '', "Body should be null"
        assert len(resultGet['headers']) == 8, "Should be 8"
        result = "/mocapay/partners/v1/terminal/qrcode/create?grabID="+config.merchantID+"&terminalID="+config.terminalID
        assert resultGet['apiUrl'] == result, "Should be "+result

    # case 2 test put
    requestBodyCancel = {
        "msgID": msgID,
        "grabID":"84dfaba5-7a1b-4e91-aa1c-f7ef93895266",
        "terminalID":"terminal25-meow-meow-meow",
        "currency":"VND",
        "origTxID":"partner-xi-1",
        "partnerTxID":partnerTxID
    }
    resultPut = RestClient.prepareRequest(config, 'PUT', config.pathPosCancel,'application/json', requestBodyCancel, 'OFFLINE','')
    if(resultPut) :
        assert resultPut['headers']['Authorization'].find(config.partnerID) != -1, "It need to have partnerID in header"
        assert len(resultGet['headers']) == 8, "Length should be 8"

    # case 3 test post
    requestChargeInit = {"partnerTxID": partnerTxID, "partnerGroupTxID": partnerTxID, "amount": 4000, "currency": "VND", "merchantID": "0a46279c-c38c-480b-9fda-1466a5700445", "description": "testing otc", "isSync": False, "metaInfo": {"brandName": ""}};
    resultPost = RestClient.prepareRequest(config, 'POST', config.pathPosCancel,'application/json', requestChargeInit, 'OFFLINE','')
    if(resultPost) :
        assert len(resultPost['headers']) == 9, "Length should be 9"

    # case 4 test post
    requestChargeComplete = {"partnerTxID": partnerTxID, "accessToken": "fake"};
    resultPostChargeComplete = RestClient.prepareRequest(config, 'POST', config.pathOnaChargeComplete,'application/json', requestChargeComplete, 'ONLINE','fake')
    
    if(resultPostChargeComplete) :
        assert len(resultPostChargeComplete['headers']) == 10, "Length should be 10"

    # case 5 test oauth token
    requestOAuth = {"partnerTxID": partnerTxID, "code": "fake"};
    requestOAuth = RestClient.prepareRequest(config, 'POST', config.pathOnaOAuth2Token,'application/json', requestOAuth, 'ONLINE','')
    print(requestOAuth)
    if(requestOAuth) :
        assert len(requestOAuth['headers']) == 7, "Length sshould be 7"
        

def test_sendRequest():
    requestChargeInit = {"partnerTxID": hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest(), "partnerGroupTxID": hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest(), "amount": 4000, "currency": "VND", "merchantID": "0a46279c-c38c-480b-9fda-1466a5700445", "description": "testing otc", "isSync": False, "metaInfo": {"brandName": ""}};
    result = RestClient.sendRequest(config,'POST',config.pathOnaChargeInit,'application/json',requestChargeInit , 'ONLINE','')
    if(result) :
        assert result['status'] == 200, "Status should be 200"

def test_get():
    result = RestClient.get(config, config.pathPosCreateQRCode, 'application/json', 'OFFLINE')
    if(result) :
        assert result['status'] != 200, "Status should be 200"

def test_post():
    requestChargeInit = {"partnerTxID": hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest(), "partnerGroupTxID": hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest(), "amount": 4000, "currency": "VND", "merchantID": "0a46279c-c38c-480b-9fda-1466a5700445", "description": "testing otc", "isSync": False, "metaInfo": {"brandName": ""}};
    result = RestClient.post(config, config.pathOnaChargeInit, requestChargeInit, 'application/json', 'ONLINE')
    if(result) :
        assert result['status'] == 200, "Status should be 200"

def test_put():
    bodyCancel = {
        "msgID": hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()  ,
        "grabID":"84dfaba5-7a1b-4e91-aa1c-f7ef93895266",
        "terminalID":"terminal25-meow-meow-meow",
        "currency":"VND",
        "origTxID":"partner-xi-1",
        "partnerTxID":hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    }
    result = RestClient.put(config, config.pathPosCancel, bodyCancel,'application/json', 'OFFLINE')
    if(result) :
        assert result['status'] == 400, "Status should be 400 because the transaction is fake"

if __name__ == "__main__":
    test_sha256()    
    test_base64URLEncode()
    test_randomString()
    test_merge_two_dicts()
    test_fmt_date()
    test_generateHmac()
    test_generatePOPSign()
    test_http_build_query()
    test_prepareRequest()
    test_sendRequest()
    test_get()
    test_post()
    test_put()
    print("Everything passed")