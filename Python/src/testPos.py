import os
import hashlib
from MerchantIntegrationOffline import MerchantIntegrationOffline
from RestClient import RestClient
from Config import Config
import json

# Sample call API charge init to get QR offline.
ENV = 'STG'
COUNTRY = 'VN'
CURRENCY = 'VND'
PARTNER_ID = '939e5338-c610-4d09-b3f1-392a3c88a27f'
PARTNER_SECRET = 'JX9CcWUUIhJzQS-h'
MERCHANT_ID = 'f68c4f68-f55f-4214-a2cc-ef9d756941e6'
TERMINAL_ID = '6c6693bb4b370f3626060ba96'
partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
msgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest() 
print(partnerTxID)
call = MerchantIntegrationOffline(ENV, COUNTRY, PARTNER_ID, PARTNER_SECRET, MERCHANT_ID, TERMINAL_ID)
amount = input("Please enter number amount: ")
respCreateQRCode= call.posCreateQRCode(msgID, partnerTxID, int(amount), CURRENCY)
print('- Response: '+ str(respCreateQRCode))
ready = input('Plz input yes when you already payment: ')
if (ready == 'yes') :
    respGetStatus = call.posGetTxnStatus(msgID, partnerTxID, CURRENCY)
    print('- Response: '+ str(respGetStatus))
    refundPartnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    refundMsgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest() 
    refundAmount = input("Please enter number amount: ")
    respPosRefund= call.posRefund(refundMsgID, refundPartnerTxID,int(refundAmount), CURRENCY, partnerTxID, 'test refund')
    print('- Response: '+ str(respPosRefund))
    respPosGetRefundStatus = call.posGetRefundStatus(refundMsgID, refundPartnerTxID, CURRENCY)
    print('- Response: '+ str(respPosGetRefundStatus))