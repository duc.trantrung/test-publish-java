import hashlib
from MerchantIntegrationOnline import MerchantIntegrationOnline
from MerchantIntegrationOffline import MerchantIntegrationOffline
from RestClient import RestClient
import json

def testonA():
    # Sample call API charge init to get desktop QR online.
    partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    partnerGroupTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    print('partnerTxID: ' +partnerTxID)
    print('partnerGroupTxID: ' +partnerGroupTxID)
    call = MerchantIntegrationOnline('STAGING',
    'VN',
    'fd092e5b-900c-4969-8c2f-48ab29ef9d67',
    'nRrOISCpbpgFx3D_',
    '0a46279c-c38c-480b-9fda-1466a5700445',
    'e9b5560b0be844a2ad55c6afa8b23fbb',
    'BDGSPQYYUqLXNkmy',
    'http://localhost:8888/result')

    amount = input("Please enter number amount: ")

    respChargeInit = call.onaCreateWebUrl(partnerTxID,partnerGroupTxID,int(amount),'testing otc','VND',state = RestClient.randomString(7))

    if(isinstance(respChargeInit,str)) :
        print("Plz copy url and open it on browser and payment by grab app: \n" + respChargeInit)

        # --------Start get access token and confirm payment
        code = input("Please enter code from redirect url: ")

        respAuthCode = call.onaOAuthToken(partnerTxID,code)

        print ('respAuthCode'+ str(respAuthCode))
        if(respAuthCode['status'] == 200) :
            data = json.loads(respAuthCode['data'])
            access_token = data['access_token']

            respChargeComplete = call.onaChargeComplete(partnerTxID,access_token)
            respGetChargeStatus = call.onaGetChargeStatus(partnerTxID, 'VND', access_token)
            print(respGetChargeStatus)
            if(respChargeComplete['status' ==200]) :
                data = json.json.load(respChargeComplete['data'])
                refundPartnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
                refundAmount = input("Please enter number amount for refund: ")
                respRefund = call.onaRefund(partnerGroupTxID, refundPartnerTxID, refundAmount, data['txID'], 'test refund', access_token, 'VND');
                print (respRefund)
                respGetRefundStatus = call.onaGetRefundStatus(refundPartnerTxID, 'VND', access_token)
                print(respGetRefundStatus)
            else :
                print("Have an error when complete the transaction")
        else :
            print("Have an error")
        # --------end get access token and confirm payment
    else:
        print (respChargeInit)

def testPos():
    partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    msgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest() 
    print(partnerTxID)
    call = MerchantIntegrationOffline('STAGING','VN','939e5338-c610-4d09-b3f1-392a3c88a27f','JX9CcWUUIhJzQS-h','f68c4f68-f55f-4214-a2cc-ef9d756941e6','6c6693bb4b370f3626060ba96')
    amount = input("Please enter number amount: ")
    respCreateQRCode= call.posCreateQRCode(msgID, partnerTxID, int(amount), 'VND')
    print('- Response: '+ str(respCreateQRCode))

    respGetStatus = call.posGetTxnStatus(msgID, partnerTxID, 'VND')
    print('- Response: '+ str(respGetStatus))

if __name__ == "__main__":
    testonA()
    testPos()