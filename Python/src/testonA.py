import hashlib
from MerchantIntegrationOnline import MerchantIntegrationOnline
from RestClient import RestClient
import json

# Sample call API charge init to get desktop QR online.
ENV = 'STG'
COUNTRY = 'VN'
CURRENCY = 'VND'
PARTNER_ID = 'fd092e5b-900c-4969-8c2f-48ab29ef9d67'
PARTNER_SECRET = 'nRrOISCpbpgFx3D_'
MERCHANT_ID = '0a46279c-c38c-480b-9fda-1466a5700445'
CLIENT_ID = 'e9b5560b0be844a2ad55c6afa8b23fbb'
CLIENT_SECRET = 'BDGSPQYYUqLXNkmy'
REDIRECT_URI= 'http://localhost:8888/result'

partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
partnerGroupTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
print('partnerTxID: ' +partnerTxID)
print('partnerGroupTxID: ' +partnerGroupTxID)
call = MerchantIntegrationOnline(ENV, COUNTRY, PARTNER_ID, PARTNER_SECRET, MERCHANT_ID, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI)
amount = input("Please enter number amount: ")

respChargeInit = call.onaCreateWebUrl(partnerTxID, partnerGroupTxID, int(amount), 'testing otc', CURRENCY,state= RestClient.randomString(7))

if(isinstance(respChargeInit,str)) :
    print("Plz copy url and open it on browser and payment by grab app: \n" + respChargeInit)
    # --------Start get access token and confirm payment
    code = input("Please enter code from redirect url: ")

    respAuthCode = call.onaOAuthToken(partnerTxID,code)

    print ('respAuthCode'+ str(respAuthCode))
    if(respAuthCode['status'] == 200) :
        data = json.loads(respAuthCode['data'])
        access_token = data['access_token']

        respChargeComplete = call.onaChargeComplete(partnerTxID, access_token)
        print(respChargeComplete)
        respGetChargeStatus = call.onaGetChargeStatus(partnerTxID, CURRENCY, access_token)
        print(respGetChargeStatus)
        if(respChargeComplete['status'] ==200) :
            data = json.loads(respChargeComplete['data'])
            refundPartnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
            refundAmount = input("Please enter number amount for refund: ")
            respRefund = call.onaRefund(refundPartnerTxID, partnerGroupTxID, refundAmount, CURRENCY, data['txID'], 'test refund', access_token);
            print (respRefund);
            respGetRefundStatus = call.onaGetRefundStatus(refundPartnerTxID, CURRENCY, access_token)
            print(respGetRefundStatus)
        else :
            print("Have an error when complete the transaction")
    else :
        print("Have an error")
    # --------end get access token and confirm payment
else:
    print (respChargeInit);




