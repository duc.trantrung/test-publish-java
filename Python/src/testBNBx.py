import hashlib
from MerchantIntegrationOnline import MerchantIntegrationOnline
from RestClient import RestClient
import json

# Sample call API charge init to get desktop QR online.
ENV = 'STG';
COUNTRY = 'MY';
CURRENCY = 'MYR';
PARTNER_ID = 'b597c141-58e6-41c0-a362-027eed96fc81';
PARTNER_SECRET = 'w_iQ9ls106MTxP1V';
MERCHANT_ID = 'ff7b0a25-e9e5-4abe-aa48-82cd9e7c9379';
CLIENT_ID = '8888ef0060c24f41929ca41bfc702032';
CLIENT_SECRET = 'K6J1aznYAfT8ckyn';
REDIRECT_URI= 'http://172.104.183.204/index.php';

partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
partnerGroupTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
print('partnerTxID: ' +partnerTxID)
print('partnerGroupTxID: ' +partnerGroupTxID)
call = MerchantIntegrationOnline(ENV, COUNTRY, PARTNER_ID, PARTNER_SECRET, MERCHANT_ID, CLIENT_ID, CLIENT_SECRET, REDIRECT_URI)
amount = input("Please enter number amount: ")

respChargeInit = call.onaCreateWebUrl(partnerTxID, partnerGroupTxID, int(amount), 'testing otc', CURRENCY, {}, {}, {}, False, ['INSTALMENT'], RestClient.randomString(7))

if(isinstance(respChargeInit,str)) :
    print("Plz copy url and open it on browser and payment by grab app: \n" + respChargeInit)
    # --------Start get access token and confirm payment
    code = input("Please enter code from redirect url: ")

    respAuthCode = call.onaOAuthToken(partnerTxID,code)

    print ('respAuthCode'+ str(respAuthCode))
    if(respAuthCode['status'] == 200) :
        data = json.loads(respAuthCode['data'])
        access_token = data['access_token']

        respChargeComplete = call.onaChargeComplete(partnerTxID, access_token)
        print(respChargeComplete)
        respGetChargeStatus = call.onaGetChargeStatus(partnerTxID, CURRENCY, access_token)
        print(respGetChargeStatus)
        if(respChargeComplete['status'] ==200) :
            data = json.loads(respChargeComplete['data'])
            refundPartnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
            refundAmount = input("Please enter number amount: ")
            respRefund = call.onaRefund(refundPartnerTxID, partnerGroupTxID, refundAmount, CURRENCY, data['txID'], 'test refund', access_token);
            print (respRefund);
            respGetRefundStatus = call.onaGetRefundStatus(refundPartnerTxID, CURRENCY, access_token)
            print(respGetRefundStatus)
        else :
            print("Have an error when complete the transaction")
    else :
        print("Have an error")
    # --------end get access token and confirm payment
else:
    print (respChargeInit);
