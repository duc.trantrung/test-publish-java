import os
import hashlib
from MerchantIntegrationOffline import MerchantIntegrationOffline
from RestClient import RestClient
from Config import Config
import json

# Sample call API charge init to get QR offline.
ENV = 'STG'
COUNTRY = 'PH'
CURRENCY = 'PHP'
PARTNER_ID = '679db50f-3943-4fc1-af87-93e075a32869'
PARTNER_SECRET = 'ZRh3iPhLpaAbBANN'
MERCHANT_ID = '6ea846c7-afb8-4a03-8c7c-5ddedda57a88'
TERMINAL_ID = '6ac42887732823c9d9298c868'

partnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
msgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest() 
print(partnerTxID)
call = MerchantIntegrationOffline(ENV, COUNTRY, PARTNER_ID, PARTNER_SECRET, MERCHANT_ID, TERMINAL_ID)
amount = input("Please enter number amount: ")
respCreateQRCode= call.posCreateQRCode(msgID, partnerTxID, int(amount), 'VND')
print('- Response: '+ str(respCreateQRCode))
ready = input('Plz input yes when you already payment: ')
if (ready == 'yes') :
    respGetStatus = call.posGetTxnStatus(msgID, partnerTxID, 'VND')
    print('- Response: '+ str(respGetStatus))
    refundPartnerTxID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest()
    refundMsgID = hashlib.md5(RestClient.randomString(32).encode('utf-8')).hexdigest() 
    refundAmount = input("Please enter number amount: ")
    respPosRefund= call.posRefund(refundMsgID, refundPartnerTxID,int(refundAmount), CURRENCY, partnerTxID, 'test refund')
    print('- Response: '+ str(respPosRefund))
    respPosGetRefundStatus = call.posGetRefundStatus(refundMsgID, refundPartnerTxID, CURRENCY)
    print('- Response: '+ str(respPosGetRefundStatus))